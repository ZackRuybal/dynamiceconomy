package me.ksafin.DynamicEconomy;

import couk.Adamki11s.AutoUpdater.AUCore;
import couk.Adamki11s.Extras.Colour.ExtrasColour;
import couk.Adamki11s.Extras.Extras.Extras;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;
import lib.PatPeter.SQLibrary.MySQL;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

public class DynamicEconomy extends JavaPlugin {
   public static FileConfiguration config;
   String name;
   String version;
   private final DynamicEconomyPlayerListener playerListener = new DynamicEconomyPlayerListener(this);
   private DynamicEconomyCommandExecutor commandExec;
   public static Economy economy = null;
   public static Permission permission = null;
   public static ExtrasColour color = new ExtrasColour();
   public static String prefix;
   public static int defaultAmount;
   public static boolean localNotify;
   public static boolean globalNotify;
   public static boolean hasUpdate;
   public static boolean logwriting;
   public static boolean updateNotifOnLogin;
   public static double salestax;
   public static double purchasetax;
   public static boolean depositTax;
   public static String taxAccount;
   public static boolean location_restrict;
   public static int minimum_y;
   public static int maximum_y;
   public static boolean altCommands;
   public static boolean useRegions;
   public static boolean useLoans;
   public static double interestRate;
   public static int paybackTime;
   public static int maxLoans;
   public static double maxLoanAmount;
   public static double minLoanAmount;
   public static boolean useLoanAccount;
   public static String loanAccountName;
   public static long loanCheckInterval;
   public static boolean enableUpdateChecker;
   public static boolean useStaticInterest;
   public static double dynamicCompressionRate;
   public static String[] bannedSaleItems;
   public static String[] bannedPurchaseItems;
   public static boolean enableOverTimePriceDecay;
   public static double overTimePriceDecayPercent;
   public static boolean enableOverTimePriceInflation;
   public static double overTimePriceInflationPercent;
   public static long overTimePriceChangePeriod;
   public static long overTimePriceChangePeriodCheck;
   public static boolean taxAccountIsBank;
   public static boolean loanAccountIsBank;
   public static String[] dyneconWorld;
   public static String currencySymbol;
   public static boolean enableRandomEvents;
   public static int randomEventInterval;
   public static double randomEventChance;
   public static String signTaglineColor;
   public static String signInfoColor;
   public static String signInvalidColor;
   public static boolean isWandOn = true;
   public static boolean groupControl;
   public static boolean useRegionFlags;
   public static boolean usemysql;
   private static String sqlhost;
   private static String sqluser;
   private static String sqlpw;
   private static String sqlport;
   private static String sqldb;
   public static boolean isBuySellCommandsEnabled;
   public AUCore updater;
   static File regionFile;
   static DynamicEconomy plugin;
   public static HashMap selectedCorners = new HashMap();
   public static File configFile;
   static FileConfiguration regionConfig;
   public static File loansFile;
   static FileConfiguration loansFileConfig;
   public static File itemsFile;
   static FileConfiguration itemConfig;
   public static File messagesFile;
   public static FileConfiguration messagesConfig;
   public static File randomEventFile;
   public static FileConfiguration randomEventConfig;
   public static File signsFile;
   public static FileConfiguration signsConfig;
   public static File usersFile;
   public static FileConfiguration usersConfig;
   public static File aliasFile;
   public static FileConfiguration aliasConfig;
   public static File groupsFile;
   public static FileConfiguration groupsConfig;
   public static File shopsFile;
   public static FileConfiguration shopsConfig;
   static Logger log = Logger.getLogger("Minecraft");
   public static MySQL mysql;
   public static Connection connection;

   public DynamicEconomy() {
      this.updater = new AUCore("http://exampop.com/update.html", log, "[DynamicEconomy]");
   }

   public void onEnable() {
      PluginManager pm = this.getServer().getPluginManager();
      PluginDescriptionFile pdf = pm.getPlugin("DynamicEconomy").getDescription();
      plugin = this;
      this.name = pdf.getName();
      this.version = pdf.getVersion();
      List author = pdf.getAuthors();
      log.info(this.name + " v" + this.version + " by " + (String)author.get(0) + " enabled!");
      boolean economyIsSet = this.setupEconomy();
      boolean permissionIsSet = this.setupPermissions();
      if (permissionIsSet) {
         log.info("[DynamicEconomy] Vault Permissions hooked");
      } else {
         log.info("[DynamicEconomy] Vault Permissions not hooked");
      }

      if (economyIsSet) {
         log.info("[DynamicEconomy] Vault Economy hooked");
      } else {
         log.info("[DynamicEconomy] Vault Economy not hooked");
      }

      this.playerListener.setPermission(permission);
      double fullVer = Double.parseDouble(this.version.substring(0, 2));
      double subVer = Double.parseDouble(this.version.substring(2));
      new Extras("DynamicEconomy");
      hasUpdate = !this.updater.checkVersion(fullVer, subVer, "DynamicEconomy");
      this.playerListener.fullver = fullVer;
      this.playerListener.subver = subVer;
      this.playerListener.setUpdater(this.updater);
      itemsFile = new File(this.getDataFolder(), "Items.yml");
      itemConfig = YamlConfiguration.loadConfiguration(itemsFile);
      regionFile = new File(this.getDataFolder(), "Regions.yml");
      regionUtils.setRegionFile(regionFile);
      regionConfig = YamlConfiguration.loadConfiguration(regionFile);
      regionUtils.setRegionFileConfig(regionConfig);
      loansFile = new File(this.getDataFolder(), "Loans.yml");
      loansFileConfig = YamlConfiguration.loadConfiguration(loansFile);
      loan.initFiles(loansFile, loansFileConfig);
      configFile = new File(this.getDataFolder(), "config.yml");
      config = YamlConfiguration.loadConfiguration(configFile);
      messagesFile = new File(this.getDataFolder(), "messages.yml");
      messagesConfig = YamlConfiguration.loadConfiguration(messagesFile);
      randomEventFile = new File(this.getDataFolder(), "randomevents.yml");
      randomEventConfig = YamlConfiguration.loadConfiguration(randomEventFile);
      File signsDir = new File(this.getDataFolder(), "signs");
      signsFile = new File(signsDir, "signs.yml");
      signsConfig = YamlConfiguration.loadConfiguration(signsFile);
      usersFile = new File(this.getDataFolder(), "users.yml");
      usersConfig = YamlConfiguration.loadConfiguration(usersFile);
      aliasFile = new File(this.getDataFolder(), "alias.yml");
      aliasConfig = YamlConfiguration.loadConfiguration(aliasFile);
      groupsFile = new File(this.getDataFolder(), "groups.yml");
      groupsConfig = YamlConfiguration.loadConfiguration(groupsFile);
      shopsFile = new File(this.getDataFolder(), "shops.yml");
      shopsConfig = YamlConfiguration.loadConfiguration(shopsFile);
      regionConfig.createSection("regions");
      Transaction.regionConfigFile = regionConfig;
      File logFile = new File(this.getDataFolder(), "log.txt");
      new Utility(logFile, this);
      InputStream itemsIn = this.getClass().getResourceAsStream("Items.yml");
      InputStream configIn = this.getClass().getResourceAsStream("config.yml");
      InputStream aliasIn = this.getClass().getResourceAsStream("alias.yml");
      InputStream messagesIn = this.getClass().getResourceAsStream("messages.yml");
      InputStream randomeventsIn = this.getClass().getResourceAsStream("randomevents.yml");
      InputStream groupsIn = this.getClass().getResourceAsStream("groups.yml");
      if (itemsFile.exists()) {
         log.info("[DynamicEconomy] Items database loaded.");
      } else {
         try {
            Utility.copyFile(itemsIn, itemsFile);
         } catch (Exception var34) {
            log.info("[DynamicEconomy] IOException when creating Items.yml in Main");
            log.info(itemsFile.toString());
            var34.printStackTrace();
         }

         log.info("[DynamicEconomy] Items.yml created.");
      }

      if (configFile.exists()) {
         log.info("[DynamicEconomy] Core Config loaded.");
      } else {
         try {
            Utility.copyFile(configIn, configFile);
         } catch (Exception var33) {
            log.info("[DynamicEconomy] IOException when creating config.yml in Main");
            var33.printStackTrace();
         }

         log.info("[DynamicEconomy] config.yml created.");
      }

      if (signsFile.exists()) {
         log.info("[DynamicEconomy] Signs File Loaded.");
      } else {
         try {
            signsDir.mkdir();
            signsConfig.save(signsFile);
         } catch (Exception var32) {
            log.info("[DynamicEconomy] IOException when creating signsFile in Main");
            var32.printStackTrace();
         }

         log.info("[DynamicEconomy] signs.yml created.");
      }

      if (shopsFile.exists()) {
         log.info("[DynamicEconomy] Shops File Loaded.");
      } else {
         try {
            shopsConfig.save(shopsFile);
         } catch (Exception var31) {
            log.info("[DynamicEconomy] IOException when creating Shops File in Main");
            var31.printStackTrace();
         }

         log.info("[DynamicEconomy] shops.yml created.");
      }

      if (regionFile.exists()) {
         try {
            regionConfig.load(regionFile);
         } catch (Exception var30) {
            log.info("[DynamicEconomy] Error loading regions.yml");
         }

         log.info("[DynamicEconomy] Region database loaded.");
      } else {
         try {
            regionConfig.save(regionFile);
         } catch (Exception var29) {
            log.info("[DynamicEconomy] IOException when creating Regions.yml in Main");
            log.info(regionFile.toString());
            var29.printStackTrace();
         }

         log.info("[DynamicEconomy] regions.yml created.");
      }

      if (loansFile.exists()) {
         log.info("[DynamicEconomy] Loans database loaded.");
      } else {
         try {
            loansFileConfig.save(loansFile);
            loansFileConfig.createSection("loans");
            loansFileConfig.createSection("debts");
         } catch (Exception var28) {
            log.info("[DynamicEconomy] IOException when creating Loans.yml in Main");
            log.info(loansFile.toString());
            var28.printStackTrace();
         }

         log.info("[DynamicEconomy] loans.yml created.");
      }

      if (messagesFile.exists()) {
         log.info("[DynamicEconomy] Messages loaded.");
      } else {
         try {
            Utility.copyFile(messagesIn, messagesFile);
         } catch (Exception var27) {
            log.info("[DynamicEconomy] IOException when creating messages.yml in Main");
            var27.printStackTrace();
         }

         log.info("[DynamicEconomy] messages.yml created.");
      }

      if (randomEventFile.exists()) {
         log.info("[DynamicEconomy] Random events loaded.");
      } else {
         try {
            Utility.copyFile(randomeventsIn, randomEventFile);
         } catch (Exception var26) {
            log.info("[DynamicEconomy] IOException when creating randomevents.yml in Main");
            var26.printStackTrace();
         }

         log.info("[DynamicEconomy] randomevents.yml created.");
      }

      if (usersFile.exists()) {
         log.info("[DynamicEconomy] User Settings loaded.");
      } else {
         try {
            usersConfig.save(usersFile);
            usersConfig.load(usersFile);
         } catch (Exception var25) {
            log.info("[DynamicEconomy] IOException when creating users.yml in Main");
            var25.printStackTrace();
         }

         log.info("[DynamicEconomy] users.yml created.");
      }

      if (aliasFile.exists()) {
         log.info("[DynamicEconomy] Alias list loaded.");
      } else {
         try {
            Utility.copyFile(aliasIn, aliasFile);
         } catch (Exception var24) {
            log.info("[DynamicEconomy] IOException when creating alias.yml in Main");
            var24.printStackTrace();
         }

         log.info("[DynamicEconomy] alias.yml created.");
      }

      if (groupsFile.exists()) {
         log.info("[DynamicEconomy] Item Groups loaded.");
      } else {
         try {
            Utility.copyFile(groupsIn, groupsFile);
         } catch (Exception var23) {
            log.info("[DynamicEconomy] IOException when creating groups.yml in Main");
            var23.printStackTrace();
         }

         log.info("[DynamicEconomy] groups.yml created.");
      }

      relConfig();

      try {
         Metrics metrics = new Metrics(plugin);
         metrics.start();
      } catch (IOException var22) {
         var22.printStackTrace();
      }

      if (usemysql) {
         mysql = new MySQL(log, "[DynamicEconomy]", sqlhost, sqlport, "DynamicEconomy", sqluser, sqlpw);
         if (mysql.checkConnection()) {
            connection = mysql.open();
            if (mysql.createTable("CREATE TABLE DynamicEconomyItems(item VARCHAR(50),PRIMARY KEY(item),id INT,price DECIMAL,floor DECIMAL,ceiling DECIMALdescription TEXT,stock INT,span DECIMALbuytime BIGINT,selltime BIGINT)")) {
               log.info("[DynamicEconomy] MySQL Items Table created.");
            }

            if (mysql.checkTable("DynamicEconomySigns")) {
               log.info("[DynamicEconomy] MySQL Signs Table loaded.");
            } else if (mysql.createTable("CREATE TABLE DynamicEconomySigns(coords VARCHAR(50),PRIMARY KEY(coords),world VARCHAR(50),item VARCHAR(50),type VARCHAR(50))")) {
               log.info("[DynamicEconomy] MySQL Signs Table created.");
            } else {
               log.info("[DynamicEconomy] Failed to create MySQL Signs table.");
            }

            if (mysql.checkTable("DynamicEconomyRegions")) {
               log.info("[DynamicEconomy] MySQL Regions Table loaded.");
            } else if (mysql.createTable("CREATE TABLE DynamicEconomyRegions(region VARCHAR(100),PRIMARY KEY(region),xMax SMALLINT,xMin SMALLINT,yMax SMALLINT,yMin SMALLINTzMax SMALLINT,zMin SMALLINT,purchasetax DECIMALsalestax DECIMAL)")) {
               log.info("[DynamicEconomy] MySQL Regions Table created.");
            } else {
               log.info("[DynamicEconomy] Failed to create MySQL Regions table.");
            }

            if (mysql.checkTable("DynamicEconomyRegionGroups")) {
               log.info("[DynamicEconomy] MySQL RegionGroups loaded.");
            } else if (mysql.createTable("CREATE TABLE DynamicEconomyRegionGroups(id INT NOT NULL AUTO_INCREMENT,PRIMARY KEY(id),region VARCHAR(100),type VARCHAR(50),group VARCHAR(100))")) {
               log.info("[DynamicEconomy] MySQL RegionGroups Table created.");
            } else {
               log.info("[DynamicEconomy] Failed to create MySQL RegionGroups table.");
            }

            if (mysql.checkTable("DynamicEconomyRegionBanned")) {
               log.info("[DynamicEconomy] MySQL RegionBanned Table loaded.");
            } else if (mysql.createTable("CREATE TABLE DynamicEconomyRegionBanned(id INT NOT NULL AUTO_INCREMENT,PRIMARY KEY(id),region VARCHAR(100),type VARCHAR(50),item VARCHAR(100))")) {
               log.info("[DynamicEconomy] MySQL RegionBanned Table created.");
            } else {
               log.info("[DynamicEconomy] Failed to create MySQL RegionBanned table.");
            }

            if (mysql.checkTable("DynamicEconomyLoans")) {
               log.info("[DynamicEconomy] MySQL Loans Table loaded.");
            } else {
               mysql.createTable("createTable(DynamicEconomyLoans)");
            }

            if (mysql.checkTable("DynamicEconomyUsers")) {
               log.info("[DynamicEconomy] MySQL Users Table loaded.");
            } else {
               mysql.createTable("createTable(DynamicEconomyUsers)");
            }

            if (mysql.checkTable("DynamicEconomyAlias")) {
               log.info("[DynamicEconomy] MySQL Alias Table loaded.");
            } else {
               mysql.createTable("createTable(DynamicEconomyAlias)");
            }

            if (mysql.checkTable("DynamicEconomyGroups")) {
               log.info("[DynamicEconomy] MySQL Groups Table loaded.");
            } else {
               mysql.createTable("createTable(DynamicEconomyGroups)");
            }
         } else {
            log.info("[DynamicEconomy] Could not connect to MySQL database. ");
            Utility.writeToLog("Could not connect to MySQL database");
         }
      }

      if (useLoans) {
         this.getServer().getScheduler().scheduleSyncRepeatingTask(this, new loan(), 300L, loanCheckInterval);
         log.info("[DynamicEconomy] Loan Thread Spawned with delay of " + loanCheckInterval + " ticks");
      }

      long delay;
      if (enableOverTimePriceDecay || enableOverTimePriceInflation) {
         delay = overTimePriceChangePeriodCheck * 60L * 20L;
         this.getServer().getScheduler().scheduleSyncRepeatingTask(this, new Transaction(), 300L, delay);
         log.info("[DynamicEconomy] Price Change Thread Spawned with delay of " + delay + " ticks");
      }

      if (enableRandomEvents) {
         delay = (long)(randomEventInterval * 60 * 20);
         this.getServer().getScheduler().scheduleSyncRepeatingTask(this, new RandomEvent(), 300L, delay);
         log.info("[DynamicEconomy] Random Events Thread Spawned with delay of " + delay + " ticks");
      }

      this.commandExec = new DynamicEconomyCommandExecutor(this, pdf, config, configFile);
      this.commandExec.setUpdater(this.updater);
      this.commandExec.fullver = fullVer;
      this.commandExec.subver = subVer;
      this.getCommand("setfloor").setExecutor(this.commandExec);
      this.getCommand("setceiling").setExecutor(this.commandExec);
      this.getCommand("getfloor").setExecutor(this.commandExec);
      this.getCommand("getceiling").setExecutor(this.commandExec);
      this.getCommand("getspan").setExecutor(this.commandExec);
      this.getCommand("setspan").setExecutor(this.commandExec);
      this.getCommand("dynamiceconomy").setExecutor(this.commandExec);
      this.getCommand("addstock").setExecutor(this.commandExec);
      this.getCommand("dynamiceconomyreloadconfig").setExecutor(this.commandExec);
      this.getCommand("removestock").setExecutor(this.commandExec);
      this.getCommand("getdurability").setExecutor(this.commandExec);
      this.getCommand("dynecon").setExecutor(this.commandExec);
      this.getCommand("hasupdate").setExecutor(this.commandExec);
      this.getCommand("dyneconupdate").setExecutor(this.commandExec);
      this.getCommand("curtaxes").setExecutor(this.commandExec);
      this.getCommand("settax").setExecutor(this.commandExec);
      this.getCommand("shopregion").setExecutor(this.commandExec);
      this.getCommand("removeshopregion").setExecutor(this.commandExec);
      this.getCommand("expandreg").setExecutor(this.commandExec);
      this.getCommand("contractreg").setExecutor(this.commandExec);
      this.getCommand("shopregionwand").setExecutor(this.commandExec);
      this.getCommand("curselectedregion").setExecutor(this.commandExec);
      this.getCommand("curregion").setExecutor(this.commandExec);
      this.getCommand("loan").setExecutor(this.commandExec);
      this.getCommand("curinterest").setExecutor(this.commandExec);
      this.getCommand("curloans").setExecutor(this.commandExec);
      this.getCommand("curworld").setExecutor(this.commandExec);
      this.getCommand("banitem").setExecutor(this.commandExec);
      this.getCommand("unbanitem").setExecutor(this.commandExec);
      this.getCommand("marketquiet").setExecutor(this.commandExec);
      this.getCommand("togglewand").setExecutor(this.commandExec);
      this.getCommand("addalias").setExecutor(this.commandExec);
      this.getCommand("removealias").setExecutor(this.commandExec);
      this.getCommand("creategroup").setExecutor(this.commandExec);
      this.getCommand("addtogroup").setExecutor(this.commandExec);
      this.getCommand("removefromgroup").setExecutor(this.commandExec);
      this.getCommand("removegroup").setExecutor(this.commandExec);
      this.getCommand("canibuy").setExecutor(this.commandExec);
      this.getCommand("canisell").setExecutor(this.commandExec);
      this.getCommand("addgrouptouser").setExecutor(this.commandExec);
      this.getCommand("removegroupfromuser").setExecutor(this.commandExec);
      this.getCommand("viewgroup").setExecutor(this.commandExec);
      this.getCommand("addregiongroup").setExecutor(this.commandExec);
      this.getCommand("removeregiongroup").setExecutor(this.commandExec);
      this.getCommand("banregionitem").setExecutor(this.commandExec);
      this.getCommand("unbanregionitem").setExecutor(this.commandExec);
      this.getCommand("buyenchantment").setExecutor(this.commandExec);
      this.getCommand("sellenchantment").setExecutor(this.commandExec);
      this.getCommand("priceenchantment").setExecutor(this.commandExec);
      this.getCommand("renewpricing").setExecutor(this.commandExec);
      if (altCommands) {
         this.getCommand("debuy").setExecutor(this.commandExec);
         this.getCommand("desell").setExecutor(this.commandExec);
         this.getCommand("deprice").setExecutor(this.commandExec);
      } else {
         this.getCommand("buy").setExecutor(this.commandExec);
         this.getCommand("sell").setExecutor(this.commandExec);
         this.getCommand("price").setExecutor(this.commandExec);
      }

      pm.registerEvents(this.playerListener, this);
   }

   private Boolean setupEconomy() {
      RegisteredServiceProvider economyProvider = this.getServer().getServicesManager().getRegistration(Economy.class);
      if (economyProvider != null) {
         economy = (Economy)economyProvider.getProvider();
      }

      return economy != null ? true : false;
   }

   private Boolean setupPermissions() {
      RegisteredServiceProvider permissionProvider = this.getServer().getServicesManager().getRegistration(Permission.class);
      if (permissionProvider != null) {
         permission = (Permission)permissionProvider.getProvider();
      }

      return permission != null ? true : false;
   }

   public static void reloadConfigValues(Player player, String[] args) {
      relConfig();
      color.sendColouredMessage(player, "&2Configuration for DynamicEconomy reloaded");
   }

   public static void relConfig() {
      try {
         config.load(configFile);
         messagesConfig.load(messagesFile);
         itemConfig.load(itemsFile);
         regionConfig.load(regionFile);
         signsConfig.load(signsFile);
         usersConfig.load(usersFile);
         aliasConfig.load(aliasFile);
         groupsConfig.load(groupsFile);
         shopsConfig.load(shopsFile);
      } catch (Exception var1) {
         log.info("[DynamicEconomy] Error loading config.yml in reloadConfigValues() ");
         log.info(var1.toString());
         var1.printStackTrace();
      }

      prefix = config.getString("prefix", "");
      defaultAmount = config.getInt("default-amount", 1);
      localNotify = config.getBoolean("local-price-notify", true);
      globalNotify = config.getBoolean("global-price-notify", true);
      logwriting = config.getBoolean("log-writing", true);
      salestax = config.getDouble("salestax", 0.0D);
      purchasetax = config.getDouble("purchasetax", 0.0D);
      depositTax = config.getBoolean("deposit-tax-to-account", false);
      taxAccount = config.getString("account-name", "");
      location_restrict = config.getBoolean("location-restrict", false);
      minimum_y = config.getInt("minimum-y", 0);
      maximum_y = config.getInt("maximum-y", 0);
      altCommands = config.getBoolean("alt-commands", false);
      useRegions = config.getBoolean("use-regions", false);
      useLoans = config.getBoolean("use-loans", true);
      useStaticInterest = config.getBoolean("use-static-interest", false);
      dynamicCompressionRate = config.getDouble("dynamic-compression-rate", 0.0D);
      if (useStaticInterest) {
         interestRate = config.getDouble("interest-rate", 0.05D);
      } else {
         interestRate = config.getDouble("dynamic-interest-rate", 0.0D);
      }

      if (interestRate == 0.0D) {
         plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
            public void run() {
               loan.dynamicInterest(true);
            }
         }, 100L);
      }

      paybackTime = config.getInt("payback-time", 20);
      maxLoans = config.getInt("max-num-loans", 1);
      maxLoanAmount = config.getDouble("max-loan-amount", 500.0D);
      minLoanAmount = (double)config.getInt("min-loan-amount", 10);
      useLoanAccount = config.getBoolean("use-loan-account", false);
      loanAccountName = config.getString("loan-account-name", "");
      loanCheckInterval = config.getLong("loan-check-interval", 300L);
      enableUpdateChecker = config.getBoolean("enable-update-checker", true);
      bannedSaleItems = config.getString("banned-sale-items", "").split(",");
      bannedPurchaseItems = config.getString("banned-purchase-items", "").split(",");
      enableOverTimePriceDecay = config.getBoolean("enable-over-time-price-decay", true);
      overTimePriceDecayPercent = config.getDouble("over-time-price-decay-percent", 0.1D);
      enableOverTimePriceInflation = config.getBoolean("enable-over-time-price-inflation", true);
      overTimePriceInflationPercent = config.getDouble("over-time-price-inflation-percent", 0.1D);
      overTimePriceChangePeriod = config.getLong("over-time-price-change-period", 1440L);
      overTimePriceChangePeriodCheck = config.getLong("over-time-price-change-period-check", 15L);
      taxAccountIsBank = config.getBoolean("tax-account-is-bank", false);
      loanAccountIsBank = config.getBoolean("loan-account-is-bank", false);
      dyneconWorld = config.getString("dynecon-world", "world").split(",");
      currencySymbol = config.getString("currency-symbol", "$");
      enableRandomEvents = config.getBoolean("enable-random-events", true);
      randomEventInterval = config.getInt("random-event-interval", 10);
      randomEventChance = config.getDouble("random-event-chance", 0.1D);
      signTaglineColor = config.getString("sign-tagline-color", "&2");
      signInfoColor = config.getString("sign-info-color", "&f");
      signInvalidColor = config.getString("sign-invalid-color", "&c");
      groupControl = config.getBoolean("group-control", false);
      useRegionFlags = config.getBoolean("use-region-flags", true);
      usemysql = config.getBoolean("use-mysql", false);
      sqlhost = config.getString("hostname", "localhost");
      sqldb = config.getString("database", "minecraft");
      sqluser = config.getString("user", "root");
      sqlpw = config.getString("password", "root");
      sqlport = config.getString("port", "3306");
      isBuySellCommandsEnabled = config.getBoolean("enable-buysell-commands", true);
      Messages.getMessages();
      dataSigns.updateTaxSigns();
      dataSigns.updateColors();
      DynamicShop.updateColors();
   }

   public void onDisable() {
      log.info(this.name + " v" + this.version + " disabled!");

      try {
         Utility.out.close();
         Utility.bf.close();
         selectedCorners.clear();
         regionConfig.save(regionFile);
         loansFileConfig.save(loansFile);
         itemConfig.save(itemsFile);
         signsConfig.save(signsFile);
         usersConfig.save(usersFile);
         config.save(configFile);
         aliasConfig.save(aliasFile);
         groupsConfig.save(groupsFile);
         shopsConfig.save(shopsFile);
         this.getServer().getScheduler().cancelTasks(this);
      } catch (Exception var2) {
         log.info("[DynamicEconomy] Exception saving configuration files.");
      }

   }
}
