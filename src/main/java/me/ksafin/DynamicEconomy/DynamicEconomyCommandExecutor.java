package me.ksafin.DynamicEconomy;

import couk.Adamki11s.AutoUpdater.AUCore;
import couk.Adamki11s.Extras.Colour.ExtrasColour;
import java.io.File;
import java.util.ArrayList;
import java.util.logging.Logger;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommandYamlParser;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginDescriptionFile;

public class DynamicEconomyCommandExecutor implements CommandExecutor {
   private File ItemsFile;
   private FileConfiguration ItemConfig;
   private ExtrasColour color = new ExtrasColour();
   private PluginDescriptionFile pluginDescription;
   private FileConfiguration config;
   public AUCore updater = null;
   public double fullver;
   public double subver;
   public File confFile;
   public AUCore latestVerCheck = new AUCore("http://exampop.com/latest.html", Logger.getLogger("NULL"), "[DynamicEconomy]");

   public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
      Player player = null;
      if (sender instanceof Player) {
         player = (Player)sender;
      }

      if (!cmd.getName().equalsIgnoreCase("price") && !cmd.getName().equalsIgnoreCase("deprice")) {
         if (cmd.getName().equalsIgnoreCase("setfloor")) {
            if (DynamicEconomy.permission.has(player, "dynamiceconomy.setfloor")) {
               if (this.isInWorld(player)) {
                  Item.setFloor(player, args);
               } else {
                  this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.wrongWorld);
                  Utility.writeToLog(player.getName() + " tried to call /setfloor, but was in the wrong world.");
               }
            } else {
               this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.noPermission);
               Utility.writeToLog(player.getName() + " called /setfloor but did not have permission.");
            }

            return true;
         } else if (cmd.getName().equalsIgnoreCase("setceiling")) {
            if (DynamicEconomy.permission.has(player, "dynamiceconomy.setceiling")) {
               if (this.isInWorld(player)) {
                  Item.setCeiling(player, args);
               } else {
                  this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.wrongWorld);
                  Utility.writeToLog(player.getName() + " tried to call /setceiling, but was in the wrong world.");
               }
            } else {
               this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.noPermission);
               Utility.writeToLog(player.getName() + " called /setceiling but did not have permission.");
            }

            return true;
         } else if (cmd.getName().equalsIgnoreCase("getfloor")) {
            if (DynamicEconomy.permission.has(player, "dynamiceconomy.getfloor")) {
               if (this.isInWorld(player)) {
                  Item.getFloor(player, args);
               } else {
                  this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.wrongWorld);
                  Utility.writeToLog(player.getName() + " tried to call /getfloor, but was in the wrong world.");
               }
            } else {
               this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.noPermission);
               Utility.writeToLog(player.getName() + " called /getfloor but did not have permission.");
            }

            return true;
         } else if (cmd.getName().equalsIgnoreCase("getceiling")) {
            if (DynamicEconomy.permission.has(player, "dynamiceconomy.getceiling")) {
               if (this.isInWorld(player)) {
                  Item.getCeiling(player, args);
               } else {
                  this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.wrongWorld);
                  Utility.writeToLog(player.getName() + " tried to call /getceiling, but was in the wrong world.");
               }
            } else {
               this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.noPermission);
               Utility.writeToLog(player.getName() + " called /getceiling but did not have permission.");
            }

            return true;
         } else if (cmd.getName().equalsIgnoreCase("getspan")) {
            if (DynamicEconomy.permission.has(player, "dynamiceconomy.getspan")) {
               if (this.isInWorld(player)) {
                  Item.getSpan(player, args);
               } else {
                  this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.wrongWorld);
                  Utility.writeToLog(player.getName() + " tried to call /getspan, but was in the wrong world.");
               }
            } else {
               this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.noPermission);
               Utility.writeToLog(player.getName() + " called /getspan but did not have permission.");
            }

            return true;
         } else if (cmd.getName().equalsIgnoreCase("setspan")) {
            if (DynamicEconomy.permission.has(player, "dynamiceconomy.setspan")) {
               if (this.isInWorld(player)) {
                  Item.setSpan(player, args);
               } else {
                  this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.wrongWorld);
                  Utility.writeToLog(player.getName() + " tried to call /setspan, but was in the wrong world.");
               }
            } else {
               this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.noPermission);
               Utility.writeToLog(player.getName() + " called /setspan but did not have permission.");
            }

            return true;
         } else if (cmd.getName().equalsIgnoreCase("dynamiceconomy")) {
            if (DynamicEconomy.permission.has(player, "dynamiceconomy.dynamiceconomy")) {
               if (this.isInWorld(player)) {
                  this.commandList(player, args);
                  Utility.writeToLog(player.getName() + " called /dynamiceconomy for help");
               } else {
                  this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.wrongWorld);
                  Utility.writeToLog(player.getName() + " tried to call /dynamiceconomy, but was in the wrong world.");
               }
            } else {
               this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.noPermission);
               Utility.writeToLog(player.getName() + " called /dynamiceconomy but did not have permission.");
            }

            return true;
         } else if (cmd.getName().equalsIgnoreCase("dynecon")) {
            if (DynamicEconomy.permission.has(player, "dynamiceconomy.dynamiceconomy")) {
               if (this.isInWorld(player)) {
                  this.commandList(player, args);
                  Utility.writeToLog(player.getName() + " called /dynamiceconomy for help");
               } else {
                  this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.wrongWorld);
                  Utility.writeToLog(player.getName() + " tried to call /dynamiceconomy, but was in the wrong world.");
               }
            } else {
               this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.noPermission);
               Utility.writeToLog(player.getName() + " called /dynecon but did not have permission.");
            }

            return true;
         } else {
            Location loc;
            int y;
            if (!cmd.getName().equalsIgnoreCase("buy") && !cmd.getName().equalsIgnoreCase("debuy")) {
               if (!cmd.getName().equalsIgnoreCase("sell") && !cmd.getName().equalsIgnoreCase("desell")) {
                  if (cmd.getName().equalsIgnoreCase("addstock")) {
                     if (DynamicEconomy.permission.has(player, "dynamiceconomy.addstock")) {
                        if (this.isInWorld(player)) {
                           Transaction.addStock(player, args);
                        } else {
                           this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.wrongWorld);
                           Utility.writeToLog(player.getName() + " tried to call /addstock, but was in the wrong world.");
                        }
                     } else {
                        this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.noPermission);
                        Utility.writeToLog(player.getName() + " called /addstock but did not have permission.");
                     }

                     return true;
                  } else {
                     boolean isLatest;
                     if (cmd.getName().equalsIgnoreCase("dynamiceconomyreloadconfig")) {
                        isLatest = true;
                        boolean isInWorld = true;
                        String name = "Console";
                        if (player != null) {
                           isLatest = DynamicEconomy.permission.has(player, "dynamiceconomy.dynamiceconomyreloadconfig");
                           if (!this.isInWorld(player)) {
                              this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.wrongWorld);
                              Utility.writeToLog(player.getName() + " tried to call /dynamiceconomyreloadconfig, but was in the wrong world.");
                           }

                           name = player.getName();
                        }

                        if (isLatest) {
                           DynamicEconomy.reloadConfigValues(player, args);
                           Utility.writeToLog(name + " reloaded the DynamicEconomy config.yml");
                        } else {
                           this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.noPermission);
                           Utility.writeToLog(player.getName() + " called /dynamiceconomyreloadconfig but did not have permission.");
                        }

                        return true;
                     } else if (cmd.getName().equalsIgnoreCase("removestock")) {
                        if (DynamicEconomy.permission.has(player, "dynamiceconomy.removestock")) {
                           if (this.isInWorld(player)) {
                              Transaction.removeStock(player, args);
                           } else {
                              this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.wrongWorld);
                              Utility.writeToLog(player.getName() + " tried to call /removestock, but was in the wrong world.");
                           }
                        } else {
                           this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.noPermission);
                           Utility.writeToLog(player.getName() + " called /removestock but did not have permission.");
                        }

                        return true;
                     } else if (cmd.getName().equalsIgnoreCase("getdurability")) {
                        if (DynamicEconomy.permission.has(player, "dynamiceconomy.getdurability")) {
                           if (this.isInWorld(player)) {
                              Item.getDurCommand(player, args);
                           } else {
                              this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.wrongWorld);
                              Utility.writeToLog(player.getName() + " tried to call /getdurability, but was in the wrong world.");
                           }
                        } else {
                           this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.noPermission);
                           Utility.writeToLog(player.getName() + " called /getdurability but did not have permission.");
                        }

                        return true;
                     } else if (cmd.getName().equalsIgnoreCase("hasupdate")) {
                        if (DynamicEconomy.permission.has(player, "dynamiceconomy.update")) {
                           if (this.isInWorld(player)) {
                              if (DynamicEconomy.enableUpdateChecker) {
                                 isLatest = this.updater.checkVersion(this.fullver, this.subver, "DynamicEconomy");
                                 Utility.writeToLog(player.getName() + " called /hasupdate");
                                 if (!isLatest) {
                                    this.color.sendColouredMessage(player, DynamicEconomy.prefix + "&2New Version of DynamicEconomy Available!");
                                 } else {
                                    this.color.sendColouredMessage(player, DynamicEconomy.prefix + "&2You have the latest version of DynamicEconomy!");
                                 }
                              }
                           } else {
                              this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.wrongWorld);
                              Utility.writeToLog(player.getName() + " tried to call /hasupdate, but was in the wrong world.");
                           }
                        } else {
                           this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.noPermission);
                           Utility.writeToLog(player.getName() + " called /hasupdate but did not have permission.");
                        }

                        return true;
                     } else if (cmd.getName().equalsIgnoreCase("dyneconupdate")) {
                        isLatest = false;
                        if (sender instanceof Player) {
                           isLatest = DynamicEconomy.permission.has(player, "dynamiceconomy.update");
                        } else {
                           isLatest = true;
                        }

                        if (isLatest) {
                           if (this.isInWorld(player)) {
                              if (DynamicEconomy.enableUpdateChecker) {
                                 Utility.writeToLog(player.getName() + " called /dyneconupdate and downloaded the latest version of DynamicEconomy");
                                 this.latestVerCheck.checkVersion(0.0D, 0.0D, "NULL");
                                 String link = this.latestVerCheck.getReason();
                                 this.updater.forceDownload(link, "DynamicEconomy", player);
                              }
                           } else {
                              this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.wrongWorld);
                              Utility.writeToLog(player.getName() + " tried to call /dyneconupdate, but was in the wrong world.");
                           }
                        } else {
                           this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.noPermission);
                           Utility.writeToLog(player.getName() + " called /dyneconupdate but did not have permission.");
                        }

                        return true;
                     } else if (cmd.getName().equalsIgnoreCase("curtaxes")) {
                        if (DynamicEconomy.permission.has(player, "dynamiceconomy.curtaxes")) {
                           if (this.isInWorld(player)) {
                              Transaction.curTaxes(player, args);
                           } else {
                              this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.wrongWorld);
                              Utility.writeToLog(player.getName() + " tried to call /curtaxes, but was in the wrong world.");
                           }
                        } else {
                           this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.noPermission);
                           Utility.writeToLog(player.getName() + " called /curtaxes but did not have permission.");
                        }

                        return true;
                     } else if (cmd.getName().equalsIgnoreCase("settax")) {
                        if (DynamicEconomy.permission.has(player, "dynamiceconomy.settax")) {
                           if (this.isInWorld(player)) {
                              Transaction.setTaxes(player, args);
                           } else {
                              this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.wrongWorld);
                              Utility.writeToLog(player.getName() + " tried to call /settax, but was in the wrong world.");
                           }
                        } else {
                           this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.noPermission);
                           Utility.writeToLog(player.getName() + " called /settax but did not have permission.");
                        }

                        return true;
                     } else if (cmd.getName().equalsIgnoreCase("shopregion")) {
                        if (DynamicEconomy.permission.has(player, "dynamiceconomy.shopregion")) {
                           if (this.isInWorld(player)) {
                              regionUtils.createRegion(player, args);
                           } else {
                              this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.wrongWorld);
                              Utility.writeToLog(player.getName() + " tried to call /shopregion, but was in the wrong world.");
                           }
                        } else {
                           this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.noPermission);
                           Utility.writeToLog(player.getName() + " called /shopregion but didn't have permission");
                        }

                        return true;
                     } else if (cmd.getName().equalsIgnoreCase("removeshopregion")) {
                        if (DynamicEconomy.permission.has(player, "dynamiceconomy.shopregion")) {
                           if (this.isInWorld(player)) {
                              regionUtils.deleteShopRegion(player, args);
                           } else {
                              this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.wrongWorld);
                              Utility.writeToLog(player.getName() + " tried to call /shopregion, but was in the wrong world.");
                           }
                        } else {
                           this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.noPermission);
                           Utility.writeToLog(player.getName() + " called /removeshopregion but didn't have permission");
                        }

                        return true;
                     } else if (cmd.getName().equalsIgnoreCase("expandreg")) {
                        if (DynamicEconomy.permission.has(player, "dynamiceconomy.shopregion")) {
                           if (this.isInWorld(player)) {
                              regionUtils.expandRegion(player, args);
                           } else {
                              this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.wrongWorld);
                              Utility.writeToLog(player.getName() + " tried to call /expandreg, but was in the wrong world.");
                           }
                        } else {
                           this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.noPermission);
                           Utility.writeToLog(player.getName() + " called /shopregion but didn't have permission");
                        }

                        return true;
                     } else if (cmd.getName().equalsIgnoreCase("contractreg")) {
                        if (DynamicEconomy.permission.has(player, "dynamiceconomy.shopregion")) {
                           if (this.isInWorld(player)) {
                              regionUtils.contractRegion(player, args);
                           } else {
                              this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.wrongWorld);
                              Utility.writeToLog(player.getName() + " tried to call /contractreg, but was in the wrong world.");
                           }
                        } else {
                           this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.noPermission);
                           Utility.writeToLog(player.getName() + " called /shopregion but didn't have permission");
                        }

                        return true;
                     } else if (cmd.getName().equalsIgnoreCase("shopregionwand")) {
                        if (DynamicEconomy.permission.has(player, "dynamiceconomy.shopregion")) {
                           if (this.isInWorld(player)) {
                              regionUtils.wand(player, args);
                           } else {
                              this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.wrongWorld);
                              Utility.writeToLog(player.getName() + " tried to call /shopregionwand, but was in the wrong world.");
                           }
                        } else {
                           this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.noPermission);
                           Utility.writeToLog(player.getName() + " called /shopregionwand but didn't have permission");
                        }

                        return true;
                     } else if (cmd.getName().equalsIgnoreCase("curselectedregion")) {
                        if (DynamicEconomy.permission.has(player, "dynamiceconomy.shopregion")) {
                           if (this.isInWorld(player)) {
                              regionUtils.getCorners(player, args);
                           } else {
                              this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.wrongWorld);
                              Utility.writeToLog(player.getName() + " tried to call /curregion, but was in the wrong world.");
                           }
                        } else {
                           this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.noPermission);
                           Utility.writeToLog(player.getName() + " called /curregion but didn't have permission");
                        }

                        return true;
                     } else if (cmd.getName().equalsIgnoreCase("loan")) {
                        if (DynamicEconomy.permission.has(player, "dynamiceconomy.loan")) {
                           if (this.isInWorld(player)) {
                              if (DynamicEconomy.useLoans) {
                                 loan.lend(player, args);
                              } else {
                                 this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.loansDisabled);
                                 Utility.writeToLog(player.getName() + " called /loan but loans are disabled.");
                              }
                           } else {
                              this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.wrongWorld);
                              Utility.writeToLog(player.getName() + " tried to call /loan, but was in the wrong world.");
                           }
                        } else {
                           this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.noPermission);
                           Utility.writeToLog(player.getName() + " called /loan but didn't have permission");
                        }

                        return true;
                     } else if (cmd.getName().equalsIgnoreCase("curinterest")) {
                        if (DynamicEconomy.permission.has(player, "dynamiceconomy.curinterest")) {
                           if (this.isInWorld(player)) {
                              if (DynamicEconomy.useLoans) {
                                 loan.getInterest(player, args);
                              } else {
                                 this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.loansDisabled);
                                 Utility.writeToLog(player.getName() + " called /curinterest but loans are disabled.");
                              }
                           } else {
                              this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.wrongWorld);
                              Utility.writeToLog(player.getName() + " tried to call /curinterest, but was in the wrong world.");
                           }
                        } else {
                           this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.noPermission);
                           Utility.writeToLog(player.getName() + " called /curinterest but didn't have permission");
                        }

                        return true;
                     } else if (cmd.getName().equalsIgnoreCase("curloans")) {
                        if (DynamicEconomy.permission.has(player, "dynamiceconomy.loan")) {
                           if (this.isInWorld(player)) {
                              if (DynamicEconomy.useLoans) {
                                 loan.getLoans(player, args);
                              } else {
                                 this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.loansDisabled);
                                 Utility.writeToLog(player.getName() + " called /curloans but loans are disabled.");
                              }
                           } else {
                              this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.wrongWorld);
                              Utility.writeToLog(player.getName() + " tried to call /curinterest, but was in the wrong world.");
                           }
                        } else {
                           this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.noPermission);
                           Utility.writeToLog(player.getName() + " called /curloans but didn't have permission");
                        }

                        return true;
                     } else if (cmd.getName().equalsIgnoreCase("curworld")) {
                        if (DynamicEconomy.permission.has(player, "dynamiceconomy.curworld")) {
                           this.curWorld(player);
                        } else {
                           this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.noPermission);
                           Utility.writeToLog(player.getName() + " called /curworld but didn't have permission");
                        }

                        return true;
                     } else if (cmd.getName().equalsIgnoreCase("banItem")) {
                        if (DynamicEconomy.permission.has(player, "dynamiceconomy.banItem")) {
                           if (this.isInWorld(player)) {
                              Item.banItem(player, args);
                           } else {
                              this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.wrongWorld);
                              Utility.writeToLog(player.getName() + " tried to call /banItem, but was in the wrong world.");
                           }
                        } else {
                           this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.noPermission);
                           Utility.writeToLog(player.getName() + " called /banItem but did not have permission.");
                        }

                        return true;
                     } else if (cmd.getName().equalsIgnoreCase("unbanItem")) {
                        if (DynamicEconomy.permission.has(player, "dynamiceconomy.banItem")) {
                           if (this.isInWorld(player)) {
                              Item.unbanItem(player, args);
                           } else {
                              this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.wrongWorld);
                              Utility.writeToLog(player.getName() + " tried to call /unbanItem, but was in the wrong world.");
                           }
                        } else {
                           this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.noPermission);
                           Utility.writeToLog(player.getName() + " called /unbanItem but did not have permission.");
                        }

                        return true;
                     } else if (cmd.getName().equalsIgnoreCase("marketquiet")) {
                        if (DynamicEconomy.permission.has(player, "dynamiceconomy.marketquiet")) {
                           if (this.isInWorld(player)) {
                              Utility.makeQuiet(player);
                           } else {
                              this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.wrongWorld);
                              Utility.writeToLog(player.getName() + " tried to call /marketquiet, but was in the wrong world.");
                           }
                        } else {
                           this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.noPermission);
                           Utility.writeToLog(player.getName() + " called /marketquiet but did not have permission.");
                        }

                        return true;
                     } else if (cmd.getName().equalsIgnoreCase("togglewand")) {
                        if (DynamicEconomy.permission.has(player, "dynamiceconomy.selectregion")) {
                           if (this.isInWorld(player)) {
                              regionUtils.toggleWand(player, args);
                           } else {
                              this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.wrongWorld);
                              Utility.writeToLog(player.getName() + " tried to call /togglewand, but was in the wrong world.");
                           }
                        } else {
                           this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.noPermission);
                           Utility.writeToLog(player.getName() + " called /togglewand but did not have permission.");
                        }

                        return true;
                     } else if (cmd.getName().equalsIgnoreCase("addalias")) {
                        if (DynamicEconomy.permission.has(player, "dynamiceconomy.alias")) {
                           if (this.isInWorld(player)) {
                              Item.addAlias(player, args);
                           } else {
                              this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.wrongWorld);
                              Utility.writeToLog(player.getName() + " tried to call /addalias, but was in the wrong world.");
                           }
                        } else {
                           this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.noPermission);
                           Utility.writeToLog(player.getName() + " called /addalias but did not have permission.");
                        }

                        return true;
                     } else if (cmd.getName().equalsIgnoreCase("removealias")) {
                        if (DynamicEconomy.permission.has(player, "dynamiceconomy.alias")) {
                           if (this.isInWorld(player)) {
                              Item.removeAlias(player, args);
                           } else {
                              this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.wrongWorld);
                              Utility.writeToLog(player.getName() + " tried to call /removealias, but was in the wrong world.");
                           }
                        } else {
                           this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.noPermission);
                           Utility.writeToLog(player.getName() + " called /removealias but did not have permission.");
                        }

                        return true;
                     } else if (cmd.getName().equalsIgnoreCase("canibuy")) {
                        if (DynamicEconomy.permission.has(player, "dynamiceconomy.canibuy")) {
                           if (this.isInWorld(player)) {
                              Item.canIBuy(player, args);
                           } else {
                              this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.wrongWorld);
                              Utility.writeToLog(player.getName() + " tried to call /canibuy, but was in the wrong world.");
                           }
                        } else {
                           this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.noPermission);
                           Utility.writeToLog(player.getName() + " called /canibuy but did not have permission.");
                        }

                        return true;
                     } else if (cmd.getName().equalsIgnoreCase("canisell")) {
                        if (DynamicEconomy.permission.has(player, "dynamiceconomy.canisell")) {
                           if (this.isInWorld(player)) {
                              Item.canISell(player, args);
                           } else {
                              this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.wrongWorld);
                              Utility.writeToLog(player.getName() + " tried to call /canisell, but was in the wrong world.");
                           }
                        } else {
                           this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.noPermission);
                           Utility.writeToLog(player.getName() + " called /canisell but did not have permission.");
                        }

                        return true;
                     } else if (cmd.getName().equalsIgnoreCase("creategroup")) {
                        if (DynamicEconomy.permission.has(player, "dynamiceconomy.groups")) {
                           if (this.isInWorld(player)) {
                              Item.addItemGroup(player, args);
                           } else {
                              this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.wrongWorld);
                              Utility.writeToLog(player.getName() + " tried to call /creategroup, but was in the wrong world.");
                           }
                        } else {
                           this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.noPermission);
                           Utility.writeToLog(player.getName() + " called /creategroup but did not have permission.");
                        }

                        return true;
                     } else if (cmd.getName().equalsIgnoreCase("removegroup")) {
                        if (DynamicEconomy.permission.has(player, "dynamiceconomy.groups")) {
                           if (this.isInWorld(player)) {
                              Item.removeItemGroup(player, args);
                           } else {
                              this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.wrongWorld);
                              Utility.writeToLog(player.getName() + " tried to call /removegroup, but was in the wrong world.");
                           }
                        } else {
                           this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.noPermission);
                           Utility.writeToLog(player.getName() + " called /removegroup but did not have permission.");
                        }

                        return true;
                     } else if (cmd.getName().equalsIgnoreCase("addtogroup")) {
                        if (DynamicEconomy.permission.has(player, "dynamiceconomy.groups")) {
                           if (this.isInWorld(player)) {
                              Item.addItemToGroup(player, args);
                           } else {
                              this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.wrongWorld);
                              Utility.writeToLog(player.getName() + " tried to call /addtogroup, but was in the wrong world.");
                           }
                        } else {
                           this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.noPermission);
                           Utility.writeToLog(player.getName() + " called /addtogroup but did not have permission.");
                        }

                        return true;
                     } else if (cmd.getName().equalsIgnoreCase("removefromgroup")) {
                        if (DynamicEconomy.permission.has(player, "dynamiceconomy.groups")) {
                           if (this.isInWorld(player)) {
                              Item.removeItemFromGroup(player, args);
                           } else {
                              this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.wrongWorld);
                              Utility.writeToLog(player.getName() + " tried to call /removefromgroup, but was in the wrong world.");
                           }
                        } else {
                           this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.noPermission);
                           Utility.writeToLog(player.getName() + " called /removefromgroup but did not have permission.");
                        }

                        return true;
                     } else if (cmd.getName().equalsIgnoreCase("addgrouptouser")) {
                        if (DynamicEconomy.permission.has(player, "dynamiceconomy.groups")) {
                           if (this.isInWorld(player)) {
                              Item.addGroupToUser(player, args);
                           } else {
                              this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.wrongWorld);
                              Utility.writeToLog(player.getName() + " tried to call /addgrouptouser, but was in the wrong world.");
                           }
                        } else {
                           this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.noPermission);
                           Utility.writeToLog(player.getName() + " called /addgrouptouser but did not have permission.");
                        }

                        return true;
                     } else if (cmd.getName().equalsIgnoreCase("removegroupfromuser")) {
                        if (DynamicEconomy.permission.has(player, "dynamiceconomy.groups")) {
                           if (this.isInWorld(player)) {
                              Item.removeGroupFromUser(player, args);
                           } else {
                              this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.wrongWorld);
                              Utility.writeToLog(player.getName() + " tried to call /removegroupfromuser, but was in the wrong world.");
                           }
                        } else {
                           this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.noPermission);
                           Utility.writeToLog(player.getName() + " called /removegroupfromuser but did not have permission.");
                        }

                        return true;
                     } else if (cmd.getName().equalsIgnoreCase("viewgroup")) {
                        if (DynamicEconomy.permission.has(player, "dynamiceconomy.viewgroup")) {
                           if (this.isInWorld(player)) {
                              Item.viewGroup(player, args);
                           } else {
                              this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.wrongWorld);
                              Utility.writeToLog(player.getName() + " tried to call /viewgroup, but was in the wrong world.");
                           }
                        } else {
                           this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.noPermission);
                           Utility.writeToLog(player.getName() + " called /viewgroup but did not have permission.");
                        }

                        return true;
                     } else if (cmd.getName().equalsIgnoreCase("curregion")) {
                        if (DynamicEconomy.permission.has(player, "dynamiceconomy.curregion")) {
                           if (this.isInWorld(player)) {
                              regionUtils.curRegion(player, args);
                           } else {
                              this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.wrongWorld);
                              Utility.writeToLog(player.getName() + " tried to call /curregion, but was in the wrong world.");
                           }
                        } else {
                           this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.noPermission);
                           Utility.writeToLog(player.getName() + " called /curregion but did not have permission.");
                        }

                        return true;
                     } else if (cmd.getName().equalsIgnoreCase("addregiongroup")) {
                        if (DynamicEconomy.permission.has(player, "dynamiceconomy.shopregion")) {
                           if (this.isInWorld(player)) {
                              regionUtils.addItemGroup(player, args);
                           } else {
                              this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.wrongWorld);
                              Utility.writeToLog(player.getName() + " tried to call /addregiongroup, but was in the wrong world.");
                           }
                        } else {
                           this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.noPermission);
                           Utility.writeToLog(player.getName() + " called /addregiongroup but did not have permission.");
                        }

                        return true;
                     } else if (cmd.getName().equalsIgnoreCase("removeregiongroup")) {
                        if (DynamicEconomy.permission.has(player, "dynamiceconomy.shopregion")) {
                           if (this.isInWorld(player)) {
                              regionUtils.removeItemGroup(player, args);
                           } else {
                              this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.wrongWorld);
                              Utility.writeToLog(player.getName() + " tried to call /removeregiongroup, but was in the wrong world.");
                           }
                        } else {
                           this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.noPermission);
                           Utility.writeToLog(player.getName() + " called /removeregiongroup but did not have permission.");
                        }

                        return true;
                     } else if (cmd.getName().equalsIgnoreCase("banregionItem")) {
                        if (DynamicEconomy.permission.has(player, "dynamiceconomy.shopregion")) {
                           if (this.isInWorld(player)) {
                              regionUtils.banRegionItem(player, args);
                           } else {
                              this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.wrongWorld);
                              Utility.writeToLog(player.getName() + " tried to call /banregionItem, but was in the wrong world.");
                           }
                        } else {
                           this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.noPermission);
                           Utility.writeToLog(player.getName() + " called /banregionItem but did not have permission.");
                        }

                        return true;
                     } else if (cmd.getName().equalsIgnoreCase("unbanregionItem")) {
                        if (DynamicEconomy.permission.has(player, "dynamiceconomy.shopregion")) {
                           if (this.isInWorld(player)) {
                              regionUtils.unbanRegionItem(player, args);
                           } else {
                              this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.wrongWorld);
                              Utility.writeToLog(player.getName() + " tried to call /unbanregionItem, but was in the wrong world.");
                           }
                        } else {
                           this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.noPermission);
                           Utility.writeToLog(player.getName() + " called /unbanregionItem but did not have permission.");
                        }

                        return true;
                     } else if (cmd.getName().equalsIgnoreCase("buyenchantment")) {
                        if (!DynamicEconomy.isBuySellCommandsEnabled) {
                           this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.buyDisabled);
                           Utility.writeToLog(player.getName() + " tried to use /buyenchantment, but it's disabled.");
                           return true;
                        } else {
                           if (DynamicEconomy.permission.has(player, "dynamiceconomy.buyenchantment")) {
                              if (this.isInWorld(player)) {
                                 Transaction.buyEnchantment(player, args);
                              } else {
                                 this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.wrongWorld);
                                 Utility.writeToLog(player.getName() + " tried to call /buyenchantment, but was in the wrong world.");
                              }
                           } else {
                              this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.noPermission);
                              Utility.writeToLog(player.getName() + " called /buyenchantment but did not have permission.");
                           }

                           return true;
                        }
                     } else if (cmd.getName().equalsIgnoreCase("sellenchantment")) {
                        if (!DynamicEconomy.isBuySellCommandsEnabled) {
                           this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.buyDisabled);
                           Utility.writeToLog(player.getName() + " tried to use /buyenchantment, but it's disabled.");
                           return true;
                        } else {
                           if (DynamicEconomy.permission.has(player, "dynamiceconomy.sellenchantment")) {
                              if (this.isInWorld(player)) {
                                 Transaction.sellEnchantment(player, args);
                              } else {
                                 this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.wrongWorld);
                                 Utility.writeToLog(player.getName() + " tried to call /sellenchantment, but was in the wrong world.");
                              }
                           } else {
                              this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.noPermission);
                              Utility.writeToLog(player.getName() + " called /sellenchantment but did not have permission.");
                           }

                           return true;
                        }
                     } else if (cmd.getName().equalsIgnoreCase("priceenchantment")) {
                        if (DynamicEconomy.permission.has(player, "dynamiceconomy.priceenchantment")) {
                           if (this.isInWorld(player)) {
                              Item.priceEnchantment(player, args);
                           } else {
                              this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.wrongWorld);
                              Utility.writeToLog(player.getName() + " tried to call /priceenchantment, but was in the wrong world.");
                           }
                        } else {
                           this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.noPermission);
                           Utility.writeToLog(player.getName() + " called /priceenchantment but did not have permission.");
                        }

                        return true;
                     } else if (cmd.getName().equalsIgnoreCase("renewpricing")) {
                        if (DynamicEconomy.permission.has(player, "dynamiceconomy.renew")) {
                           if (this.isInWorld(player)) {
                              (new EnderEngine(Item.getAllInfo("STONE"))).reCalculatePrices();
                              this.color.sendColouredMessage(player, DynamicEconomy.prefix + "&2Prices for items updated!");
                           } else {
                              this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.wrongWorld);
                              Utility.writeToLog(player.getName() + " tried to call /renewpricing, but was in the wrong world.");
                           }
                        } else {
                           this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.noPermission);
                           Utility.writeToLog(player.getName() + " called /renewpricing but did not have permission.");
                        }

                        return true;
                     } else {
                        return false;
                     }
                  }
               } else if (!DynamicEconomy.isBuySellCommandsEnabled) {
                  this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.sellDisabled);
                  Utility.writeToLog(player.getName() + " tried to use /sell, but it's disabled.");
                  return true;
               } else {
                  if (DynamicEconomy.permission.has(player, "dynamiceconomy.sell")) {
                     if (this.isInWorld(player)) {
                        loc = player.getLocation();
                        y = loc.getBlockY();
                        if (DynamicEconomy.location_restrict) {
                           if (y <= DynamicEconomy.maximum_y & y >= DynamicEconomy.minimum_y) {
                              Transaction.sell(player, args);
                           } else if (y <= DynamicEconomy.minimum_y) {
                              this.color.sendColouredMessage(player, DynamicEconomy.prefix + "&2You are too deep underground to access the economy!.");
                              Utility.writeToLog(player.getName() + " called /sell but was too deep underground.");
                           } else if (y >= DynamicEconomy.maximum_y) {
                              this.color.sendColouredMessage(player, DynamicEconomy.prefix + "&2You are too high up to access the economy!.");
                              Utility.writeToLog(player.getName() + " called /sell but was too high up.");
                           }
                        } else {
                           Transaction.sell(player, args);
                        }
                     } else {
                        this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.wrongWorld);
                        Utility.writeToLog(player.getName() + " tried to call /sell, but was in the wrong world.");
                     }
                  } else {
                     this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.noPermission);
                     Utility.writeToLog(player.getName() + " called /sell but did not have permission.");
                  }

                  return true;
               }
            } else if (!DynamicEconomy.isBuySellCommandsEnabled) {
               this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.buyDisabled);
               Utility.writeToLog(player.getName() + " tried to use /buy, but it's disabled.");
               return true;
            } else {
               if (DynamicEconomy.permission.has(player, "dynamiceconomy.buy")) {
                  if (this.isInWorld(player)) {
                     loc = player.getLocation();
                     y = loc.getBlockY();
                     if (DynamicEconomy.location_restrict) {
                        if (y <= DynamicEconomy.maximum_y & y >= DynamicEconomy.minimum_y) {
                           Transaction.buy(player, args);
                        } else if (y <= DynamicEconomy.minimum_y) {
                           this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.belowMinY);
                           Utility.writeToLog(player.getName() + " called /buy but was too deep underground.");
                        } else if (y >= DynamicEconomy.maximum_y) {
                           this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.aboveMaxY);
                           Utility.writeToLog(player.getName() + " called /sell but was too high up.");
                        }
                     } else {
                        Transaction.buy(player, args);
                     }
                  } else {
                     this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.wrongWorld);
                     Utility.writeToLog(player.getName() + " tried to call /buy, but was in the wrong world.");
                  }
               } else {
                  this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.noPermission);
                  Utility.writeToLog(player.getName() + " called /buy but did not have permission.");
               }

               return true;
            }
         }
      } else {
         if (DynamicEconomy.permission.has(player, "dynamiceconomy.price")) {
            if (this.isInWorld(player)) {
               Item.getPrice(player, args);
            } else {
               this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.wrongWorld);
               Utility.writeToLog(player.getName() + " tried to call /price, but was in the wrong world.");
            }
         } else {
            this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.noPermission);
            Utility.writeToLog(player.getName() + " called /price but did not have permission.");
         }

         return true;
      }
   }

   public DynamicEconomyCommandExecutor(DynamicEconomy plugin, PluginDescriptionFile desc, FileConfiguration conf, File cf) {
      this.ItemsFile = new File(plugin.getDataFolder(), "Items.yml");
      this.ItemConfig = YamlConfiguration.loadConfiguration(this.ItemsFile);
      this.pluginDescription = desc;
      this.config = conf;
      this.confFile = cf;
   }

   public void commandList(Player player, String[] args) {
      ArrayList cmdList = (ArrayList)PluginCommandYamlParser.parse(DynamicEconomy.plugin);
      int length = cmdList.size();
      int numPages = (int)((double)length / 5.0D + 1.0D);
      int page;
      if (args.length == 0) {
         page = 1;
      } else {
         page = Integer.parseInt(args[0]);
      }

      int startCommand = page * 5 - 5;
      int endCommand = page * 5;
      if (endCommand > length) {
         endCommand = length;
      }

      this.color.sendColouredMessage(player, "&2---------&fDynamicEconomy Commands&2---------");

      for(int x = startCommand; x < endCommand; ++x) {
         Command cmd = (Command)cmdList.get(x);
         String command = cmd.getUsage();
         String desc = cmd.getDescription();
         String message = "&2" + command + " : &f" + desc;
         this.color.sendColouredMessage(player, message);
      }

      this.color.sendColouredMessage(player, "&2----------------&fPage &f" + page + "/" + numPages + "&2----------------");
   }

   public void getStockBoolean(Player player, String[] args) {
      if (args.length > 0) {
         this.color.sendColouredMessage(player, DynamicEconomy.prefix + "&2Wrong Command Usage. &f/isstock");
      } else {
         Boolean isStock = this.config.getBoolean("Use-Stock", true);
         if (isStock) {
            this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.stockOn);
         } else {
            this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.stockOff);
         }
      }

   }

   public void getBoundaryBoolean(Player player, String[] args) {
      if (args.length > 0) {
         this.color.sendColouredMessage(player, DynamicEconomy.prefix + "&2Wrong Command Usage. &f/isboundary");
      } else {
         Boolean isStock = this.config.getBoolean("Use-boundaries", true);
         if (isStock) {
            this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.boundariesOn);
         } else {
            this.color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.boundariesOff);
         }
      }

   }

   public boolean setUpdater(AUCore up) {
      this.updater = up;
      return true;
   }

   public boolean isInWorld(Player player) {
      World world = player.getWorld();
      String worldName = world.getName();
      String[] var7 = DynamicEconomy.dyneconWorld;
      int var6 = DynamicEconomy.dyneconWorld.length;

      for(int var5 = 0; var5 < var6; ++var5) {
         String worldIndex = var7[var5];
         if (worldName.equalsIgnoreCase(worldIndex)) {
            return true;
         }
      }

      return false;
   }

   public void curWorld(Player player) {
      World world = player.getWorld();
      String worldName = world.getName();
      this.color.sendColouredMessage(player, DynamicEconomy.prefix + "&2Your current world is: &f" + worldName);
   }
}
