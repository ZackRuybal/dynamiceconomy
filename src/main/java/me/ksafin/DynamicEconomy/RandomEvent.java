package me.ksafin.DynamicEconomy;

import couk.Adamki11s.Extras.Colour.ExtrasColour;
import java.text.DecimalFormat;
import java.util.Random;
import java.util.Set;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

public class RandomEvent implements Runnable {
   ExtrasColour color = new ExtrasColour();
   DecimalFormat format = new DecimalFormat("#.##");

   public void run() {
      double random = Math.random();
      if (random <= DynamicEconomy.randomEventChance) {
         FileConfiguration config = DynamicEconomy.randomEventConfig;
         ConfigurationSection events = config.getConfigurationSection("events");
         Set eventsSet = events.getKeys(false);
         String[] eventsArr = (String[])eventsSet.toArray(new String[eventsSet.size()]);
         String event = eventsArr[(new Random()).nextInt(eventsArr.length)];
         String node = "events." + event;
         ConfigurationSection eventSection = config.getConfigurationSection(node);
         String description = eventSection.getString("Description");
         String item = eventSection.getString("Item");
         int amount = eventSection.getInt("Amount");
         String[] itemInfo = Item.getAllInfo(item);
         String trueName = itemInfo[0];
         Double price = Double.parseDouble(itemInfo[1]);
         int stock = Integer.parseInt(itemInfo[5]);
         EnderEngine engine = new EnderEngine(itemInfo);
         engine.incrementStock(amount);
         engine.updateConfig();
         double newPrice = engine.getPrice();
         int newStock = engine.getStock();
         double changePrice = newPrice - price;
         int changeStock = newStock - stock;
         String strPrice = this.format.format(newPrice);
         String strChange = this.format.format(changePrice);
         if (changePrice > 0.0D) {
            strChange = "+" + strChange;
         }

         Player[] var29;
         int var28 = (var29 = Bukkit.getServer().getOnlinePlayers()).length;

         for(int var27 = 0; var27 < var28; ++var27) {
            Player p = var29[var27];
            if (!Utility.isQuiet(p)) {
               this.color.sendColouredMessage(p, DynamicEconomy.prefix + description);
               this.color.sendColouredMessage(p, DynamicEconomy.prefix + "&2New Price of &f" + trueName + "&2 is &f" + DynamicEconomy.currencySymbol + strPrice + "&2 (" + strChange + ")");
            }
         }

         dataSigns.checkForUpdates(item, changeStock, changePrice);
      }

   }
}
