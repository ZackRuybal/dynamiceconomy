package me.ksafin.DynamicEconomy;

import couk.Adamki11s.Extras.Colour.ExtrasColour;
import couk.Adamki11s.Extras.Inventory.ExtrasInventory;
import java.io.File;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.logging.Logger;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class Transaction implements Runnable {
   private static ExtrasColour color = new ExtrasColour();
   private static ExtrasInventory inv = new ExtrasInventory();
   private static Logger log = Logger.getLogger("Minecraft");
   public static FileConfiguration regionConfigFile;
   static NumberFormat f;
   public static DecimalFormat decFormat;
   public static DecimalFormat changeFormat;

   static {
      f = NumberFormat.getNumberInstance(Locale.US);
      decFormat = (DecimalFormat)f;
      changeFormat = (DecimalFormat)f;
   }

   public static boolean buy(Player player, String[] args) {
      String stringPlay = player.getName();
      if (args.length != 0 && args.length <= 2) {
         boolean withinRegion = true;
         double tax = DynamicEconomy.purchasetax;
         String[] itemInfo = new String[7];

         try {
            itemInfo = Item.getAllInfo(args[0]);
         } catch (Exception var36) {
            color.sendColouredMessage(player, DynamicEconomy.prefix + "&2You entered the command arguments in the wrong order, or your item name was invalid ");
            Utility.writeToLog(stringPlay + " entered an invalid item, or entered command arguments in the wrong order");
            return false;
         }

         String itemName = itemInfo[0];
         double itemPrice = Double.parseDouble(itemInfo[1]);
         int itemStock = Integer.parseInt(itemInfo[5]);
         long itemID = Long.parseLong(itemInfo[6]);
         if (itemID >= 2500L && itemID < 2600L) {
            color.sendColouredMessage(player, DynamicEconomy.prefix + "&f" + itemName + "&2 is an enchantment. Use &f/buyenchantment&2 to buy it.");
            Utility.writeToLog(stringPlay + "tried to buy the enchantment " + itemName + " via /buy instead of /buyenchantment.");
            return false;
         } else {
            String bannedItem;
            int emptySlotAmount;
            int reqSlot;
            if (DynamicEconomy.useRegions) {
               Location loc = player.getLocation();
               emptySlotAmount = loc.getBlockX();
               reqSlot = loc.getBlockY();
               int z = loc.getBlockZ();
               withinRegion = regionUtils.withinRegion(emptySlotAmount, reqSlot, z);
               if (!withinRegion) {
                  color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.notWithinRegion);
                  Utility.writeToLog(stringPlay + " called /buy outside of an economy region.");
                  return false;
               }

               if (DynamicEconomy.useRegionFlags) {
                  String reg = regionUtils.getRegion(emptySlotAmount, reqSlot, z);
                  String node = "regions." + reg + ".flags";
                  tax = DynamicEconomy.regionConfig.getDouble(node + ".purchasetax");
                  String[] regionBannedItems = DynamicEconomy.regionConfig.getString(node + ".banned-purchase-items", "").split(",");

                  for(int i = 0; i < regionBannedItems.length; ++i) {
                     bannedItem = Item.getTrueName(regionBannedItems[i]);
                     if (bannedItem.equals(itemName)) {
                        color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.bannedInRegion);
                        Utility.writeToLog(stringPlay + " attempted to buy the banned item: " + bannedItem);
                        return false;
                     }
                  }

                  if (DynamicEconomy.groupControl) {
                     List allowedGroups = DynamicEconomy.regionConfig.getStringList(node + ".allowed-purchase-groups");
                     boolean inRegion = Item.isItemInRegionGroup(allowedGroups, itemName);
                     if (!inRegion) {
                        color.sendColouredMessage(player, DynamicEconomy.prefix + "&2You cannot buy &f" + itemName + "&2 in this region.");
                        Utility.writeToLog(stringPlay + " attempted to buy " + itemName + " in region " + reg + ", but it's not in any allowed item groups.");
                        return false;
                     }
                  }
               }
            }

            if (itemName.equals("")) {
               color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.itemDoesntExist);
               Utility.writeToLog(stringPlay + " attempted to buy the non-existent item '" + itemName + "'");
               return false;
            } else {
               int purchaseAmount;
               for(purchaseAmount = 0; purchaseAmount < DynamicEconomy.bannedPurchaseItems.length; ++purchaseAmount) {
                  bannedItem = Item.getTrueName(DynamicEconomy.bannedPurchaseItems[purchaseAmount]);
                  if (bannedItem.equals(itemName)) {
                     color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.bannedItem);
                     Utility.writeToLog(stringPlay + " attempted to buy the banned item: " + bannedItem);
                     return false;
                  }
               }

               if (DynamicEconomy.groupControl && !Item.canBuy(player, itemName)) {
                  color.sendColouredMessage(player, DynamicEconomy.prefix + "&2You are not permitted to purchase &f" + itemName);
                  Utility.writeToLog(stringPlay + " tried to purchase " + itemName + " but was denied access.");
                  return false;
               } else {
                  purchaseAmount = 0;
                  if (args.length == 1) {
                     purchaseAmount = DynamicEconomy.defaultAmount;
                  } else if (args.length == 2) {
                     if (args[1].equalsIgnoreCase("all")) {
                        purchaseAmount = itemStock;
                     } else {
                        try {
                           purchaseAmount = Integer.parseInt(args[1]);
                        } catch (Exception var35) {
                           color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.invalidCommandArgs);
                           Utility.writeToLog(stringPlay + " entered an invalid purchase amount, or entered command arguments in the wrong order.");
                           return false;
                        }
                     }
                  }

                  emptySlotAmount = inv.getEmptySlots(player);
                  int reqSlot = false;
                  if ((itemID < 256L || itemID > 258L) && (itemID < 267L || itemID > 279L) && (itemID < 298L || itemID > 317L) && (itemID < 283L || itemID > 286L) && (itemID < 290L || itemID > 294L)) {
                     reqSlot = (int)((double)purchaseAmount / 64.0D + 0.99D);
                  } else {
                     reqSlot = purchaseAmount;
                  }

                  if (reqSlot > emptySlotAmount) {
                     color.sendColouredMessage(player, DynamicEconomy.prefix + "&2You need &f" + reqSlot + "&2 empty slots, but have &f" + emptySlotAmount);
                     Utility.writeToLog(stringPlay + " attempted to buy " + purchaseAmount + " of '" + itemName + "', but didn't have enough space.");
                     return false;
                  } else if (purchaseAmount <= 0) {
                     color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.negativeBuyAmount);
                     Utility.writeToLog(stringPlay + " attempted to buy " + purchaseAmount + " of '" + itemName + "', but this amount is invalid.");
                     return false;
                  } else {
                     double balance = DynamicEconomy.economy.getBalance(player.getName());
                     if (itemStock < purchaseAmount) {
                        color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.notEnoughStock);
                        color.sendColouredMessage(player, DynamicEconomy.prefix + "&2Your request: &f" + decFormat.format((long)purchaseAmount) + "   &2Current Stock: &f" + itemStock);
                        Utility.writeToLog(stringPlay + " attempted to buy " + decFormat.format((long)purchaseAmount) + " of '" + itemName + "' but there was only " + itemStock + " remaining");
                        return false;
                     } else {
                        EnderEngine engine = new EnderEngine(itemInfo);
                        double totalCost = engine.getCost(purchaseAmount);
                        double newPrice = engine.getPrice();
                        int newStock = engine.getStock();
                        engine.setBuyTime();
                        double change = newPrice - itemPrice;
                        int changeStock = false;
                        int changeStock = newStock - itemStock;
                        double percentTax = tax * 100.0D;
                        tax *= totalCost;
                        totalCost += tax;
                        if (DynamicEconomy.depositTax) {
                           try {
                              if (DynamicEconomy.taxAccountIsBank) {
                                 DynamicEconomy.economy.bankDeposit(DynamicEconomy.taxAccount, tax);
                              } else {
                                 DynamicEconomy.economy.depositPlayer(DynamicEconomy.taxAccount, tax);
                              }
                           } catch (Exception var34) {
                              log.info("Tax-Account " + DynamicEconomy.taxAccount + " not found.");
                              Utility.writeToLog("Attempted to deposit tax of " + DynamicEconomy.currencySymbol + tax + " to account " + DynamicEconomy.taxAccount + " but account not found.");
                           }
                        }

                        decFormat.applyPattern("#.##");
                        if (balance < totalCost) {
                           color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.notEnoughMoney);
                           color.sendColouredMessage(player, DynamicEconomy.prefix + "&2Your balance: &f" + DynamicEconomy.currencySymbol + decFormat.format(balance) + "   &2Your order total: &f" + DynamicEconomy.currencySymbol + decFormat.format(totalCost));
                           Utility.writeToLog(stringPlay + " attempted to buy " + decFormat.format((long)purchaseAmount) + " of '" + itemName + "' for " + decFormat.format(totalCost) + " but could not afford it.");
                           return false;
                        } else {
                           DynamicEconomy.economy.withdrawPlayer(player.getName(), totalCost);
                           color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.purchaseSuccess);
                           Utility.writeToLog(stringPlay + " bought " + purchaseAmount + " of '" + itemName + "' for " + totalCost);
                           totalCost = Double.valueOf(decFormat.format(totalCost));
                           itemPrice = Double.valueOf(decFormat.format(itemPrice));
                           color.sendColouredMessage(player, DynamicEconomy.prefix + "&fYou bought " + purchaseAmount + " &2of " + itemName + "&f + " + decFormat.format(percentTax) + "&2% tax = &f" + DynamicEconomy.currencySymbol + totalCost + " &2TOTAL");
                           changeFormat.applyPattern("#.#####");
                           newPrice = Double.valueOf(decFormat.format(newPrice));
                           change = Double.valueOf(changeFormat.format(change));
                           if (itemPrice != newPrice) {
                              if (DynamicEconomy.globalNotify) {
                                 Player[] var33;
                                 int var32 = (var33 = Bukkit.getServer().getOnlinePlayers()).length;

                                 for(int var31 = 0; var31 < var32; ++var31) {
                                    Player p = var33[var31];
                                    if (!Utility.isQuiet(p) && !p.equals(player)) {
                                       color.sendColouredMessage(p, DynamicEconomy.prefix + "&2New Price of &f" + itemName + "&2 is &f" + DynamicEconomy.currencySymbol + newPrice + "&2 (+" + change + ")");
                                    }
                                 }
                              }

                              if (DynamicEconomy.localNotify) {
                                 color.sendColouredMessage(player, DynamicEconomy.prefix + "&2New Price of &f" + itemName + "&2 is &f" + DynamicEconomy.currencySymbol + newPrice + "&2 (+" + change + ")");
                              }

                              Utility.writeToLog(DynamicEconomy.prefix + " New price of " + itemName + " changed dynamically to " + newPrice + "(+" + change + ")");
                           }

                           int mat = false;
                           short dmg = false;
                           byte data = false;
                           if (itemID > 3000L) {
                              int mat = getMat(itemID);
                              short dmg = getDmg(itemID);
                              ItemStack items = new ItemStack(mat, purchaseAmount, dmg);
                              player.getInventory().addItem(new ItemStack[]{items});
                           } else if (itemID == 999L) {
                              player.giveExp(purchaseAmount);
                           } else if (itemID >= 256L && itemID <= 258L || itemID >= 267L && itemID <= 279L || itemID >= 298L && itemID <= 317L || itemID >= 283L && itemID <= 286L || itemID >= 290L && itemID <= 294L) {
                              for(int x = 0; x < purchaseAmount; ++x) {
                                 inv.addToInventory(player, (int)itemID, 1);
                              }
                           } else {
                              inv.addToInventory(player, (int)itemID, purchaseAmount);
                           }

                           player.updateInventory();
                           engine.updateConfig();
                           dataSigns.checkForUpdates(itemName, changeStock, change);
                           DynamicShop.updateItem(itemName);
                           return true;
                        }
                     }
                  }
               }
            }
         }
      } else {
         color.sendColouredMessage(player, DynamicEconomy.prefix + "&2Wrong Command Usage. &f/buy [Item] (Amount)");
         Utility.writeToLog(stringPlay + " incorrectly called /buy");
         return false;
      }
   }

   private static int getMat(long itemID) {
      String idStr = String.valueOf(itemID);
      String[] split = idStr.split("00");
      int mat = Integer.parseInt(split[0]);
      return mat;
   }

   private static short getDmg(long itemID) {
      String idStr = String.valueOf(itemID);
      String[] split = idStr.split("00");
      short dmg = Short.parseShort(split[1]);
      return dmg;
   }

   public static boolean sellInventory(Player player) {
      decFormat.setGroupingUsed(false);
      Inventory inv = player.getInventory();
      ItemStack[] contents = inv.getContents();
      String[] itemInfo = new String[7];
      double totalSale = 0.0D;
      double maxDur = 0.0D;
      double tax = DynamicEconomy.salestax;
      String itemSearchID = "";
      int changeStock = false;
      ArrayList banned = new ArrayList();
      int y;
      int z;
      if (DynamicEconomy.useRegions) {
         Location loc = player.getLocation();
         int x = loc.getBlockX();
         y = loc.getBlockY();
         z = loc.getBlockZ();
         boolean withinRegion = regionUtils.withinRegion(x, y, z);
         if (!withinRegion) {
            color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.notWithinRegion);
            Utility.writeToLog(player.getName() + " called /buy outside of an economy region.");
            return false;
         }

         if (DynamicEconomy.useRegionFlags) {
            String reg = regionUtils.getRegion(x, y, z);
            String node = "regions." + reg + ".flags";
            tax = DynamicEconomy.regionConfig.getDouble(node + ".salestax", 0.0D);
         }
      }

      for(int x = 0; x < DynamicEconomy.bannedSaleItems.length; ++x) {
         banned.add(DynamicEconomy.bannedSaleItems[x]);
      }

      ItemStack[] var52 = contents;
      z = contents.length;

      label140:
      for(y = 0; y < z; ++y) {
         ItemStack item = var52[y];
         if (item != null) {
            double dur = (double)item.getDurability();
            int itemID = item.getTypeId();
            if (dur != 0.0D && (itemID < 256 || itemID > 258) && (itemID < 267 || itemID > 279) && (itemID < 298 || itemID > 317) && (itemID < 283 || itemID > 286) && (itemID < 290 || itemID > 294)) {
               itemSearchID = item.getTypeId() + ":" + (int)dur;
            } else {
               itemSearchID = String.valueOf(item.getTypeId());
            }

            itemInfo = Item.getAllInfo(itemSearchID);
            String itemName = itemInfo[0];
            double itemPrice = Double.parseDouble(itemInfo[1]);
            int itemStock = Integer.parseInt(itemInfo[5]);
            int saleAmount = item.getAmount();
            if (DynamicEconomy.groupControl && !Item.canSell(player, itemName)) {
               color.sendColouredMessage(player, DynamicEconomy.prefix + "&2You are not permitted to sell &f" + itemName);
               Utility.writeToLog(player.getName() + " tried to sell " + itemName + " but was denied access.");
            } else {
               int newStock;
               String reg;
               if (DynamicEconomy.useRegions) {
                  Location loc = player.getLocation();
                  int x = loc.getBlockX();
                  newStock = loc.getBlockY();
                  int z = loc.getBlockZ();
                  if (DynamicEconomy.useRegionFlags) {
                     reg = regionUtils.getRegion(x, newStock, z);
                     String node = "regions." + reg + ".flags";
                     String[] regionBannedItems = DynamicEconomy.regionConfig.getString(node + ".banned-sale-items", "").split(",");

                     for(int i = 0; i < regionBannedItems.length; ++i) {
                        String bannedItem = Item.getTrueName(regionBannedItems[i]);
                        if (bannedItem.equals(itemName)) {
                           color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.bannedInRegion);
                           Utility.writeToLog(player.getName() + " attempted to sell the banned item: " + bannedItem);
                           continue label140;
                        }
                     }

                     if (DynamicEconomy.groupControl) {
                        List allowedGroups = DynamicEconomy.regionConfig.getStringList(node + ".allowed-sale-groups");
                        boolean inRegion = Item.isItemInRegionGroup(allowedGroups, itemName);
                        if (!inRegion) {
                           color.sendColouredMessage(player, DynamicEconomy.prefix + "&2You cannot sell &f" + itemName + "&2 in this region.");
                           Utility.writeToLog(player.getName() + " attempted to sell " + itemName + " in region " + reg + ", but it's not in any allowed item groups.");
                           continue;
                        }
                     }
                  }
               }

               if (!banned.contains(itemName)) {
                  double newPrice = 0.0D;
                  newStock = 0;
                  EnderEngine engine = new EnderEngine(itemInfo);
                  if ((itemID < 256 || itemID > 258) && (itemID < 267 || itemID > 279) && (itemID < 298 || itemID > 317) && (itemID < 283 || itemID > 286) && (itemID < 290 || itemID > 294)) {
                     totalSale += engine.getSale(saleAmount);
                     newPrice = engine.getPrice();
                     newStock = engine.getStock();
                     engine.setSellTime();
                  } else {
                     engine.incrementStock(1);
                     double playerDur = (double)item.getDurability();
                     reg = item.getType().toString();
                     maxDur = (double)Item.getMaxDur(reg);
                     playerDur = maxDur - playerDur;
                     double percentDur = playerDur / maxDur;
                     totalSale += engine.getPrice() * percentDur;
                  }

                  engine.updateConfig();
                  double change = newPrice - itemPrice;
                  int changeStock = newStock - itemStock;
                  dataSigns.checkForUpdates(itemName, changeStock, change);
                  DynamicShop.updateItem(itemName);
                  inv.removeItem(new ItemStack[]{item});
               }
            }
         }
      }

      double percentTax = tax * 100.0D;
      tax *= totalSale;
      if (DynamicEconomy.depositTax) {
         try {
            if (DynamicEconomy.taxAccountIsBank) {
               DynamicEconomy.economy.bankDeposit(DynamicEconomy.taxAccount, tax);
            } else {
               DynamicEconomy.economy.depositPlayer(DynamicEconomy.taxAccount, tax);
            }
         } catch (Exception var47) {
            log.info("Tax-Account " + DynamicEconomy.taxAccount + " not found.");
            Utility.writeToLog("Attempted to deposit tax of " + DynamicEconomy.currencySymbol + tax + " to account " + DynamicEconomy.taxAccount + " but account not found.");
         }
      }

      DynamicEconomy.economy.depositPlayer(player.getName(), totalSale);
      color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.saleSuccess);
      player.updateInventory();
      totalSale = Double.valueOf(decFormat.format(totalSale));
      totalSale -= tax;
      color.sendColouredMessage(player, DynamicEconomy.prefix + "&fYou sold&f your inventory &2 - " + decFormat.format(percentTax) + "&2% tax = &f" + DynamicEconomy.currencySymbol + totalSale + " &2TOTAL");
      Utility.writeToLog(DynamicEconomy.prefix + player.getName() + " sold his entire inventory for " + totalSale);
      return true;
   }

   public static boolean sell(Player player, String[] args) {
      String stringPlay = player.getName();
      if (args.length != 0 && args.length <= 2) {
         if (args[0].equalsIgnoreCase("inventory") && args.length == 1) {
            sellInventory(player);
            return true;
         } else {
            String[] itemInfo = new String[7];

            try {
               if (args.length == 1 && args[0].equals("hand")) {
                  ItemStack handItem = player.getInventory().getItemInHand();
                  if (handItem.getEnchantments().size() != 0) {
                     color.sendColouredMessage(player, DynamicEconomy.prefix + "&2This item is enchanted. Use &f/sellenchantment &2instead");
                     return false;
                  }

                  int dur = handItem.getDurability();
                  int id = handItem.getTypeId();
                  String name = "";
                  if (dur == 0) {
                     name = String.valueOf(id);
                  } else {
                     name = id + ":" + dur;
                  }

                  itemInfo = Item.getAllInfo(name);
               } else {
                  itemInfo = Item.getAllInfo(args[0]);
               }
            } catch (Exception var51) {
               color.sendColouredMessage(player, DynamicEconomy.prefix + "&2You entered the command arguments in the wrong order, or your item name was invalid ");
               Utility.writeToLog(stringPlay + " entered an invalid item, or entered command arguments in the wrong order");
               return false;
            }

            String itemName = itemInfo[0];
            double itemPrice = Double.parseDouble(itemInfo[1]);
            int itemStock = Integer.parseInt(itemInfo[5]);
            long itemID = Long.parseLong(itemInfo[6]);
            if (itemID >= 2500L && itemID < 2600L) {
               color.sendColouredMessage(player, DynamicEconomy.prefix + "&f" + itemName + "&2 is an enchantment. Use &f/sellenchantment&2 to sell it.");
               Utility.writeToLog(stringPlay + "tried to buy the enchantment " + itemName + " via /sell instead of /sellenchantment.");
               return false;
            } else {
               double tax = DynamicEconomy.salestax;
               String bannedItem;
               int mat;
               boolean data;
               if (DynamicEconomy.useRegions) {
                  Location loc = player.getLocation();
                  int x = loc.getBlockX();
                  mat = loc.getBlockY();
                  int z = loc.getBlockZ();
                  data = regionUtils.withinRegion(x, mat, z);
                  if (!data) {
                     color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.notWithinRegion);
                     Utility.writeToLog(stringPlay + " called /buy outside of an economy region.");
                     return false;
                  }

                  if (DynamicEconomy.useRegionFlags) {
                     String reg = regionUtils.getRegion(x, mat, z);
                     String node = "regions." + reg + ".flags";
                     tax = DynamicEconomy.regionConfig.getDouble(node + ".salestax");
                     String[] regionBannedItems = DynamicEconomy.regionConfig.getString(node + ".banned-sale-items", "").split(",");

                     for(int i = 0; i < regionBannedItems.length; ++i) {
                        bannedItem = Item.getTrueName(regionBannedItems[i]);
                        if (bannedItem.equals(itemName)) {
                           color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.bannedInRegion);
                           Utility.writeToLog(stringPlay + " attempted to sell the banned item: " + bannedItem);
                           return false;
                        }
                     }

                     if (DynamicEconomy.groupControl) {
                        List allowedGroups = DynamicEconomy.regionConfig.getStringList(node + ".allowed-sale-groups");
                        boolean inRegion = Item.isItemInRegionGroup(allowedGroups, itemName);
                        if (!inRegion) {
                           color.sendColouredMessage(player, DynamicEconomy.prefix + "&2You cannot sell &f" + itemName + "&2 in this region.");
                           Utility.writeToLog(stringPlay + " attempted to sell " + itemName + " in region " + reg + ", but it's not in any allowed item groups.");
                           return false;
                        }
                     }
                  }
               }

               if (DynamicEconomy.groupControl && !Item.canSell(player, itemName)) {
                  color.sendColouredMessage(player, DynamicEconomy.prefix + "&2You are not permitted to sell &f" + itemName);
                  Utility.writeToLog(stringPlay + " tried to sell " + itemName + " but was denied access.");
                  return false;
               } else if (itemName.equals("")) {
                  color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.itemDoesntExist);
                  Utility.writeToLog(stringPlay + " attempted to sell the non-existent item '" + itemName + "'");
                  return false;
               } else {
                  int saleAmount;
                  for(saleAmount = 0; saleAmount < DynamicEconomy.bannedSaleItems.length; ++saleAmount) {
                     bannedItem = Item.getTrueName(DynamicEconomy.bannedSaleItems[saleAmount]);
                     if (bannedItem.equals(itemName)) {
                        color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.bannedItem);
                        Utility.writeToLog(stringPlay + " attempted to sell the banned item: " + bannedItem);
                        return false;
                     }
                  }

                  saleAmount = 0;
                  boolean isAll = false;
                  int mat = false;
                  short dmg = false;
                  data = false;
                  int userAmount = false;
                  short dmg;
                  ItemStack saleItem;
                  if (args.length == 1) {
                     if (args[0].equals("hand")) {
                        saleAmount = player.getInventory().getItemInHand().getAmount();
                        if (saleAmount == 0) {
                           color.sendColouredMessage(player, DynamicEconomy.prefix + "&2You have no item in your hand.");
                           Utility.writeToLog(stringPlay + " called /sell hand, but had no item in hand.");
                           return false;
                        }
                     } else {
                        saleAmount = DynamicEconomy.defaultAmount;
                     }
                  } else if (args.length == 2) {
                     if (args[1].equalsIgnoreCase("all")) {
                        if (itemID > 3000L) {
                           mat = getMat(itemID);
                           dmg = getDmg(itemID);
                           saleItem = new ItemStack(mat, saleAmount, dmg);
                           saleAmount = getAmountOfDataValue(player, saleItem);
                        } else if (itemID == 999L) {
                           saleAmount = (int)player.getExp();
                        } else {
                           saleItem = new ItemStack((int)itemID);
                           saleAmount = getQuantity(player, saleItem);
                        }

                        isAll = true;
                     } else {
                        try {
                           saleAmount = Integer.parseInt(args[1]);
                        } catch (Exception var50) {
                           color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.invalidCommandArgs);
                           Utility.writeToLog(stringPlay + " entered an invalid purchase amount, or entered command arguments in the wrong order.");
                           return false;
                        }
                     }
                  }

                  if (saleAmount <= 0) {
                     color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.negativeSellAmount);
                     Utility.writeToLog(stringPlay + " attempted to sell " + saleAmount + " of '" + itemName + "', but this amount is invalid.");
                     return false;
                  } else {
                     double balance = DynamicEconomy.economy.getBalance(player.getName());
                     EnderEngine engine = new EnderEngine(itemInfo);
                     double totalSale = engine.getSale(saleAmount);
                     double newPrice = engine.getPrice();
                     int newStock = engine.getStock();
                     engine.setSellTime();
                     double change = newPrice - itemPrice;
                     int changeStock = newStock - itemStock;
                     int userAmount;
                     if (args[0].equalsIgnoreCase("hand")) {
                        userAmount = player.getInventory().getItemInHand().getAmount();
                     } else if (itemID > 3000L) {
                        mat = getMat(itemID);
                        dmg = getDmg(itemID);
                        saleItem = new ItemStack(mat, saleAmount, dmg);
                        userAmount = getAmountOfDataValue(player, saleItem);
                     } else if (itemID == 999L) {
                        float percExp1 = player.getExp();
                        player.giveExp(1);
                        float percExp2 = player.getExp();
                        float percUnit = percExp2 - percExp1;
                        player.setExp(percExp2 - percUnit);
                        int level = player.getLevel();
                        int originalLevel = player.getLevel();
                        float originalExp = player.getExp();
                        float curExp = player.getExp();

                        int expcount;
                        for(expcount = 0; level > 0; ++expcount) {
                           if (curExp <= 0.0F) {
                              --level;
                              player.setExp(0.5F);
                              percExp1 = player.getExp();
                              player.giveExp(1);
                              percExp2 = player.getExp();
                              percUnit = percExp2 - percExp1;
                              player.setExp(1.0F);
                              curExp = player.getExp();
                           }

                           curExp -= percUnit;
                        }

                        player.setLevel(originalLevel);
                        player.setExp(originalExp);
                        userAmount = expcount;
                     } else {
                        ItemStack i = new ItemStack((int)itemID);
                        userAmount = getQuantity(player, i);
                     }

                     if (isAll && saleAmount <= 0) {
                        color.sendColouredMessage(player, DynamicEconomy.prefix + "&2You have no &f" + itemName);
                        Utility.writeToLog(stringPlay + " attempted to sell all of their " + itemName + ", but had none.");
                        return false;
                     } else if (saleAmount > userAmount && !isAll) {
                        color.sendColouredMessage(player, DynamicEconomy.prefix + "&2You do not have &f" + saleAmount + " " + itemName);
                        Utility.writeToLog(stringPlay + " attempted to sell " + saleAmount + " of " + itemName + ", but didn't have that many.");
                        return false;
                     } else {
                        double percentTax = tax * 100.0D;
                        tax *= totalSale;
                        totalSale -= tax;
                        if (DynamicEconomy.depositTax) {
                           try {
                              if (DynamicEconomy.taxAccountIsBank) {
                                 DynamicEconomy.economy.bankDeposit(DynamicEconomy.taxAccount, tax);
                              } else {
                                 DynamicEconomy.economy.depositPlayer(DynamicEconomy.taxAccount, tax);
                              }
                           } catch (Exception var49) {
                              log.info("Tax-Account " + DynamicEconomy.taxAccount + " not found.");
                              Utility.writeToLog("Attempted to deposit tax of " + DynamicEconomy.currencySymbol + tax + " to account " + DynamicEconomy.taxAccount + " but account not found.");
                           }
                        }

                        DynamicEconomy.economy.depositPlayer(player.getName(), totalSale);
                        color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.saleSuccess);
                        decFormat.applyPattern("#.##");
                        if (itemID >= 256L && itemID <= 258L || itemID >= 267L && itemID <= 279L || itemID >= 298L && itemID <= 317L || itemID >= 283L && itemID <= 286L || itemID >= 290L && itemID <= 294L) {
                           totalSale = 0.0D;
                           HashMap itemsList = player.getInventory().all((int)itemID);
                           ArrayList keys = new ArrayList();
                           Iterator var79 = itemsList.keySet().iterator();

                           ItemStack cs;
                           Integer key;
                           while(var79.hasNext()) {
                              key = (Integer)var79.next();
                              cs = (ItemStack)itemsList.get(key);
                              if (cs.getEnchantments().size() > 0) {
                                 keys.add(key);
                              }
                           }

                           var79 = keys.iterator();

                           while(var79.hasNext()) {
                              key = (Integer)var79.next();
                              itemsList.remove(key);
                           }

                           int numSold = 0;

                           for(Iterator var82 = itemsList.values().iterator(); var82.hasNext(); ++numSold) {
                              Object s = var82.next();
                              if (numSold == saleAmount) {
                                 break;
                              }

                              cs = (ItemStack)s;
                              ItemStack itemStack = new ItemStack((int)itemID);
                              String itemMCname = itemStack.getType().toString();
                              double maxDur = (double)Item.getMaxDur(itemMCname);
                              double playerDur = (double)cs.getDurability();
                              playerDur = maxDur - playerDur;
                              double percentDur = playerDur / maxDur;
                              totalSale += itemPrice * percentDur;
                              double indivsale = itemPrice * percentDur;
                              removeInventoryItem(player.getInventory(), cs);
                              itemPrice = Double.valueOf(decFormat.format(itemPrice));
                              engine.incrementStock(1);
                              percentDur *= 100.0D;
                              percentDur = Double.valueOf(decFormat.format(percentDur));
                              indivsale = Double.valueOf(decFormat.format(indivsale));
                              color.sendColouredMessage(player, DynamicEconomy.prefix + "&f1 &2" + itemName + "&f with " + percentDur + "% &2durability = &2" + DynamicEconomy.currencySymbol + indivsale);
                              Utility.writeToLog(stringPlay + " sold a '" + itemName + "' at " + percentDur + "% durability for " + indivsale);
                           }

                           percentTax = tax * 100.0D;
                           tax = DynamicEconomy.salestax * totalSale;
                           totalSale -= tax;
                           totalSale = Double.valueOf(decFormat.format(totalSale));
                           color.sendColouredMessage(player, DynamicEconomy.prefix + "&2TOTAL SALE (with " + decFormat.format(percentTax) + "% tax): &f" + DynamicEconomy.currencySymbol + totalSale);
                        } else {
                           ItemStack i;
                           if (args[0].equalsIgnoreCase("hand")) {
                              i = player.getInventory().getItemInHand();
                              player.getInventory().removeItem(new ItemStack[]{i});
                           } else if (itemID > 3000L) {
                              mat = getMat(itemID);
                              dmg = getDmg(itemID);
                              i = new ItemStack(mat, saleAmount, dmg);
                              player.getInventory().removeItem(new ItemStack[]{i});
                           } else if (itemID != 999L) {
                              Material material = Material.getMaterial((int)itemID);
                              i = new ItemStack(material, saleAmount);
                              player.getInventory().removeItem(new ItemStack[]{i});
                           } else {
                              float percExp1 = player.getExp();
                              player.giveExp(1);
                              float percExp2 = player.getExp();
                              float percUnit = percExp2 - percExp1;
                              player.setExp(percExp2 - percUnit);
                              int level = player.getLevel();
                              float curExp = player.getExp();

                              for(int x = 0; x < saleAmount; ++x) {
                                 if (curExp <= 0.0F) {
                                    --level;
                                    if (level == 0) {
                                       player.setLevel(0);
                                       player.setExp(0.0F);
                                       break;
                                    }

                                    player.setExp(0.5F);
                                    percExp1 = player.getExp();
                                    player.giveExp(1);
                                    percExp2 = player.getExp();
                                    percUnit = percExp2 - percExp1;
                                    player.setExp(1.0F);
                                    curExp = player.getExp();
                                 }

                                 curExp -= percUnit;
                              }

                              player.setLevel(level);
                              player.setExp(curExp);
                           }

                           player.updateInventory();
                           totalSale = Double.valueOf(decFormat.format(totalSale));
                           itemPrice = Double.valueOf(decFormat.format(itemPrice));
                           newPrice = Double.valueOf(decFormat.format(newPrice));
                           color.sendColouredMessage(player, DynamicEconomy.prefix + "&fYou sold &2" + saleAmount + "&f of &2" + itemName + "&f - " + decFormat.format(percentTax) + "&2% tax = &f" + DynamicEconomy.currencySymbol + totalSale + " &2TOTAL");
                           Utility.writeToLog(stringPlay + " succesfully sold " + saleAmount + " of '" + itemName + "' for " + totalSale);
                        }

                        changeFormat.applyPattern("#.#####");
                        newPrice = Double.valueOf(decFormat.format(newPrice));
                        change = Double.valueOf(changeFormat.format(change));
                        if (itemPrice != newPrice) {
                           if (DynamicEconomy.globalNotify) {
                              Player[] var80;
                              int var76 = (var80 = Bukkit.getServer().getOnlinePlayers()).length;

                              for(int var74 = 0; var74 < var76; ++var74) {
                                 Player p = var80[var74];
                                 if (!Utility.isQuiet(p)) {
                                    color.sendColouredMessage(p, DynamicEconomy.prefix + "&2New Price of &f" + itemName + "&2 is &f" + DynamicEconomy.currencySymbol + newPrice + "&2 (" + change + ")");
                                 }
                              }
                           } else if (DynamicEconomy.localNotify) {
                              color.sendColouredMessage(player, DynamicEconomy.prefix + "&2New Price of &f" + itemName + "&2 is &f" + DynamicEconomy.currencySymbol + newPrice + "&2 (" + change + ")");
                           }

                           Utility.writeToLog(DynamicEconomy.prefix + " New price of " + itemName + " changed dynamically to " + newPrice + "(" + change + ")");
                        }

                        engine.updateConfig();
                        dataSigns.checkForUpdates(itemName, changeStock, change);
                        DynamicShop.updateItem(itemName);
                        return true;
                     }
                  }
               }
            }
         }
      } else {
         color.sendColouredMessage(player, DynamicEconomy.prefix + "&2Wrong Command Usage. &f/sell [Item] (Amount)");
         Utility.writeToLog(stringPlay + " incorrectly called /sell");
         return false;
      }
   }

   public static void removeInventoryItems(Inventory inv, ItemStack s) {
      ItemStack[] var5;
      int var4 = (var5 = inv.getContents()).length;

      for(int var3 = 0; var3 < var4; ++var3) {
         ItemStack is = var5[var3];
         if (is.getType().equals(s.getType()) && is.getEnchantments().size() == s.getEnchantments().size()) {
            inv.removeItem(new ItemStack[]{is});
         }
      }

   }

   public static void removeInventoryItem(Inventory inv, ItemStack s) {
      ItemStack[] var5;
      int var4 = (var5 = inv.getContents()).length;

      for(int var3 = 0; var3 < var4; ++var3) {
         ItemStack is = var5[var3];
         if (is != null && is.getType().equals(s.getType()) && is.getEnchantments().size() == s.getEnchantments().size()) {
            inv.removeItem(new ItemStack[]{is});
            return;
         }
      }

   }

   private static int getAmountOfDataValue(Player p, ItemStack m) {
      ItemStack[] invent = p.getInventory().getContents();
      int amount = 0;
      ItemStack[] var7 = invent;
      int var6 = invent.length;

      for(int var5 = 0; var5 < var6; ++var5) {
         ItemStack i = var7[var5];
         if (i != null && i.getTypeId() == m.getTypeId() && i.getDurability() == m.getDurability()) {
            amount += i.getAmount();
         }
      }

      return amount;
   }

   public static int getQuantity(Player p, ItemStack item) {
      ItemStack[] invent = p.getInventory().getContents();
      int amount = 0;
      ItemStack[] var7 = invent;
      int var6 = invent.length;

      for(int var5 = 0; var5 < var6; ++var5) {
         ItemStack i = var7[var5];
         if (i != null && i.getType().equals(item.getType()) && i.getEnchantments().equals(item.getEnchantments()) && i.getDurability() == item.getDurability()) {
            amount += i.getAmount();
         }
      }

      return amount;
   }

   public static void addStock(Player player, String[] args) {
      String stringPlay = player.getName();
      if (args.length >= 2 && args.length <= 2) {
         int addStock = 0;

         try {
            addStock = Integer.parseInt(args[1]);
         } catch (Exception var8) {
            color.sendColouredMessage(player, DynamicEconomy.prefix + "&2You entered a non-integer amount.");
            Utility.writeToLog("Player" + stringPlay + " tried to add a non integer stock amount");
            var8.printStackTrace();
         }

         String[] info = Item.getAllInfo(args[0]);
         EnderEngine engine = new EnderEngine(info);
         engine.incrementStock(addStock);
         engine.updateConfig();
         int newStock = engine.getStock();
         int oldStock = Integer.parseInt(info[5]);
         color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.stockAdded);
         color.sendColouredMessage(player, DynamicEconomy.prefix + "&2Previous Stock: &f" + oldStock + "&2 | New Stock: &f" + newStock);
         Utility.writeToLog(stringPlay + " added " + addStock + " stock of " + info[0] + " for a new stock total of " + newStock);
         dataSigns.checkForUpdates(info[0], newStock - oldStock, 0.0D);
      } else {
         color.sendColouredMessage(player, DynamicEconomy.prefix + "&2Wrong Command Usage. &f/addStock [Item] [AdditionalStock]");
         Utility.writeToLog(stringPlay + " incorrectly called /addstock");
      }

   }

   public static void removeStock(Player player, String[] args) {
      String stringPlay = player.getName();
      if (args.length >= 2 && args.length <= 2) {
         int removeStock = 0;

         try {
            removeStock = Integer.parseInt(args[1]);
         } catch (Exception var8) {
            color.sendColouredMessage(player, DynamicEconomy.prefix + "&2You entered a non-integer amount.");
            Utility.writeToLog("Player" + stringPlay + " tried to remove a non integer stock amount");
            var8.printStackTrace();
         }

         String[] info = Item.getAllInfo(args[0]);
         EnderEngine engine = new EnderEngine(info);
         engine.decrementStock(removeStock);
         engine.updateConfig();
         int newStock = engine.getStock();
         int oldStock = Integer.parseInt(info[5]);
         color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.stockRemoved);
         color.sendColouredMessage(player, DynamicEconomy.prefix + "&2Previous Stock: &f" + oldStock + "&2 | New Stock: &f" + newStock);
         Utility.writeToLog(stringPlay + " removed " + removeStock + " stock of " + info[0] + " for a new stock total of " + newStock);
         dataSigns.checkForUpdates(info[0], newStock - oldStock, 0.0D);
      } else {
         color.sendColouredMessage(player, DynamicEconomy.prefix + "&2Wrong Command Usage. &f/addStock [Item] [AdditionalStock]");
         Utility.writeToLog(stringPlay + " incorrectly called /removestock");
      }

   }

   public static void curTaxes(Player player, String[] args) {
      String stringPlay = player.getName();
      if (args.length > 0) {
         color.sendColouredMessage(player, DynamicEconomy.prefix + "&2Wrong Command Usage. &f/curTaxes");
         Utility.writeToLog(stringPlay + " incorrectly called /curTaxes");
      } else {
         color.sendColouredMessage(player, DynamicEconomy.prefix + "&fSales Tax: &2" + DynamicEconomy.salestax * 100.0D + "%");
         color.sendColouredMessage(player, DynamicEconomy.prefix + "&fPurchase Tax: &2" + DynamicEconomy.purchasetax * 100.0D + "%");
         Utility.writeToLog(stringPlay + " called /curtaxes");
      }

   }

   public static void setTaxes(Player player, String[] args) {
      String stringPlay = player.getName();
      if (args.length != 3) {
         color.sendColouredMessage(player, DynamicEconomy.prefix + "&2Wrong Command Usage. &f/settax [region] [sale|purchase] [amount]");
         Utility.writeToLog(stringPlay + " incorrectly called /settax");
      } else {
         String region = args[0].toUpperCase();
         Double tax = 0.0D;

         try {
            tax = Double.parseDouble(args[2]);
         } catch (Exception var9) {
            color.sendColouredMessage(player, DynamicEconomy.prefix + "&2 " + args[2] + "&f% is an invalid amount.");
            return;
         }

         if (!args[1].equalsIgnoreCase("sale") && !args[1].equalsIgnoreCase("purchase")) {
            color.sendColouredMessage(player, DynamicEconomy.prefix + "&2This is an invalid tax name. Use either &fsale or &fpurchase");
            Utility.writeToLog(stringPlay + " tried to set tax '" + args[1] + "', which doesn't exist.");
            return;
         }

         FileConfiguration conf;
         File file;
         if (region.equalsIgnoreCase("GLOBAL")) {
            if (args[1].equalsIgnoreCase("sale")) {
               DynamicEconomy.config.set("salestax", tax);
            } else if (args[1].equalsIgnoreCase("purchase")) {
               DynamicEconomy.config.set("purchasetax", tax);
            }

            conf = DynamicEconomy.config;
            file = DynamicEconomy.configFile;
         } else {
            if (!DynamicEconomy.regionConfig.contains("regions." + region)) {
               color.sendColouredMessage(player, DynamicEconomy.prefix + "&2Region &f" + region + "&2 doesn't exist.");
               Utility.writeToLog(stringPlay + " tried to set tax of region '" + region + "', which doesn't exist.");
               return;
            }

            String node = "regions." + region + ".flags";
            if (args[1].equalsIgnoreCase("sale")) {
               node = node + ".salestax";
            } else if (args[1].equalsIgnoreCase("purchase")) {
               node = node + ".purchasetax";
            }

            DynamicEconomy.regionConfig.set(node, tax);
            conf = DynamicEconomy.regionConfig;
            file = DynamicEconomy.regionFile;
         }

         try {
            conf.save(file);
         } catch (Exception var8) {
            log.info("[DynamicEconomy] Error saving config in /settax");
            var8.printStackTrace();
         }

         decFormat.applyPattern("###.##");
         color.sendColouredMessage(player, DynamicEconomy.prefix + "&2" + args[0] + " " + args[1] + "tax set to &f" + decFormat.format(tax * 100.0D) + "%");
         Utility.writeToLog(stringPlay + " set " + region + " " + args[1] + "tax to " + decFormat.format(tax * 100.0D));
         DynamicEconomy.relConfig();
      }

   }

   public static void buyEnchantment(Player player, String[] args) {
      if (args.length != 2) {
         color.sendColouredMessage(player, DynamicEconomy.prefix + "&2Wrong Command Usage. &f/buyEnchantment [Enchantment] [Level]");
         Utility.writeToLog(player.getName() + " incorrectly called /buyEnchantment");
      } else {
         String enchantment = args[0].toUpperCase();
         int level = false;
         int level;
         if (args[1].equalsIgnoreCase("I")) {
            level = 1;
         } else if (args[1].equalsIgnoreCase("II")) {
            level = 2;
         } else if (args[1].equalsIgnoreCase("III")) {
            level = 3;
         } else if (args[1].equalsIgnoreCase("IV")) {
            level = 4;
         } else if (args[1].equalsIgnoreCase("V")) {
            level = 5;
         } else {
            try {
               level = Integer.parseInt(args[1]);
            } catch (NumberFormatException var34) {
               color.sendColouredMessage(player, DynamicEconomy.prefix + "&f" + args[1] + "&2 is not a valid enchantment level. Use 1-5 or I-V.");
               Utility.writeToLog(player.getName() + " called /buyenchantment with invalid level " + args[1]);
               return;
            }
         }

         if (!DynamicEconomy.itemConfig.contains(enchantment)) {
            color.sendColouredMessage(player, DynamicEconomy.prefix + "&f" + enchantment + "&2 is not a valid enchantment.");
            Utility.writeToLog(player.getName() + " called /buyenchantment with invalid enchantment " + enchantment);
            return;
         }

         String[] enchantmentInfo = Item.getAllInfo(enchantment);
         int id = Integer.parseInt(enchantmentInfo[6]);
         int stock = Integer.parseInt(enchantmentInfo[5]);
         double itemPrice = Double.parseDouble(enchantmentInfo[1]);
         int enchantmentID = id % 2500;
         Enchantment enchant = Enchantment.getById(enchantmentID);
         int maxLevel = enchant.getMaxLevel();
         if (level > maxLevel) {
            color.sendColouredMessage(player, DynamicEconomy.prefix + "&fThe maximum level for this enchantment is &f" + maxLevel);
            Utility.writeToLog(player.getName() + " called /buyenchantment with a level above the maximum for " + enchantment);
            return;
         }

         double tax = DynamicEconomy.purchasetax;
         String bannedItem;
         if (DynamicEconomy.useRegions) {
            Location loc = player.getLocation();
            int x = loc.getBlockX();
            int y = loc.getBlockY();
            int z = loc.getBlockZ();
            boolean withinRegion = regionUtils.withinRegion(x, y, z);
            if (!withinRegion) {
               color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.notWithinRegion);
               Utility.writeToLog(player.getName() + " called /buyenchantment outside of an economy region.");
               return;
            }

            if (DynamicEconomy.useRegionFlags) {
               String reg = regionUtils.getRegion(x, y, z);
               String node = "regions." + reg + ".flags";
               tax = DynamicEconomy.regionConfig.getDouble(node + ".purchasetax");
               String[] regionBannedItems = DynamicEconomy.regionConfig.getString(node + ".banned-purchase-items", "").split(",");

               for(int i = 0; i < regionBannedItems.length; ++i) {
                  bannedItem = Item.getTrueName(regionBannedItems[i]);
                  if (bannedItem.equals(enchantment)) {
                     color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.bannedInRegion);
                     Utility.writeToLog(player.getName() + " attempted to buy the banned item: " + bannedItem);
                     return;
                  }
               }

               if (DynamicEconomy.groupControl) {
                  List allowedGroups = DynamicEconomy.regionConfig.getStringList(node + ".allowed-purchase-groups");
                  boolean inRegion = Item.isItemInRegionGroup(allowedGroups, enchantment);
                  if (!inRegion) {
                     color.sendColouredMessage(player, DynamicEconomy.prefix + "&2You cannot buy &f" + enchantment + "&2 in this region.");
                     Utility.writeToLog(player.getName() + " attempted to buy " + enchantment + " in region " + reg + ", but it's not in any allowed item groups.");
                     return;
                  }
               }
            }
         }

         for(int x = 0; x < DynamicEconomy.bannedPurchaseItems.length; ++x) {
            bannedItem = Item.getTrueName(DynamicEconomy.bannedPurchaseItems[x]);
            if (bannedItem.equals(enchantment)) {
               color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.bannedItem);
               Utility.writeToLog(player.getName() + " attempted to buy the banned item: " + bannedItem);
               return;
            }
         }

         if (DynamicEconomy.groupControl && !Item.canBuy(player, enchantment)) {
            color.sendColouredMessage(player, DynamicEconomy.prefix + "&2You are not permitted to purchase &f" + enchantment);
            Utility.writeToLog(player.getName() + " tried to purchase " + enchantment + " but was denied access.");
            return;
         }

         ItemStack enchantTarg = player.getItemInHand();
         decFormat.applyPattern("#.##");
         boolean canEnchant = enchant.canEnchantItem(enchantTarg);
         if (!canEnchant) {
            color.sendColouredMessage(player, DynamicEconomy.prefix + "&2The item you are holding cannot be given the enchantment &f" + enchantment);
            Utility.writeToLog(player.getName() + " called /buyenchantment " + enchantment + " for an item this enchantment cannot be applied to.");
            return;
         }

         if (stock == 0) {
            color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.notEnoughStock);
            Utility.writeToLog(player.getName() + " called /buyenchantment " + enchantment + " but there were none in stock.");
            return;
         }

         EnderEngine engine = new EnderEngine(enchantmentInfo);
         double totalCost = engine.getCost(1) * (double)level;
         double newPrice = engine.getPrice();
         int newStock = engine.getStock();
         double bal = DynamicEconomy.economy.getBalance(player.getName());
         if (totalCost > bal) {
            color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.notEnoughMoney);
            Utility.writeToLog(player.getName() + " called /buyenchantment " + enchantment + " but could not afford it.");
            return;
         }

         double percentTax = tax * 100.0D;
         tax *= totalCost;
         totalCost += tax;
         DynamicEconomy.economy.withdrawPlayer(player.getName(), totalCost);
         if (DynamicEconomy.depositTax) {
            try {
               if (DynamicEconomy.taxAccountIsBank) {
                  DynamicEconomy.economy.bankDeposit(DynamicEconomy.taxAccount, tax);
               } else {
                  DynamicEconomy.economy.depositPlayer(DynamicEconomy.taxAccount, tax);
               }
            } catch (Exception var33) {
               log.info("Tax-Account " + DynamicEconomy.taxAccount + " not found.");
               Utility.writeToLog("Attempted to deposit tax of " + DynamicEconomy.currencySymbol + tax + " to account " + DynamicEconomy.taxAccount + " but account not found.");
            }
         }

         color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.purchaseSuccess);
         Utility.writeToLog(player.getName() + " bought " + 1 + " of '" + enchantment + "' for " + totalCost);
         decFormat.setGroupingUsed(false);
         totalCost = Double.valueOf(decFormat.format(totalCost));
         itemPrice = Double.valueOf(decFormat.format(itemPrice));
         color.sendColouredMessage(player, DynamicEconomy.prefix + "&fYou bought " + 1 + " &2of " + enchantment + "&f + " + decFormat.format(percentTax) + "&2% tax = &f" + DynamicEconomy.currencySymbol + totalCost + " &2TOTAL");
         changeFormat.applyPattern("#.#####");
         newPrice = Double.valueOf(decFormat.format(newPrice));
         double change = Double.valueOf(changeFormat.format(newPrice - itemPrice));
         if (itemPrice != newPrice) {
            if (DynamicEconomy.globalNotify) {
               Player[] var32;
               int var31 = (var32 = Bukkit.getServer().getOnlinePlayers()).length;

               for(int var30 = 0; var30 < var31; ++var30) {
                  Player p = var32[var30];
                  if (!Utility.isQuiet(p)) {
                     color.sendColouredMessage(p, DynamicEconomy.prefix + "&2New Price of &f" + enchantment + "&2 is &f" + DynamicEconomy.currencySymbol + newPrice + "&2 (+" + change + ")");
                  }
               }
            } else if (DynamicEconomy.localNotify) {
               color.sendColouredMessage(player, DynamicEconomy.prefix + "&2New Price of &f" + enchantment + "&2 is &f" + DynamicEconomy.currencySymbol + newPrice + "&2 (+" + change + ")");
            }

            Utility.writeToLog(DynamicEconomy.prefix + " New price of " + enchantment + " changed dynamically to " + newPrice + "(+" + change + ")");
         }

         enchantTarg.addEnchantment(enchant, level);
         player.getInventory().setItemInHand(enchantTarg);
         player.updateInventory();
         engine.updateConfig();
         dataSigns.checkForUpdates(enchantment, stock - newStock, change);
         DynamicShop.updateItem(enchantment);
      }

   }

   public static void sellEnchantment(Player player, String[] args) {
      if (args.length != 1) {
         color.sendColouredMessage(player, DynamicEconomy.prefix + "&2Wrong Command Usage. &f/sellEnchantment [Enchantment]");
         Utility.writeToLog(player.getName() + " incorrectly called /sellEnchantment");
      } else {
         String enchantment = args[0].toUpperCase();
         int level = false;
         if (!DynamicEconomy.itemConfig.contains(enchantment)) {
            color.sendColouredMessage(player, DynamicEconomy.prefix + "&f" + enchantment + "&2 is not a valid enchantment.");
            Utility.writeToLog(player.getName() + " called /sellenchantment with invalid enchantment " + enchantment);
            return;
         }

         String[] enchantmentInfo = Item.getAllInfo(enchantment);
         int id = Integer.parseInt(enchantmentInfo[6]);
         int stock = Integer.parseInt(enchantmentInfo[5]);
         double itemPrice = Double.parseDouble(enchantmentInfo[1]);
         int enchantmentID = id % 2500;
         Enchantment enchant = Enchantment.getById(enchantmentID);
         double tax = DynamicEconomy.salestax;
         String bannedItem;
         if (DynamicEconomy.useRegions) {
            Location loc = player.getLocation();
            int x = loc.getBlockX();
            int y = loc.getBlockY();
            int z = loc.getBlockZ();
            boolean withinRegion = regionUtils.withinRegion(x, y, z);
            if (!withinRegion) {
               color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.notWithinRegion);
               Utility.writeToLog(player.getName() + " called /sellenchantment outside of an economy region.");
               return;
            }

            decFormat.applyPattern("###.##");
            if (DynamicEconomy.useRegionFlags) {
               String reg = regionUtils.getRegion(x, y, z);
               String node = "regions." + reg + ".flags";
               tax = DynamicEconomy.regionConfig.getDouble(node + ".salestax");
               String[] regionBannedItems = DynamicEconomy.regionConfig.getString(node + ".banned-sale-items", "").split(",");

               for(int i = 0; i < regionBannedItems.length; ++i) {
                  bannedItem = Item.getTrueName(regionBannedItems[i]);
                  if (bannedItem.equals(enchantment)) {
                     color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.bannedInRegion);
                     Utility.writeToLog(player.getName() + " attempted to sell the banned item: " + bannedItem);
                     return;
                  }
               }

               if (DynamicEconomy.groupControl) {
                  List allowedGroups = DynamicEconomy.regionConfig.getStringList(node + ".allowed-sale-groups");
                  boolean inRegion = Item.isItemInRegionGroup(allowedGroups, enchantment);
                  if (!inRegion) {
                     color.sendColouredMessage(player, DynamicEconomy.prefix + "&2You cannot sell &f" + enchantment + "&2 in this region.");
                     Utility.writeToLog(player.getName() + " attempted to sell " + enchantment + " in region " + reg + ", but it's not in any allowed item groups.");
                     return;
                  }
               }
            }
         }

         for(int x = 0; x < DynamicEconomy.bannedSaleItems.length; ++x) {
            bannedItem = Item.getTrueName(DynamicEconomy.bannedSaleItems[x]);
            if (bannedItem.equals(enchantment)) {
               color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.bannedItem);
               Utility.writeToLog(player.getName() + " attempted to sell the banned item: " + bannedItem);
               return;
            }
         }

         if (DynamicEconomy.groupControl && !Item.canSell(player, enchantment)) {
            color.sendColouredMessage(player, DynamicEconomy.prefix + "&2You are not permitted to sell &f" + enchantment);
            Utility.writeToLog(player.getName() + " tried to sell " + enchantment + " but was denied access.");
            return;
         }

         ItemStack enchantTarg = player.getItemInHand();
         if (!enchantTarg.getEnchantments().containsKey(enchant)) {
            color.sendColouredMessage(player, DynamicEconomy.prefix + "&2The item you are holding does not have the enchantment &f" + enchantment);
            Utility.writeToLog(player.getName() + " called /sellenchantment " + enchantment + " for an item which doesnt have this enchantment");
            return;
         }

         int level = (Integer)enchantTarg.getEnchantments().get(enchant);
         enchantTarg.removeEnchantment(enchant);
         EnderEngine engine = new EnderEngine(enchantmentInfo);
         double totalSale = engine.getSale(1) * (double)level;
         double newPrice = engine.getPrice();
         int newStock = engine.getStock();
         double percentTax = tax * 100.0D;
         tax *= totalSale;
         totalSale -= tax;
         player.getInventory().setItemInHand(enchantTarg);
         DynamicEconomy.economy.depositPlayer(player.getName(), totalSale);
         if (DynamicEconomy.depositTax) {
            try {
               if (DynamicEconomy.taxAccountIsBank) {
                  DynamicEconomy.economy.bankDeposit(DynamicEconomy.taxAccount, tax);
               } else {
                  DynamicEconomy.economy.depositPlayer(DynamicEconomy.taxAccount, tax);
               }
            } catch (Exception var29) {
               log.info("Tax-Account " + DynamicEconomy.taxAccount + " not found.");
               Utility.writeToLog("Attempted to deposit tax of " + DynamicEconomy.currencySymbol + tax + " to account " + DynamicEconomy.taxAccount + " but account not found.");
            }
         }

         color.sendColouredMessage(player, DynamicEconomy.prefix + Messages.saleSuccess);
         Utility.writeToLog(player.getName() + " sold " + 1 + " of '" + enchantment + "' for " + totalSale);
         decFormat.setGroupingUsed(false);
         totalSale = Double.valueOf(decFormat.format(totalSale));
         itemPrice = Double.valueOf(decFormat.format(itemPrice));
         color.sendColouredMessage(player, DynamicEconomy.prefix + "&2TOTAL SALE (with " + decFormat.format(percentTax) + "% tax): &f" + DynamicEconomy.currencySymbol + totalSale);
         changeFormat.applyPattern("#.#####");
         newPrice = Double.valueOf(decFormat.format(newPrice));
         double change = Double.valueOf(changeFormat.format(itemPrice - newPrice));
         if (itemPrice != newPrice) {
            if (DynamicEconomy.globalNotify) {
               Player[] var28;
               int var27 = (var28 = Bukkit.getServer().getOnlinePlayers()).length;

               for(int var26 = 0; var26 < var27; ++var26) {
                  Player p = var28[var26];
                  if (!Utility.isQuiet(p)) {
                     color.sendColouredMessage(p, DynamicEconomy.prefix + "&2New Price of &f" + enchantment + "&2 is &f" + DynamicEconomy.currencySymbol + newPrice + "&2 (-" + change + ")");
                  }
               }
            } else if (DynamicEconomy.localNotify) {
               color.sendColouredMessage(player, DynamicEconomy.prefix + "&2New Price of &f" + enchantment + "&2 is &f" + DynamicEconomy.currencySymbol + newPrice + "&2 (+" + change + ")");
            }

            Utility.writeToLog(DynamicEconomy.prefix + " New price of " + enchantment + " changed dynamically to " + newPrice + "(-" + change + ")");
         }

         player.updateInventory();
         engine.updateConfig();
         dataSigns.checkForUpdates(enchantment, stock - newStock, change);
         DynamicShop.updateItem(enchantment);
      }

   }

   public void run() {
      Set itemsSet = DynamicEconomy.itemConfig.getKeys(false);
      Object[] itemsObj = itemsSet.toArray();
      String[] items = new String[itemsObj.length];

      for(int i = 0; i < items.length; ++i) {
         items[i] = itemsObj[i].toString();
      }

      long period = DynamicEconomy.overTimePriceChangePeriod * 60L * 1000L;

      for(int x = 0; x < items.length; ++x) {
         long buyTime = DynamicEconomy.itemConfig.getLong(items[x] + ".buytime");
         long sellTime = DynamicEconomy.itemConfig.getLong(items[x] + ".selltime");
         Calendar.getInstance();
         long buyDifference = Calendar.getInstance().getTimeInMillis() - buyTime;
         Calendar.getInstance();
         long sellDifference = Calendar.getInstance().getTimeInMillis() - sellTime;
         EnderEngine engine = new EnderEngine(Item.getAllInfo(items[x]));
         double price;
         Player p;
         int var23;
         int var24;
         Player[] var25;
         if (DynamicEconomy.enableOverTimePriceDecay && buyDifference >= period && buyTime != 0L) {
            engine.decay();
            price = engine.getPrice();
            decFormat.applyPattern("#.##");
            if (DynamicEconomy.globalNotify) {
               var24 = (var25 = Bukkit.getServer().getOnlinePlayers()).length;

               for(var23 = 0; var23 < var24; ++var23) {
                  p = var25[var23];
                  if (!Utility.isQuiet(p)) {
                     price = Double.valueOf(decFormat.format(price));
                     color.sendColouredMessage(p, DynamicEconomy.prefix + "&2New Price of &f" + items[x] + "&2 is &f" + DynamicEconomy.currencySymbol + price + "&2 ( -" + DynamicEconomy.overTimePriceDecayPercent * 100.0D + "% )");
                  }
               }
            }

            DynamicEconomy.itemConfig.set(items[x] + ".buytime", Calendar.getInstance().getTimeInMillis());
         }

         if (DynamicEconomy.enableOverTimePriceInflation && sellDifference >= period && sellTime != 0L) {
            engine.inflate();
            price = engine.getPrice();
            decFormat.applyPattern("#.##");
            if (DynamicEconomy.globalNotify) {
               var24 = (var25 = Bukkit.getServer().getOnlinePlayers()).length;

               for(var23 = 0; var23 < var24; ++var23) {
                  p = var25[var23];
                  if (!Utility.isQuiet(p)) {
                     price = Double.valueOf(decFormat.format(price));
                     color.sendColouredMessage(p, DynamicEconomy.prefix + "&2New Price of &f" + items[x] + "&2 is &f" + DynamicEconomy.currencySymbol + price + "&2 ( +" + DynamicEconomy.overTimePriceInflationPercent * 100.0D + "% )");
                  }
               }
            }

            DynamicEconomy.itemConfig.set(items[x] + ".selltime", Calendar.getInstance().getTimeInMillis());
         }
      }

      try {
         DynamicEconomy.itemConfig.save(DynamicEconomy.itemsFile);
      } catch (Exception var26) {
         var26.printStackTrace();
      }

   }
}
