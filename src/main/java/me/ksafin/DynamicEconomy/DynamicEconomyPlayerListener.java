package me.ksafin.DynamicEconomy;

import couk.Adamki11s.AutoUpdater.AUCore;
import couk.Adamki11s.Extras.Colour.ExtrasColour;
import java.util.ArrayList;
import java.util.logging.Logger;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;

public class DynamicEconomyPlayerListener implements Listener {
   public static Permission permission = null;
   private final ExtrasColour color = new ExtrasColour();
   public AUCore updater = null;
   public double fullver;
   public double subver;
   private static Logger log = Logger.getLogger("Minecraft");
   public DynamicEconomy plugin;

   public DynamicEconomyPlayerListener(DynamicEconomy instance) {
      this.plugin = instance;
   }

   @EventHandler
   public void onPlayerJoin(PlayerJoinEvent event) {
      if (DynamicEconomy.enableUpdateChecker) {
         final Player player = event.getPlayer();
         final String stringPlay = player.getName();
         boolean latest = this.updater.checkVersion(this.fullver, this.subver, "DynamicEconomy");
         Utility.writeToLog(stringPlay + " logged in.");
         if (!latest && permission.has(player, "DynamicEconomy.update")) {
            this.plugin.getServer().getScheduler().scheduleSyncDelayedTask(this.plugin, new Runnable() {
               public void run() {
                  DynamicEconomyPlayerListener.this.color.sendColouredMessage(player, "&2New Version of DynamicEconomy Available!");
                  Utility.writeToLog(stringPlay + " notified of new DynamicEconomy version");
               }
            }, 60L);
         }
      }

      if (!DynamicEconomy.usersConfig.contains(event.getPlayer().getName() + ".PURCHASEGROUPS")) {
         ArrayList list = new ArrayList();
         list.add("*");
         ArrayList list2 = new ArrayList();
         list2.add("*");
         DynamicEconomy.usersConfig.set(event.getPlayer().getName() + ".PURCHASEGROUPS", list);
         DynamicEconomy.usersConfig.set(event.getPlayer().getName() + ".SALEGROUPS", list2);

         try {
            DynamicEconomy.usersConfig.save(DynamicEconomy.usersFile);
         } catch (Exception var5) {
            log.info("[DynamicEconomy] Error saving users config");
            log.info(var5.toString());
            var5.printStackTrace();
         }
      }

      if (!DynamicEconomy.usersConfig.contains(event.getPlayer().getName() + ".QUIET")) {
         DynamicEconomy.usersConfig.set(event.getPlayer().getName() + ".QUIET", false);
      }

   }

   @EventHandler
   public void onPlayerInteract(PlayerInteractEvent event) {
      Action action = event.getAction();
      Player player = event.getPlayer();
      ItemStack inHand = player.getInventory().getItemInMainHand();
      Block block = event.getClickedBlock();
      Material item = inHand.getType();
      if (DynamicEconomy.isWandOn && item.equals(Material.WOOD_SPADE) && DynamicEconomy.permission.has(player, "dynamiceconomy.selectregion") && block != null) {
         String playerName = player.getName();
         int x = block.getX();
         int y = block.getY();
         int z = block.getZ();
         String xyz = x + " " + y + " " + z;
         String[] coords = new String[2];
         Object coordinates;
         String[] stringCoords;
         if (action == Action.RIGHT_CLICK_BLOCK) {
            coords[1] = xyz;
            if (DynamicEconomy.selectedCorners.containsKey(playerName)) {
               coordinates = DynamicEconomy.selectedCorners.get(playerName);
               stringCoords = (String[])coordinates;
               stringCoords[1] = xyz;
               DynamicEconomy.selectedCorners.put(playerName, stringCoords);
            } else {
               DynamicEconomy.selectedCorners.put(playerName, coords);
            }

            this.color.sendColouredMessage(player, "&2Selected block #2: " + x + " , " + y + " , " + z);
            Utility.writeToLog(playerName + " Selected block #2: " + x + " , " + y + " , " + z);
         } else if (action == Action.LEFT_CLICK_BLOCK) {
            coords[0] = xyz;
            if (DynamicEconomy.selectedCorners.containsKey(playerName)) {
               coordinates = DynamicEconomy.selectedCorners.get(playerName);
               stringCoords = (String[])coordinates;
               stringCoords[0] = xyz;
               DynamicEconomy.selectedCorners.put(playerName, stringCoords);
            } else {
               DynamicEconomy.selectedCorners.put(playerName, coords);
            }

            this.color.sendColouredMessage(player, "&2Selected block #1: " + x + " , " + y + " , " + z);
            Utility.writeToLog(playerName + " Selected block #1: " + x + " , " + y + " , " + z);
         }
      }

      if (DynamicShop.isShop(block) && block != null) {
         String[] args;
         if (action == Action.LEFT_CLICK_BLOCK) {
            if (DynamicEconomy.permission.has(event.getPlayer(), "dynamiceconomy.buy")) {
               args = DynamicShop.getArgs(block);
               if (Item.isEnchantment(args[0])) {
                  args = DynamicShop.getEnchantArgs(block);
                  Transaction.buyEnchantment(event.getPlayer(), args);
               } else {
                  Transaction.buy(event.getPlayer(), args);
               }
            }
         } else if (action == Action.RIGHT_CLICK_BLOCK && DynamicEconomy.permission.has(event.getPlayer(), "dynamiceconomy.sell")) {
            args = DynamicShop.getArgs(block);
            if (Item.isEnchantment(args[0])) {
               args = DynamicShop.getEnchantArgs(block);
               String[] newArgs = new String[]{args[0]};
               Transaction.sellEnchantment(event.getPlayer(), newArgs);
            } else {
               Transaction.sell(event.getPlayer(), args);
            }
         }
      }

   }

   @EventHandler
   public void onSignChange(SignChangeEvent event) {
      String line = event.getLine(0);
      if (!line.equalsIgnoreCase("dynamicsign") && !line.equalsIgnoreCase("ds")) {
         if (line.equalsIgnoreCase("dynamicshop") || line.equalsIgnoreCase("dsh")) {
            if (DynamicEconomy.permission.has(event.getPlayer(), "dynamiceconomy.createshop")) {
               new DynamicShop(event);
            } else {
               this.color.sendColouredMessage(event.getPlayer(), DynamicEconomy.prefix + "&2You do not have permission to create a &fDynamicShop");
               Utility.writeToLog(event.getPlayer() + " tried to create a DynamicShop but didn't have permission.");
            }
         }
      } else if (DynamicEconomy.permission.has(event.getPlayer(), "dynamiceconomy.createsign")) {
         new dataSigns(event);
      } else {
         this.color.sendColouredMessage(event.getPlayer(), DynamicEconomy.prefix + "&2You do not have permission to create a &fDynamicSign");
         Utility.writeToLog(event.getPlayer() + " tried to create a DynamicSign but didn't have permission.");
      }

   }

   @EventHandler
   public void onBlockBreak(BlockBreakEvent event) {
      Block block = event.getBlock();
      int id = block.getTypeId();
      if (id == 68 || id == 63 || id == 323) {
         dataSigns.removeDataSign(block);
         DynamicShop.removeShopSign(block);
      }

   }

   public boolean setPermission(Permission perm) {
      permission = perm;
      return true;
   }

   public boolean setUpdater(AUCore up) {
      this.updater = up;
      return true;
   }
}
