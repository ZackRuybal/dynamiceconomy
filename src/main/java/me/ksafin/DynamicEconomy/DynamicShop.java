package me.ksafin.DynamicEconomy;

import couk.Adamki11s.Extras.Colour.ExtrasColour;
import java.io.IOException;
import java.util.Iterator;
import java.util.Set;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.event.block.SignChangeEvent;

public class DynamicShop {
   private static ExtrasColour color = new ExtrasColour();

   public DynamicShop(SignChangeEvent event) {
      String item = event.getLine(1);
      FileConfiguration conf = DynamicEconomy.shopsConfig;
      String[] itemInfo;
      int x;
      if (Item.isEnchantment(item)) {
         int level;
         try {
            level = Integer.parseInt(event.getLine(2));
         } catch (NumberFormatException var20) {
            this.setInvalid(event);
            return;
         }

         itemInfo = Item.getAllInfo(item);
         int id = Integer.parseInt(itemInfo[6]);
         id %= 2500;
         Enchantment enchant = Enchantment.getById(id);
         if (level <= 0 || level > enchant.getMaxLevel()) {
            this.setInvalidEnchantLevel(event);
            return;
         }

         if (itemInfo[0].equalsIgnoreCase("")) {
            color.sendColouredMessage(event.getPlayer(), DynamicEconomy.prefix + "&2The item &f" + item + "&2 doesn't exist.");
            Utility.writeToLog(event.getPlayer() + " tried to create a DynamicShop for the non-existant item " + item);
            this.setInvalid(event);
            return;
         }

         EnderEngine engine = new EnderEngine(itemInfo);
         double price = engine.getPrice();
         event.setLine(0, Utility.getColor(DynamicEconomy.signTaglineColor) + "[DynamicShop]");
         event.setLine(1, Utility.getColor(DynamicEconomy.signTaglineColor) + "Enchantment");
         event.setLine(2, Utility.getColor(DynamicEconomy.signInfoColor) + itemInfo[0] + " " + Utility.convertToRomanNumeral(level));
         event.setLine(3, Utility.getColor(DynamicEconomy.signTaglineColor) + DynamicEconomy.currencySymbol + Utility.getColor(DynamicEconomy.signInfoColor) + price * (double)level);
         Block signBlock = event.getBlock();
         x = signBlock.getX();
         int y = signBlock.getY();
         int z = signBlock.getZ();
         String coords = x + " " + y + " " + z;
         String node = coords + ".";
         conf.set(node + "item", itemInfo[0]);
         conf.set(node + "level", level);
         conf.set(node + "world", event.getBlock().getWorld().getName());
      } else {
         int amount;
         try {
            amount = Integer.parseInt(event.getLine(2));
         } catch (NumberFormatException var19) {
            this.setInvalid(event);
            return;
         }

         if (amount < 1) {
            this.setInvalid(event);
         }

         itemInfo = Item.getAllInfo(item);
         if (itemInfo[0].equalsIgnoreCase("")) {
            color.sendColouredMessage(event.getPlayer(), DynamicEconomy.prefix + "&2The item &f" + item + "&2 doesn't exist.");
            Utility.writeToLog(event.getPlayer() + " tried to create a DynamicShop for the non-existant item " + item);
            this.setInvalid(event);
            return;
         }

         EnderEngine engine = new EnderEngine(itemInfo);
         double price = engine.getPrice();
         event.setLine(0, Utility.getColor(DynamicEconomy.signTaglineColor) + "[DynamicShop]");
         event.setLine(1, Utility.getColor(DynamicEconomy.signTaglineColor) + itemInfo[0]);
         event.setLine(2, Utility.getColor(DynamicEconomy.signTaglineColor) + DynamicEconomy.currencySymbol + Utility.getColor(DynamicEconomy.signInfoColor) + price);
         event.setLine(3, Utility.getColor(DynamicEconomy.signTaglineColor) + "AMOUNT: " + Utility.getColor(DynamicEconomy.signInfoColor) + amount);
         Block signBlock = event.getBlock();
         int x = signBlock.getX();
         int y = signBlock.getY();
         x = signBlock.getZ();
         String coords = x + " " + y + " " + x;
         String node = coords + ".";
         conf.set(node + "item", itemInfo[0]);
         conf.set(node + "amount", amount);
         conf.set(node + "world", event.getBlock().getWorld().getName());
      }

      try {
         conf.save(DynamicEconomy.shopsFile);
      } catch (Exception var18) {
         var18.printStackTrace();
      }

   }

   public static void updateColors() {
      Set signs = DynamicEconomy.shopsConfig.getKeys(false);
      Iterator i = signs.iterator();

      while(i.hasNext()) {
         String coords = (String)i.next();
         Sign curSign = getSign(coords);
         if (curSign != null) {
            String item = DynamicEconomy.shopsConfig.getString(coords + ".item");
            String[] info = Item.getAllInfo(item);
            EnderEngine engine = new EnderEngine(info);
            int level;
            if (engine.isEnchantment()) {
               level = DynamicEconomy.shopsConfig.getInt(coords + ".level");
               curSign.setLine(0, Utility.getColor(DynamicEconomy.signTaglineColor) + "[DynamicShop]");
               curSign.setLine(1, Utility.getColor(DynamicEconomy.signTaglineColor) + "Enchantment");
               curSign.setLine(2, Utility.getColor(DynamicEconomy.signInfoColor) + info[0] + " " + Utility.convertToRomanNumeral(level));
               curSign.setLine(3, Utility.getColor(DynamicEconomy.signTaglineColor) + DynamicEconomy.currencySymbol + Utility.getColor(DynamicEconomy.signInfoColor) + engine.getPrice() * (double)level);
            } else {
               level = DynamicEconomy.shopsConfig.getInt(coords + ".amount");
               curSign.setLine(0, Utility.getColor(DynamicEconomy.signTaglineColor) + "[DynamicShop]");
               curSign.setLine(1, Utility.getColor(DynamicEconomy.signTaglineColor) + info[0]);
               curSign.setLine(2, Utility.getColor(DynamicEconomy.signTaglineColor) + DynamicEconomy.currencySymbol + Utility.getColor(DynamicEconomy.signInfoColor) + engine.getPrice());
               curSign.setLine(3, Utility.getColor(DynamicEconomy.signTaglineColor) + "AMOUNT: " + Utility.getColor(DynamicEconomy.signInfoColor) + level);
            }

            curSign.update();
         }
      }

   }

   public static void removeShopSign(Block block) {
      FileConfiguration conf = DynamicEconomy.shopsConfig;
      int x = block.getX();
      int y = block.getY();
      int z = block.getZ();
      String signID = x + " " + y + " " + z;
      if (conf.contains(signID)) {
         conf.set(signID, (Object)null);

         try {
            conf.save(DynamicEconomy.shopsFile);
         } catch (Exception var7) {
            var7.printStackTrace();
         }
      }

   }

   public static boolean isShop(Block block) {
      if (block == null) {
         return false;
      } else {
         FileConfiguration conf = DynamicEconomy.shopsConfig;
         int x = block.getX();
         int y = block.getY();
         int z = block.getZ();
         String signID = x + " " + y + " " + z;
         return conf.contains(signID);
      }
   }

   public static String[] getArgs(Block block) {
      FileConfiguration conf = DynamicEconomy.shopsConfig;
      int x = block.getX();
      int y = block.getY();
      int z = block.getZ();
      String signID = x + " " + y + " " + z;
      String item = conf.getString(signID + ".item");
      String amount = String.valueOf(conf.getInt(signID + ".amount"));
      String[] args = new String[]{item, amount};
      return args;
   }

   public static String[] getEnchantArgs(Block block) {
      FileConfiguration conf = DynamicEconomy.shopsConfig;
      int x = block.getX();
      int y = block.getY();
      int z = block.getZ();
      String signID = x + " " + y + " " + z;
      String item = conf.getString(signID + ".item");
      String level = String.valueOf(conf.getInt(signID + ".level"));
      String[] args = new String[]{item, level};
      return args;
   }

   public static void updateItem(String item) {
      FileConfiguration conf = DynamicEconomy.shopsConfig;
      Set shops = conf.getKeys(false);
      Iterator i = shops.iterator();
      int level = 1;

      while(i.hasNext()) {
         String coords = (String)i.next();
         ConfigurationSection curShop = conf.getConfigurationSection(coords);
         if (curShop.getString("item").equalsIgnoreCase(item)) {
            if (curShop.contains("level")) {
               level = curShop.getInt("level");
            }

            Sign sign = getSign(coords);
            double price = DynamicEconomy.itemConfig.getDouble(item + ".price") * (double)level;
            if (curShop.contains("level")) {
               sign.setLine(3, Utility.getColor(DynamicEconomy.signTaglineColor) + DynamicEconomy.currencySymbol + Utility.getColor(DynamicEconomy.signInfoColor) + price);
            } else {
               sign.setLine(2, Utility.getColor(DynamicEconomy.signTaglineColor) + DynamicEconomy.currencySymbol + Utility.getColor(DynamicEconomy.signInfoColor) + price);
            }

            sign.update();
         }
      }

   }

   public static Sign getSign(String coords) {
      FileConfiguration conf = DynamicEconomy.shopsConfig;
      String[] splitID = coords.split(" ");
      int x = Integer.parseInt(splitID[0]);
      int y = Integer.parseInt(splitID[1]);
      int z = Integer.parseInt(splitID[2]);
      String node = coords + ".world";
      String worldName = conf.getString(node, "world");
      Location loc = new Location(Bukkit.getServer().getWorld(worldName), (double)x, (double)y, (double)z);
      Block block = loc.getBlock();
      if (block.getState() instanceof Sign) {
         Sign sign = (Sign)block.getState();
         return sign;
      } else {
         conf.set(coords, (Object)null);

         try {
            conf.save(DynamicEconomy.shopsFile);
         } catch (IOException var12) {
            var12.printStackTrace();
         }

         Utility.writeToLog("DynamicShop no longer found at " + coords + ", entry removed from file");
         return null;
      }
   }

   public void setInvalid(SignChangeEvent event) {
      event.setLine(0, "");
      event.setLine(1, Utility.getColor(DynamicEconomy.signInvalidColor) + "INVALID");
      event.setLine(2, Utility.getColor(DynamicEconomy.signInvalidColor) + "ARGUMENTS");
      event.setLine(3, "");
   }

   public void setInvalidEnchantLevel(SignChangeEvent event) {
      event.setLine(0, Utility.getColor(DynamicEconomy.signInvalidColor) + "INVALID");
      event.setLine(1, Utility.getColor(DynamicEconomy.signInvalidColor) + "ENCHANTMENT");
      event.setLine(2, Utility.getColor(DynamicEconomy.signInvalidColor) + "LEVEL");
      event.setLine(3, "");
   }
}
