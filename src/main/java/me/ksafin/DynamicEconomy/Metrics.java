package me.ksafin.DynamicEconomy;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Level;
import org.bukkit.Bukkit;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginDescriptionFile;

public class Metrics {
   private static final int REVISION = 5;
   private static final String BASE_URL = "http://mcstats.org";
   private static final String REPORT_URL = "/report/%s";
   private static final String CONFIG_FILE = "plugins/PluginMetrics/config.yml";
   private static final String CUSTOM_DATA_SEPARATOR = "~~";
   private static final int PING_INTERVAL = 10;
   private final Plugin plugin;
   private final Set graphs = Collections.synchronizedSet(new HashSet());
   private final Metrics.Graph defaultGraph = new Metrics.Graph("Default", (Metrics.Graph)null);
   private final YamlConfiguration configuration;
   private final File configurationFile;
   private final String guid;
   private final Object optOutLock = new Object();
   private volatile int taskId = -1;

   public Metrics(Plugin plugin) throws IOException {
      if (plugin == null) {
         throw new IllegalArgumentException("Plugin cannot be null");
      } else {
         this.plugin = plugin;
         this.configurationFile = new File("plugins/PluginMetrics/config.yml");
         this.configuration = YamlConfiguration.loadConfiguration(this.configurationFile);
         this.configuration.addDefault("opt-out", false);
         this.configuration.addDefault("guid", UUID.randomUUID().toString());
         if (this.configuration.get("guid", (Object)null) == null) {
            this.configuration.options().header("http://mcstats.org").copyDefaults(true);
            this.configuration.save(this.configurationFile);
         }

         this.guid = this.configuration.getString("guid");
      }
   }

   public Metrics.Graph createGraph(String name) {
      if (name == null) {
         throw new IllegalArgumentException("Graph name cannot be null");
      } else {
         Metrics.Graph graph = new Metrics.Graph(name, (Metrics.Graph)null);
         this.graphs.add(graph);
         return graph;
      }
   }

   public void addGraph(Metrics.Graph graph) {
      if (graph == null) {
         throw new IllegalArgumentException("Graph cannot be null");
      } else {
         this.graphs.add(graph);
      }
   }

   public void addCustomData(Metrics.Plotter plotter) {
      if (plotter == null) {
         throw new IllegalArgumentException("Plotter cannot be null");
      } else {
         this.defaultGraph.addPlotter(plotter);
         this.graphs.add(this.defaultGraph);
      }
   }

   public boolean start() {
      Object var1 = this.optOutLock;
      synchronized(this.optOutLock) {
         if (this.isOptOut()) {
            return false;
         } else if (this.taskId >= 0) {
            return true;
         } else {
            this.taskId = this.plugin.getServer().getScheduler().scheduleAsyncRepeatingTask(this.plugin, new Runnable() {
               private boolean firstPost = true;

               public void run() {
                  try {
                     synchronized(Metrics.this.optOutLock) {
                        if (Metrics.this.isOptOut() && Metrics.this.taskId > 0) {
                           Metrics.this.plugin.getServer().getScheduler().cancelTask(Metrics.this.taskId);
                           Metrics.this.taskId = -1;
                           Iterator var3 = Metrics.this.graphs.iterator();

                           while(var3.hasNext()) {
                              Metrics.Graph graph = (Metrics.Graph)var3.next();
                              graph.onOptOut();
                           }
                        }
                     }

                     Metrics.this.postPlugin(!this.firstPost);
                     this.firstPost = false;
                  } catch (IOException var5) {
                     Bukkit.getLogger().log(Level.INFO, "[Metrics] " + var5.getMessage());
                  }

               }
            }, 0L, 12000L);
            return true;
         }
      }
   }

   public boolean isOptOut() {
      Object var1 = this.optOutLock;
      synchronized(this.optOutLock) {
         try {
            this.configuration.load("plugins/PluginMetrics/config.yml");
         } catch (IOException var3) {
            Bukkit.getLogger().log(Level.INFO, "[Metrics] " + var3.getMessage());
            return true;
         } catch (InvalidConfigurationException var4) {
            Bukkit.getLogger().log(Level.INFO, "[Metrics] " + var4.getMessage());
            return true;
         }

         return this.configuration.getBoolean("opt-out", false);
      }
   }

   public void enable() throws IOException {
      Object var1 = this.optOutLock;
      synchronized(this.optOutLock) {
         if (this.isOptOut()) {
            this.configuration.set("opt-out", false);
            this.configuration.save(this.configurationFile);
         }

         if (this.taskId < 0) {
            this.start();
         }

      }
   }

   public void disable() throws IOException {
      Object var1 = this.optOutLock;
      synchronized(this.optOutLock) {
         if (!this.isOptOut()) {
            this.configuration.set("opt-out", true);
            this.configuration.save(this.configurationFile);
         }

         if (this.taskId > 0) {
            this.plugin.getServer().getScheduler().cancelTask(this.taskId);
            this.taskId = -1;
         }

      }
   }

   private void postPlugin(boolean isPing) throws IOException {
      PluginDescriptionFile description = this.plugin.getDescription();
      StringBuilder data = new StringBuilder();
      data.append(encode("guid")).append('=').append(encode(this.guid));
      encodeDataPair(data, "version", description.getVersion());
      encodeDataPair(data, "server", Bukkit.getVersion());
      encodeDataPair(data, "players", Integer.toString(Bukkit.getServer().getOnlinePlayers().length));
      encodeDataPair(data, "revision", String.valueOf(5));
      if (isPing) {
         encodeDataPair(data, "ping", "true");
      }

      Set var4 = this.graphs;
      synchronized(this.graphs) {
         Iterator iter = this.graphs.iterator();

         while(true) {
            if (!iter.hasNext()) {
               break;
            }

            Metrics.Graph graph = (Metrics.Graph)iter.next();
            Iterator var8 = graph.getPlotters().iterator();

            while(var8.hasNext()) {
               Metrics.Plotter plotter = (Metrics.Plotter)var8.next();
               String key = String.format("C%s%s%s%s", "~~", graph.getName(), "~~", plotter.getColumnName());
               String value = Integer.toString(plotter.getValue());
               encodeDataPair(data, key, value);
            }
         }
      }

      URL url = new URL("http://mcstats.org" + String.format("/report/%s", encode(this.plugin.getDescription().getName())));
      URLConnection connection;
      if (this.isMineshafterPresent()) {
         connection = url.openConnection(Proxy.NO_PROXY);
      } else {
         connection = url.openConnection();
      }

      connection.setDoOutput(true);
      OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
      writer.write(data.toString());
      writer.flush();
      BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
      String response = reader.readLine();
      writer.close();
      reader.close();
      if (response != null && !response.startsWith("ERR")) {
         if (response.contains("OK This is your first update this hour")) {
            Set var21 = this.graphs;
            synchronized(this.graphs) {
               Iterator iter = this.graphs.iterator();

               while(iter.hasNext()) {
                  Metrics.Graph graph = (Metrics.Graph)iter.next();
                  Iterator var13 = graph.getPlotters().iterator();

                  while(var13.hasNext()) {
                     Metrics.Plotter plotter = (Metrics.Plotter)var13.next();
                     plotter.reset();
                  }
               }
            }
         }

      } else {
         throw new IOException(response);
      }
   }

   private boolean isMineshafterPresent() {
      try {
         Class.forName("mineshafter.MineServer");
         return true;
      } catch (Exception var2) {
         return false;
      }
   }

   private static void encodeDataPair(StringBuilder buffer, String key, String value) throws UnsupportedEncodingException {
      buffer.append('&').append(encode(key)).append('=').append(encode(value));
   }

   private static String encode(String text) throws UnsupportedEncodingException {
      return URLEncoder.encode(text, "UTF-8");
   }

   public static class Graph {
      private final String name;
      private final Set plotters;

      private Graph(String name) {
         this.plotters = new LinkedHashSet();
         this.name = name;
      }

      public String getName() {
         return this.name;
      }

      public void addPlotter(Metrics.Plotter plotter) {
         this.plotters.add(plotter);
      }

      public void removePlotter(Metrics.Plotter plotter) {
         this.plotters.remove(plotter);
      }

      public Set getPlotters() {
         return Collections.unmodifiableSet(this.plotters);
      }

      public int hashCode() {
         return this.name.hashCode();
      }

      public boolean equals(Object object) {
         if (!(object instanceof Metrics.Graph)) {
            return false;
         } else {
            Metrics.Graph graph = (Metrics.Graph)object;
            return graph.name.equals(this.name);
         }
      }

      protected void onOptOut() {
      }

      // $FF: synthetic method
      Graph(String var1, Metrics.Graph var2) {
         this(var1);
      }
   }

   public abstract static class Plotter {
      private final String name;

      public Plotter() {
         this("Default");
      }

      public Plotter(String name) {
         this.name = name;
      }

      public abstract int getValue();

      public String getColumnName() {
         return this.name;
      }

      public void reset() {
      }

      public int hashCode() {
         return this.getColumnName().hashCode();
      }

      public boolean equals(Object object) {
         if (!(object instanceof Metrics.Plotter)) {
            return false;
         } else {
            Metrics.Plotter plotter = (Metrics.Plotter)object;
            return plotter.name.equals(this.name) && plotter.getValue() == this.getValue();
         }
      }
   }
}
