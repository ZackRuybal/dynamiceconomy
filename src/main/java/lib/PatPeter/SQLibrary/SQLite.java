package lib.PatPeter.SQLibrary;

import java.io.File;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;

public class SQLite extends Database {
   public String location;
   public String name;
   private File sqlFile;
   // $FF: synthetic field
   private static int[] $SWITCH_TABLE$lib$PatPeter$SQLibrary$Database$Statements;

   public SQLite(Logger log, String prefix, String name, String location) {
      super(log, prefix, "[SQLite] ");
      this.name = name;
      this.location = location;
      File folder = new File(this.location);
      if (this.name.contains("/") || this.name.contains("\\") || this.name.endsWith(".db")) {
         this.writeError("The database name can not contain: /, \\, or .db", true);
      }

      if (!folder.exists()) {
         folder.mkdir();
      }

      this.sqlFile = new File(folder.getAbsolutePath() + File.separator + name + ".db");
   }

   protected boolean initialize() {
      try {
         Class.forName("org.sqlite.JDBC");
         return true;
      } catch (ClassNotFoundException var2) {
         this.writeError("You need the SQLite library " + var2, true);
         return false;
      }
   }

   public Connection open() {
      if (this.initialize()) {
         try {
            this.connection = DriverManager.getConnection("jdbc:sqlite:" + this.sqlFile.getAbsolutePath());
            return this.connection;
         } catch (SQLException var2) {
            this.writeError("SQLite exception on initialize " + var2, true);
         }
      }

      return null;
   }

   public void close() {
      if (this.connection != null) {
         try {
            this.connection.close();
         } catch (SQLException var2) {
            this.writeError("Error on Connection close: " + var2, true);
         }
      }

   }

   public Connection getConnection() {
      return this.connection == null ? this.open() : this.connection;
   }

   public boolean checkConnection() {
      return this.connection != null;
   }

   public ResultSet query(String query) {
      Statement statement = null;
      ResultSet result = null;

      try {
         this.connection = this.open();
         statement = this.connection.createStatement();
         switch($SWITCH_TABLE$lib$PatPeter$SQLibrary$Database$Statements()[this.getStatement(query).ordinal()]) {
         case 1:
            result = statement.executeQuery(query);
            return result;
         default:
            statement.executeQuery(query);
            return result;
         }
      } catch (SQLException var5) {
         if (!var5.getMessage().toLowerCase().contains("locking") && !var5.getMessage().toLowerCase().contains("locked")) {
            this.writeError("Error at SQL Query: " + var5.getMessage(), false);
            return null;
         } else {
            return this.retry(query);
         }
      }
   }

   PreparedStatement prepare(String query) {
      try {
         this.connection = this.open();
         PreparedStatement ps = this.connection.prepareStatement(query);
         return ps;
      } catch (SQLException var3) {
         if (!var3.toString().contains("not return ResultSet")) {
            this.writeError("Error in SQL prepare() query: " + var3.getMessage(), false);
         }

         return null;
      }
   }

   public boolean createTable(String query) {
      Statement statement = null;

      try {
         if (!query.equals("") && query != null) {
            statement = this.connection.createStatement();
            statement.execute(query);
            return true;
         } else {
            this.writeError("SQL Create Table query empty.", true);
            return false;
         }
      } catch (SQLException var4) {
         this.writeError(var4.getMessage(), true);
         return false;
      }
   }

   public boolean checkTable(String table) {
      DatabaseMetaData dbm = null;

      try {
         dbm = this.open().getMetaData();
         ResultSet tables = dbm.getTables((String)null, (String)null, table, (String[])null);
         return tables.next();
      } catch (SQLException var4) {
         this.writeError("Failed to check if table \"" + table + "\" exists: " + var4.getMessage(), true);
         return false;
      }
   }

   public boolean wipeTable(String table) {
      Statement statement = null;
      String query = null;

      try {
         if (!this.checkTable(table)) {
            this.writeError("Error at Wipe Table: table, " + table + ", does not exist", true);
            return false;
         } else {
            statement = this.connection.createStatement();
            query = "DELETE FROM " + table + ";";
            statement.executeQuery(query);
            return true;
         }
      } catch (SQLException var5) {
         if (!var5.getMessage().toLowerCase().contains("locking") && !var5.getMessage().toLowerCase().contains("locked") && !var5.toString().contains("not return ResultSet")) {
            this.writeError("Error at SQL Wipe Table Query: " + var5, false);
         }

         return false;
      }
   }

   public ResultSet retry(String query) {
      Statement statement = null;
      ResultSet result = null;

      try {
         statement = this.connection.createStatement();
         result = statement.executeQuery(query);
         return result;
      } catch (SQLException var5) {
         if (!var5.getMessage().toLowerCase().contains("locking") && !var5.getMessage().toLowerCase().contains("locked")) {
            this.writeError("Error in SQL query: " + var5.getMessage(), false);
         } else {
            this.writeError("Please close your previous ResultSet to run the query: \n" + query, false);
         }

         return null;
      }
   }

   // $FF: synthetic method
   static int[] $SWITCH_TABLE$lib$PatPeter$SQLibrary$Database$Statements() {
      int[] var10000 = $SWITCH_TABLE$lib$PatPeter$SQLibrary$Database$Statements;
      if ($SWITCH_TABLE$lib$PatPeter$SQLibrary$Database$Statements != null) {
         return var10000;
      } else {
         int[] var0 = new int[Database.Statements.values().length];

         try {
            var0[Database.Statements.ALTER.ordinal()] = 11;
         } catch (NoSuchFieldError var14) {
            ;
         }

         try {
            var0[Database.Statements.CALL.ordinal()] = 9;
         } catch (NoSuchFieldError var13) {
            ;
         }

         try {
            var0[Database.Statements.CREATE.ordinal()] = 10;
         } catch (NoSuchFieldError var12) {
            ;
         }

         try {
            var0[Database.Statements.DELETE.ordinal()] = 4;
         } catch (NoSuchFieldError var11) {
            ;
         }

         try {
            var0[Database.Statements.DO.ordinal()] = 5;
         } catch (NoSuchFieldError var10) {
            ;
         }

         try {
            var0[Database.Statements.DROP.ordinal()] = 12;
         } catch (NoSuchFieldError var9) {
            ;
         }

         try {
            var0[Database.Statements.HANDLER.ordinal()] = 8;
         } catch (NoSuchFieldError var8) {
            ;
         }

         try {
            var0[Database.Statements.INSERT.ordinal()] = 2;
         } catch (NoSuchFieldError var7) {
            ;
         }

         try {
            var0[Database.Statements.LOAD.ordinal()] = 7;
         } catch (NoSuchFieldError var6) {
            ;
         }

         try {
            var0[Database.Statements.RENAME.ordinal()] = 14;
         } catch (NoSuchFieldError var5) {
            ;
         }

         try {
            var0[Database.Statements.REPLACE.ordinal()] = 6;
         } catch (NoSuchFieldError var4) {
            ;
         }

         try {
            var0[Database.Statements.SELECT.ordinal()] = 1;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            var0[Database.Statements.TRUNCATE.ordinal()] = 13;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            var0[Database.Statements.UPDATE.ordinal()] = 3;
         } catch (NoSuchFieldError var1) {
            ;
         }

         $SWITCH_TABLE$lib$PatPeter$SQLibrary$Database$Statements = var0;
         return var0;
      }
   }
}
