package lib.PatPeter.SQLibrary;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.plugin.java.JavaPlugin;

public class SQLibrary extends JavaPlugin {
   public static final Logger logger = Logger.getLogger("Minecraft");

   public void onDisable() {
      logger.log(Level.INFO, "SQLibrary stopped.");
   }

   public void onEnable() {
      logger.log(Level.INFO, "SQLibrary loaded.");
   }
}
