package lib.PatPeter.SQLibrary;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;

public class MySQL extends Database {
   private String hostname = "localhost";
   private String portnmbr = "3306";
   private String username = "minecraft";
   private String password = "";
   private String database = "minecraft";
   // $FF: synthetic field
   private static int[] $SWITCH_TABLE$lib$PatPeter$SQLibrary$Database$Statements;

   public MySQL(Logger log, String prefix, String hostname, String portnmbr, String database, String username, String password) {
      super(log, prefix, "[MySQL] ");
      this.hostname = hostname;
      this.portnmbr = portnmbr;
      this.database = database;
      this.username = username;
      this.password = password;
   }

   protected boolean initialize() {
      try {
         Class.forName("com.mysql.jdbc.Driver");
         return true;
      } catch (ClassNotFoundException var2) {
         this.writeError("Class Not Found Exception: " + var2.getMessage() + ".", true);
         return false;
      }
   }

   public Connection open() {
      if (this.initialize()) {
         String url = "";

         try {
            url = "jdbc:mysql://" + this.hostname + ":" + this.portnmbr + "/" + this.database;
            this.connection = DriverManager.getConnection(url, this.username, this.password);
         } catch (SQLException var3) {
            this.writeError(url, true);
            this.writeError("Could not be resolved because of an SQL Exception: " + var3.getMessage() + ".", true);
         }
      }

      return null;
   }

   public void close() {
      try {
         if (this.connection != null) {
            this.connection.close();
         }
      } catch (Exception var2) {
         this.writeError("Failed to close database connection: " + var2.getMessage(), true);
      }

   }

   public Connection getConnection() {
      return this.connection;
   }

   public boolean checkConnection() {
      return this.connection != null;
   }

   public ResultSet query(String query) {
      Statement statement = null;
      ResultSet result = null;

      try {
         statement = this.connection.createStatement();
         result = statement.executeQuery("SELECT CURTIME()");
         switch($SWITCH_TABLE$lib$PatPeter$SQLibrary$Database$Statements()[this.getStatement(query).ordinal()]) {
         case 1:
            result = statement.executeQuery(query);
            break;
         default:
            statement.executeUpdate(query);
         }

         return result;
      } catch (SQLException var5) {
         this.writeError("Error in SQL query: " + var5.getMessage(), false);
         return result;
      }
   }

   public PreparedStatement prepare(String query) {
      PreparedStatement ps = null;

      try {
         ps = this.connection.prepareStatement(query);
         return ps;
      } catch (SQLException var4) {
         if (!var4.toString().contains("not return ResultSet")) {
            this.writeError("Error in SQL prepare() query: " + var4.getMessage(), false);
         }

         return ps;
      }
   }

   public boolean createTable(String query) {
      Statement statement = null;

      try {
         if (!query.equals("") && query != null) {
            statement = this.connection.createStatement();
            statement.execute(query);
            return true;
         } else {
            this.writeError("SQL query empty: createTable(" + query + ")", true);
            return false;
         }
      } catch (SQLException var4) {
         this.writeError(var4.getMessage(), true);
         return false;
      } catch (Exception var5) {
         this.writeError(var5.getMessage(), true);
         return false;
      }
   }

   public boolean checkTable(String table) {
      try {
         Statement statement = this.connection.createStatement();
         ResultSet result = statement.executeQuery("SELECT * FROM " + table);
         if (result == null) {
            return false;
         }

         if (result != null) {
            return true;
         }
      } catch (SQLException var4) {
         if (var4.getMessage().contains("exist")) {
            return false;
         }

         this.writeError("Error in SQL query: " + var4.getMessage(), false);
      }

      return this.query("SELECT * FROM " + table) == null;
   }

   public boolean wipeTable(String table) {
      Statement statement = null;
      String query = null;

      try {
         if (!this.checkTable(table)) {
            this.writeError("Error wiping table: \"" + table + "\" does not exist.", true);
            return false;
         } else {
            statement = this.connection.createStatement();
            query = "DELETE FROM " + table + ";";
            statement.executeUpdate(query);
            return true;
         }
      } catch (SQLException var5) {
         return !var5.toString().contains("not return ResultSet") ? false : false;
      }
   }

   // $FF: synthetic method
   static int[] $SWITCH_TABLE$lib$PatPeter$SQLibrary$Database$Statements() {
      int[] var10000 = $SWITCH_TABLE$lib$PatPeter$SQLibrary$Database$Statements;
      if ($SWITCH_TABLE$lib$PatPeter$SQLibrary$Database$Statements != null) {
         return var10000;
      } else {
         int[] var0 = new int[Database.Statements.values().length];

         try {
            var0[Database.Statements.ALTER.ordinal()] = 11;
         } catch (NoSuchFieldError var14) {
            ;
         }

         try {
            var0[Database.Statements.CALL.ordinal()] = 9;
         } catch (NoSuchFieldError var13) {
            ;
         }

         try {
            var0[Database.Statements.CREATE.ordinal()] = 10;
         } catch (NoSuchFieldError var12) {
            ;
         }

         try {
            var0[Database.Statements.DELETE.ordinal()] = 4;
         } catch (NoSuchFieldError var11) {
            ;
         }

         try {
            var0[Database.Statements.DO.ordinal()] = 5;
         } catch (NoSuchFieldError var10) {
            ;
         }

         try {
            var0[Database.Statements.DROP.ordinal()] = 12;
         } catch (NoSuchFieldError var9) {
            ;
         }

         try {
            var0[Database.Statements.HANDLER.ordinal()] = 8;
         } catch (NoSuchFieldError var8) {
            ;
         }

         try {
            var0[Database.Statements.INSERT.ordinal()] = 2;
         } catch (NoSuchFieldError var7) {
            ;
         }

         try {
            var0[Database.Statements.LOAD.ordinal()] = 7;
         } catch (NoSuchFieldError var6) {
            ;
         }

         try {
            var0[Database.Statements.RENAME.ordinal()] = 14;
         } catch (NoSuchFieldError var5) {
            ;
         }

         try {
            var0[Database.Statements.REPLACE.ordinal()] = 6;
         } catch (NoSuchFieldError var4) {
            ;
         }

         try {
            var0[Database.Statements.SELECT.ordinal()] = 1;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            var0[Database.Statements.TRUNCATE.ordinal()] = 13;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            var0[Database.Statements.UPDATE.ordinal()] = 3;
         } catch (NoSuchFieldError var1) {
            ;
         }

         $SWITCH_TABLE$lib$PatPeter$SQLibrary$Database$Statements = var0;
         return var0;
      }
   }
}
