package lib.PatPeter.SQLibrary;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.logging.Logger;

public abstract class Database {
   protected Logger log;
   protected final String PREFIX;
   protected final String DATABASE_PREFIX;
   protected boolean connected;
   protected Connection connection;

   public Database(Logger log, String prefix, String dp) {
      this.log = log;
      this.PREFIX = prefix;
      this.DATABASE_PREFIX = dp;
      this.connected = false;
      this.connection = null;
   }

   protected void writeInfo(String toWrite) {
      if (toWrite != null) {
         this.log.info(this.PREFIX + this.DATABASE_PREFIX + toWrite);
      }

   }

   protected void writeError(String toWrite, boolean severe) {
      if (toWrite != null) {
         if (severe) {
            this.log.severe(this.PREFIX + this.DATABASE_PREFIX + toWrite);
         } else {
            this.log.warning(this.PREFIX + this.DATABASE_PREFIX + toWrite);
         }
      }

   }

   abstract boolean initialize();

   abstract Connection open();

   abstract void close();

   abstract Connection getConnection();

   abstract boolean checkConnection();

   abstract ResultSet query(String var1);

   abstract PreparedStatement prepare(String var1);

   protected Database.Statements getStatement(String query) {
      String trimmedQuery = query.trim();
      if (trimmedQuery.substring(0, 6).equalsIgnoreCase("SELECT")) {
         return Database.Statements.SELECT;
      } else if (trimmedQuery.substring(0, 6).equalsIgnoreCase("INSERT")) {
         return Database.Statements.INSERT;
      } else if (trimmedQuery.substring(0, 6).equalsIgnoreCase("UPDATE")) {
         return Database.Statements.UPDATE;
      } else if (trimmedQuery.substring(0, 6).equalsIgnoreCase("DELETE")) {
         return Database.Statements.DELETE;
      } else if (trimmedQuery.substring(0, 6).equalsIgnoreCase("CREATE")) {
         return Database.Statements.CREATE;
      } else if (trimmedQuery.substring(0, 5).equalsIgnoreCase("ALTER")) {
         return Database.Statements.ALTER;
      } else if (trimmedQuery.substring(0, 4).equalsIgnoreCase("DROP")) {
         return Database.Statements.DROP;
      } else if (trimmedQuery.substring(0, 8).equalsIgnoreCase("TRUNCATE")) {
         return Database.Statements.TRUNCATE;
      } else if (trimmedQuery.substring(0, 6).equalsIgnoreCase("RENAME")) {
         return Database.Statements.RENAME;
      } else if (trimmedQuery.substring(0, 2).equalsIgnoreCase("DO")) {
         return Database.Statements.DO;
      } else if (trimmedQuery.substring(0, 7).equalsIgnoreCase("REPLACE")) {
         return Database.Statements.REPLACE;
      } else if (trimmedQuery.substring(0, 4).equalsIgnoreCase("LOAD")) {
         return Database.Statements.LOAD;
      } else if (trimmedQuery.substring(0, 7).equalsIgnoreCase("HANDLER")) {
         return Database.Statements.HANDLER;
      } else {
         return trimmedQuery.substring(0, 4).equalsIgnoreCase("CALL") ? Database.Statements.CALL : Database.Statements.SELECT;
      }
   }

   abstract boolean createTable(String var1);

   abstract boolean checkTable(String var1);

   abstract boolean wipeTable(String var1);

   protected static enum Statements {
      SELECT,
      INSERT,
      UPDATE,
      DELETE,
      DO,
      REPLACE,
      LOAD,
      HANDLER,
      CALL,
      CREATE,
      ALTER,
      DROP,
      TRUNCATE,
      RENAME;
   }
}
