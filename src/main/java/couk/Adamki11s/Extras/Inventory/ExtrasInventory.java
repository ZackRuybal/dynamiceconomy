package couk.Adamki11s.Extras.Inventory;

import java.util.HashMap;
import org.bukkit.Material;
import org.bukkit.craftbukkit.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class ExtrasInventory extends InventoryMethods {
   private HashMap playerInventory = new HashMap();

   public void addToInventory(Player p, Material m, int quantity) {
      p.getInventory().addItem(new ItemStack[]{new ItemStack(m.getId(), quantity)});
   }

   public void addToInventory(Player p, int id, int quantity) {
      p.getInventory().addItem(new ItemStack[]{new ItemStack(id, quantity)});
   }

   public void removeFromInventory(Player p, Material m, int quantity) {
      int amount = this.getAmountOf(p, m.getId());
      p.getInventory().remove(m.getId());
      this.addToInventory(p, m.getId(), amount - quantity);
   }

   public void removeFromInventory(Player p, int id, int quantity) {
      int amount = this.getAmountOf(p, id);
      p.getInventory().remove(id);
      this.addToInventory(p, id, amount - quantity);
   }

   public int getEmptySlots(Player p) {
      ItemStack[] invent = p.getInventory().getContents();
      int freeSlots = 0;
      ItemStack[] var7 = invent;
      int var6 = invent.length;

      for(int var5 = 0; var5 < var6; ++var5) {
         ItemStack i = var7[var5];
         if (i == null) {
            ++freeSlots;
         }
      }

      return freeSlots;
   }

   public boolean doesInventoryContain(Player p, Material m) {
      ItemStack[] invent = p.getInventory().getContents();
      ItemStack[] var7 = invent;
      int var6 = invent.length;

      for(int var5 = 0; var5 < var6; ++var5) {
         ItemStack i = var7[var5];
         if (i != null && i.getType() == m) {
            return true;
         }
      }

      return false;
   }

   public boolean doesInventoryContain(Player p, int id) {
      ItemStack[] invent = p.getInventory().getContents();
      ItemStack[] var7 = invent;
      int var6 = invent.length;

      for(int var5 = 0; var5 < var6; ++var5) {
         ItemStack i = var7[var5];
         if (i != null && i.getTypeId() == id) {
            return true;
         }
      }

      return false;
   }

   public int getStackCount(Player p, Material m) {
      ItemStack[] invent = p.getInventory().getContents();
      int stackCount = 0;
      ItemStack[] var8 = invent;
      int var7 = invent.length;

      for(int var6 = 0; var6 < var7; ++var6) {
         ItemStack i = var8[var6];
         if (i != null && i.getType() == m) {
            ++stackCount;
         }
      }

      return stackCount;
   }

   public int getStackCount(Player p, int id) {
      ItemStack[] invent = p.getInventory().getContents();
      int stackCount = 0;
      ItemStack[] var8 = invent;
      int var7 = invent.length;

      for(int var6 = 0; var6 < var7; ++var6) {
         ItemStack i = var8[var6];
         if (i != null && i.getTypeId() == id) {
            ++stackCount;
         }
      }

      return stackCount;
   }

   public int getAmountOf(Player p, Material m) {
      ItemStack[] invent = p.getInventory().getContents();
      int amount = 1;
      ItemStack[] var8 = invent;
      int var7 = invent.length;

      for(int var6 = 0; var6 < var7; ++var6) {
         ItemStack i = var8[var6];
         if (i != null && i.getType() == m) {
            amount += i.getAmount();
         }
      }

      return amount;
   }

   public int getAmountOfDataValue(Player p, ItemStack m) {
      ItemStack[] invent = p.getInventory().getContents();
      int amount = 0;
      ItemStack[] var8 = invent;
      int var7 = invent.length;

      for(int var6 = 0; var6 < var7; ++var6) {
         ItemStack i = var8[var6];
         if (i != null && i.getTypeId() == m.getTypeId() && i.getDurability() == m.getDurability()) {
            amount += i.getAmount();
         }
      }

      return amount;
   }

   public int getAmountOf(Player p, int id) {
      ItemStack[] invent = p.getInventory().getContents();
      int amount = 0;
      ItemStack[] var8 = invent;
      int var7 = invent.length;

      for(int var6 = 0; var6 < var7; ++var6) {
         ItemStack i = var8[var6];
         if (i != null && i.getTypeId() == id) {
            amount += i.getAmount();
            if (i.getDurability() >= 1) {
               amount -= i.getAmount();
            }
         }
      }

      return amount;
   }

   public int getAmountOf(Player p, ItemStack item) {
      ItemStack[] invent = p.getInventory().getContents();
      int amount = 0;
      ItemStack[] var8 = invent;
      int var7 = invent.length;

      for(int var6 = 0; var6 < var7; ++var6) {
         ItemStack i = var8[var6];
         if (i != null && i.getType().equals(item.getType()) && i.getEnchantments().equals(item.getEnchantments()) && i.getDurability() == item.getDurability()) {
            amount += i.getAmount();
         }
      }

      return amount;
   }

   public void sortInventory(Player p) {
      ItemStack[] invent = p.getInventory().getContents();
      ItemStack[] var6 = invent;
      int var5 = invent.length;

      for(int var4 = 0; var4 < var5; ++var4) {
         ItemStack i = var6[var4];
         if (i != null) {
            this.removeFromInventory(p, i.getType(), 0);
         }
      }

   }

   public void removeAllFromInventory(Player p, int id) {
      if (this.doesInventoryContain(p, id)) {
         p.getInventory().remove(id);
      }

   }

   public void removeAllFromInventory(Player p, Material m) {
      if (this.doesInventoryContain(p, m)) {
         p.getInventory().remove(m);
      }

   }

   public void wipeInventory(Player p) {
      ItemStack[] var5;
      int var4 = (var5 = p.getInventory().getContents()).length;

      for(int var3 = 0; var3 < var4; ++var3) {
         ItemStack i = var5[var3];
         if (i != null) {
            p.getInventory().remove(i.getTypeId());
         }
      }

   }

   public boolean storeInventory(Player p) {
      if (p != null) {
         this.playerInventory.put(p, p.getInventory().getContents());
         return true;
      } else {
         return false;
      }
   }

   public ItemStack[] retrieveInventory(Player p) {
      return this.playerInventory.containsKey(p) ? (ItemStack[])this.playerInventory.get(p) : null;
   }

   public void updateInventory(Player p) {
      ((CraftPlayer)p).getHandle().syncInventory();
   }
}
