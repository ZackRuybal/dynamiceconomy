package couk.Adamki11s.Extras.Inventory;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public abstract class InventoryMethods {
   public abstract void addToInventory(Player var1, Material var2, int var3);

   public abstract void addToInventory(Player var1, int var2, int var3);

   public abstract void removeFromInventory(Player var1, Material var2, int var3);

   public abstract void removeFromInventory(Player var1, int var2, int var3);

   public abstract int getEmptySlots(Player var1);

   public abstract boolean doesInventoryContain(Player var1, Material var2);

   public abstract boolean doesInventoryContain(Player var1, int var2);

   public abstract int getStackCount(Player var1, Material var2);

   public abstract int getStackCount(Player var1, int var2);

   public abstract int getAmountOf(Player var1, Material var2);

   public abstract int getAmountOf(Player var1, int var2);

   public abstract void sortInventory(Player var1);

   public abstract void removeAllFromInventory(Player var1, int var2);

   public abstract void removeAllFromInventory(Player var1, Material var2);

   public abstract void wipeInventory(Player var1);

   public abstract boolean storeInventory(Player var1);

   public abstract ItemStack[] retrieveInventory(Player var1);

   public abstract void updateInventory(Player var1);
}
