package couk.Adamki11s.Extras.Cryptography;

public abstract class CryptographyMethods {
   public abstract String computeMD5Hash(String var1);

   public abstract boolean compareHashes(String var1, String var2);

   public abstract String computeSHA1Hash(String var1);

   public abstract String computeSHA2_256BitHash(String var1);

   public abstract String computeSHA2_384BitHash(String var1);

   public abstract String computeSHA2_512BitHash(String var1);
}
