package couk.Adamki11s.Extras.Cryptography;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class ExtrasCryptography extends CryptographyMethods {
   public String computeMD5Hash(String s) {
      try {
         MessageDigest m = MessageDigest.getInstance("MD5");
         m.update(s.getBytes(), 0, s.length());
         return (new BigInteger(1, m.digest())).toString(16).toString();
      } catch (NoSuchAlgorithmException var3) {
         var3.printStackTrace();
         return null;
      }
   }

   public boolean compareHashes(String hash1, String hash2) {
      return hash1.equals(hash2);
   }

   public String computeSHA1Hash(String s) {
      try {
         MessageDigest md = MessageDigest.getInstance("SHA-1");
         byte[] sha1hash = new byte[40];
         md.update(s.getBytes("iso-8859-1"), 0, s.length());
         sha1hash = md.digest();
         return convertToHex(sha1hash);
      } catch (NoSuchAlgorithmException var4) {
         var4.printStackTrace();
      } catch (UnsupportedEncodingException var5) {
         var5.printStackTrace();
      }

      return null;
   }

   private static String convertToHex(byte[] data) {
      StringBuffer buf = new StringBuffer();

      for(int i = 0; i < data.length; ++i) {
         int halfbyte = data[i] >>> 4 & 15;
         int var4 = 0;

         do {
            if (halfbyte >= 0 && halfbyte <= 9) {
               buf.append((char)(48 + halfbyte));
            } else {
               buf.append((char)(97 + (halfbyte - 10)));
            }

            halfbyte = data[i] & 15;
         } while(var4++ < 1);
      }

      return buf.toString();
   }

   public String computeSHA2_256BitHash(String s) {
      try {
         MessageDigest md = MessageDigest.getInstance("SHA-256");
         md.update(s.getBytes());
         byte[] byteData = md.digest();
         StringBuffer sb = new StringBuffer();

         for(int i = 0; i < byteData.length; ++i) {
            sb.append(Integer.toString((byteData[i] & 255) + 256, 16).substring(1));
         }

         StringBuffer hexString = new StringBuffer();

         for(int i = 0; i < byteData.length; ++i) {
            String hex = Integer.toHexString(255 & byteData[i]);
            if (hex.length() == 1) {
               hexString.append('0');
            }

            hexString.append(hex);
         }

         return hexString.toString();
      } catch (NoSuchAlgorithmException var9) {
         var9.printStackTrace();
         return null;
      }
   }

   public String computeSHA2_384BitHash(String s) {
      try {
         MessageDigest md = MessageDigest.getInstance("SHA-384");
         md.update(s.getBytes());
         byte[] byteData = md.digest();
         StringBuffer sb = new StringBuffer();

         for(int i = 0; i < byteData.length; ++i) {
            sb.append(Integer.toString((byteData[i] & 255) + 256, 16).substring(1));
         }

         StringBuffer hexString = new StringBuffer();

         for(int i = 0; i < byteData.length; ++i) {
            String hex = Integer.toHexString(255 & byteData[i]);
            if (hex.length() == 1) {
               hexString.append('0');
            }

            hexString.append(hex);
         }

         return hexString.toString();
      } catch (NoSuchAlgorithmException var9) {
         var9.printStackTrace();
         return null;
      }
   }

   public String computeSHA2_512BitHash(String s) {
      try {
         MessageDigest md = MessageDigest.getInstance("SHA-512");
         md.update(s.getBytes());
         byte[] byteData = md.digest();
         StringBuffer sb = new StringBuffer();

         for(int i = 0; i < byteData.length; ++i) {
            sb.append(Integer.toString((byteData[i] & 255) + 256, 16).substring(1));
         }

         StringBuffer hexString = new StringBuffer();

         for(int i = 0; i < byteData.length; ++i) {
            String hex = Integer.toHexString(255 & byteData[i]);
            if (hex.length() == 1) {
               hexString.append('0');
            }

            hexString.append(hex);
         }

         return hexString.toString();
      } catch (NoSuchAlgorithmException var9) {
         var9.printStackTrace();
         return null;
      }
   }
}
