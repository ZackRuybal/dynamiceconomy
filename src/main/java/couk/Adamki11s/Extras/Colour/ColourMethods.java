package couk.Adamki11s.Extras.Colour;

import org.bukkit.Server;
import org.bukkit.entity.Player;

public abstract class ColourMethods {
   public abstract void sendColouredMessage(Player var1, String var2);

   public abstract void sendMultiColouredMessage(Player var1, String var2);

   public abstract void broadcastColouredMessage(Server var1, String var2);

   public abstract void broadcastMultiColouredMessage(Server var1, String var2);
}
