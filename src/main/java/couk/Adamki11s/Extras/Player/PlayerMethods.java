package couk.Adamki11s.Extras.Player;

import java.util.UUID;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

public abstract class PlayerMethods {
   public abstract boolean isOnGround(Player var1);

   public abstract boolean isUnderWater(Player var1);

   public abstract void mountEntity(Player var1, Entity var2);

   public abstract void mountPlayer(Player var1, Player var2);

   public abstract void setBlockOnPlayerHead(Player var1, Material var2);

   public abstract void setBlockOnPlayerHead(Player var1, int var2);

   public abstract int getSecondsLived(Player var1);

   public abstract int getMinutesLived(Player var1);

   public abstract int getHoursLived(Player var1);

   public abstract UUID getUniqueUUID(Player var1);

   public abstract void forceChat(Player var1, String var2);

   public abstract int getDimension(Player var1);

   public abstract void removeBlockOnPlayerHead(Player var1);

   public abstract Block getLookedAtBlock(Player var1);

   public abstract Location getLocationLooked(Player var1);
}
