package couk.Adamki11s.Extras.Player;

import java.util.HashSet;
import java.util.List;
import java.util.UUID;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.entity.CraftPlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class ExtrasPlayer extends PlayerMethods {
   public boolean isOnGround(Player p) {
      return ((CraftPlayer)p).getHandle().onGround;
   }

   public void mountEntity(Player p, Entity e) {
      e.setPassenger(p);
   }

   public boolean isUnderWater(Player p) {
      World w = p.getWorld();
      Location l = p.getLocation();
      l.setY(l.getY() + 1.0D);
      return w.getBlockAt(l).getType() == Material.WATER || w.getBlockAt(l).getType() == Material.STATIONARY_WATER;
   }

   public void setBlockOnPlayerHead(Player p, Material m) {
      p.getInventory().setHelmet(new ItemStack(m, 1));
   }

   public void mountPlayer(Player mounter, Player target) {
      target.setPassenger(mounter);
   }

   public void setBlockOnPlayerHead(Player p, int id) {
      p.getInventory().setHelmet(new ItemStack(id, 1));
   }

   public int getSecondsLived(Player p) {
      return Math.round((float)(((CraftPlayer)p).getHandle().ticksLived / 20));
   }

   public int getMinutesLived(Player p) {
      return Math.round((float)(((CraftPlayer)p).getHandle().ticksLived / 20 / 60));
   }

   public int getHoursLived(Player p) {
      return Math.round((float)(((CraftPlayer)p).getHandle().ticksLived / 20 / 60 / 60));
   }

   public UUID getUniqueUUID(Player p) {
      return p.getUniqueId();
   }

   public void forceChat(Player p, String message) {
      p.chat(message);
   }

   public int getDimension(Player p) {
      return ((CraftPlayer)p).getHandle().dimension;
   }

   public void removeBlockOnPlayerHead(Player p) {
      p.getInventory().setHelmet((ItemStack)null);
   }

   public Block getLookedAtBlock(Player p) {
      List blocks = p.getLineOfSight((HashSet)null, 500);
      Block lookedAt = (Block)blocks.get(blocks.size() - 1);
      return lookedAt;
   }

   public Location getLocationLooked(Player p) {
      List blocks = p.getLineOfSight((HashSet)null, 500);
      Block lookedAt = (Block)blocks.get(blocks.size() - 1);
      return lookedAt.getLocation();
   }
}
