package couk.Adamki11s.Extras.Regions;

import java.util.ArrayList;
import java.util.List;
import org.bukkit.Location;
import org.bukkit.Server;
import org.bukkit.entity.Player;

public abstract class RegionMethods {
   public abstract boolean isInsideCuboid(Player var1, Location var2, Location var3);

   public abstract boolean isInsideRadius(Player var1, Location var2, int var3);

   public abstract List getPlayersInCuboid(Server var1, Location var2, Location var3);

   public abstract List getPlayersInRadius(Server var1, Location var2, int var3);

   public abstract List getEntitiesInCuboid(Location var1, Location var2);

   public abstract List getEntitiesInRadius(Location var1, int var2);

   public abstract int getBlockCountInCuboid(Location var1, Location var2);

   public abstract int getBlockCountInRadius(Location var1, int var2);

   public abstract ArrayList getBlocksInRegion(Location var1, Location var2);
}
