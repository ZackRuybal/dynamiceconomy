package couk.Adamki11s.Extras.Regions;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.bukkit.Location;
import org.bukkit.Server;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

public class ExtrasRegions extends RegionMethods {
   public boolean isInsideCuboid(Player p, Location point1, Location point2) {
      double x1 = point1.getX();
      double x2 = point2.getX();
      double y1 = point1.getY();
      double y2 = point2.getY();
      double z1 = point1.getZ();
      double z2 = point2.getZ();
      double px = p.getLocation().getX();
      double py = p.getLocation().getY();
      double pz = p.getLocation().getZ();
      return (py <= y1 && py >= y2 || py >= y1 && py <= y2) && (pz <= z1 && pz >= z2 || pz >= z1 && pz <= z2) && (px <= x1 && px >= x2 || px >= x1 && px <= x2) && (px <= x1 && px >= x2 || px >= x1 && px <= x2);
   }

   public boolean isInsideRadius(Player p, Location centre, int radius) {
      Location point1 = new Location(centre.getWorld(), centre.getX() - (double)radius, centre.getY() - (double)radius, centre.getZ() - (double)radius);
      Location point2 = new Location(centre.getWorld(), centre.getX() + (double)radius, centre.getY() + (double)radius, centre.getZ() + (double)radius);
      double x1 = point1.getX();
      double x2 = point2.getX();
      double y1 = point1.getY();
      double y2 = point2.getY();
      double z1 = point1.getZ();
      double z2 = point2.getZ();
      double px = p.getLocation().getX();
      double py = p.getLocation().getY();
      double pz = p.getLocation().getZ();
      return (py <= y1 && py >= y2 || py >= y1 && py <= y2) && (pz <= z1 && pz >= z2 || pz >= z1 && pz <= z2) && (px <= x1 && px >= x2 || px >= x1 && px <= x2) && (px <= x1 && px >= x2 || px >= x1 && px <= x2);
   }

   public List getPlayersInCuboid(Server s, Location point1, Location point2) {
      List playerSet = new ArrayList();
      Player[] var8;
      int var7 = (var8 = s.getOnlinePlayers()).length;

      for(int var6 = 0; var6 < var7; ++var6) {
         Player p = var8[var6];
         double x1 = point1.getX();
         double x2 = point2.getX();
         double y1 = point1.getY();
         double y2 = point2.getY();
         double z1 = point1.getZ();
         double z2 = point2.getZ();
         double px = p.getLocation().getX();
         double py = p.getLocation().getY();
         double pz = p.getLocation().getZ();
         if ((py <= y1 && py >= y2 || py >= y1 && py <= y2) && (pz <= z1 && pz >= z2 || pz >= z1 && pz <= z2) && (px <= x1 && px >= x2 || px >= x1 && px <= x2) && (px <= x1 && px >= x2 || px >= x1 && px <= x2)) {
            playerSet.add(p);
         }
      }

      return playerSet;
   }

   public List getPlayersInRadius(Server s, Location centre, int radius) {
      Location point1 = new Location(centre.getWorld(), centre.getX() - (double)radius, centre.getY() - (double)radius, centre.getZ() - (double)radius);
      Location point2 = new Location(centre.getWorld(), centre.getX() + (double)radius, centre.getY() + (double)radius, centre.getZ() + (double)radius);
      List playerSet = new ArrayList();
      Player[] var10;
      int var9 = (var10 = s.getOnlinePlayers()).length;

      for(int var8 = 0; var8 < var9; ++var8) {
         Player p = var10[var8];
         double x1 = point1.getX();
         double x2 = point2.getX();
         double y1 = point1.getY();
         double y2 = point2.getY();
         double z1 = point1.getZ();
         double z2 = point2.getZ();
         double px = p.getLocation().getX();
         double py = p.getLocation().getY();
         double pz = p.getLocation().getZ();
         if ((py <= y1 && py >= y2 || py >= y1 && py <= y2) && (pz <= z1 && pz >= z2 || pz >= z1 && pz <= z2) && (px <= x1 && px >= x2 || px >= x1 && px <= x2) && (px <= x1 && px >= x2 || px >= x1 && px <= x2)) {
            playerSet.add(p);
         }
      }

      return playerSet;
   }

   public List getEntitiesInCuboid(Location point1, Location point2) {
      List entitySet = new ArrayList();
      World w = point1.getWorld();
      Iterator var6 = w.getEntities().iterator();

      while(true) {
         Entity e;
         double x1;
         double x2;
         double ex;
         do {
            double z1;
            double z2;
            double ez;
            do {
               double y1;
               double y2;
               double ey;
               do {
                  do {
                     if (!var6.hasNext()) {
                        return entitySet;
                     }

                     e = (Entity)var6.next();
                     x1 = point1.getX();
                     x2 = point2.getX();
                     y1 = point1.getY();
                     y2 = point2.getY();
                     z1 = point1.getZ();
                     z2 = point2.getZ();
                     ex = e.getLocation().getX();
                     ey = e.getLocation().getY();
                     ez = e.getLocation().getZ();
                  } while((ey > y1 || ey < y2) && (ey < y1 || ey > y2));
               } while((ez > z1 || ez < z2) && (ez < z1 || ez > z2));
            } while((ex > x1 || ex < x2) && (ex < x1 || ex > x2));
         } while((ex > x1 || ex < x2) && (ex < x1 || ex > x2));

         entitySet.add(e);
      }
   }

   public List getEntitiesInRadius(Location centre, int radius) {
      List entitySet = new ArrayList();
      Location point1 = new Location(centre.getWorld(), centre.getX() - (double)radius, centre.getY() - (double)radius, centre.getZ() - (double)radius);
      Location point2 = new Location(centre.getWorld(), centre.getX() + (double)radius, centre.getY() + (double)radius, centre.getZ() + (double)radius);
      World w = point1.getWorld();
      Iterator var8 = w.getEntities().iterator();

      while(true) {
         Entity e;
         double x1;
         double x2;
         double ex;
         do {
            double z1;
            double z2;
            double ez;
            do {
               double y1;
               double y2;
               double ey;
               do {
                  do {
                     if (!var8.hasNext()) {
                        return entitySet;
                     }

                     e = (Entity)var8.next();
                     x1 = point1.getX();
                     x2 = point2.getX();
                     y1 = point1.getY();
                     y2 = point2.getY();
                     z1 = point1.getZ();
                     z2 = point2.getZ();
                     ex = e.getLocation().getX();
                     ey = e.getLocation().getY();
                     ez = e.getLocation().getZ();
                  } while((ey > y1 || ey < y2) && (ey < y1 || ey > y2));
               } while((ez > z1 || ez < z2) && (ez < z1 || ez > z2));
            } while((ex > x1 || ex < x2) && (ex < x1 || ex > x2));
         } while((ex > x1 || ex < x2) && (ex < x1 || ex > x2));

         entitySet.add(e);
      }
   }

   public int getBlockCountInCuboid(Location point1, Location point2) {
      int x1 = point1.getBlockX();
      int x2 = point2.getBlockX();
      int y1 = point1.getBlockY();
      int y2 = point2.getBlockY();
      int z1 = point1.getBlockZ();
      int z2 = point2.getBlockZ();
      int xdist;
      if (x1 > x2) {
         xdist = Math.round((float)(x1 - x2));
      } else {
         xdist = Math.round((float)(x2 - x1));
      }

      int ydist;
      if (y1 > y2) {
         ydist = Math.round((float)(y1 - y2));
      } else {
         ydist = Math.round((float)(y2 - y1));
      }

      int zdist;
      if (z1 > z2) {
         zdist = Math.round((float)(z1 - z2));
      } else {
         zdist = Math.round((float)(z2 - z1));
      }

      int blockCount = xdist * ydist * zdist;
      return blockCount;
   }

   public int getBlockCountInRadius(Location centre, int radius) {
      Location point1 = new Location(centre.getWorld(), centre.getX() - (double)radius, centre.getY() - (double)radius, centre.getZ() - (double)radius);
      Location point2 = new Location(centre.getWorld(), centre.getX() + (double)radius, centre.getY() + (double)radius, centre.getZ() + (double)radius);
      int x1 = point1.getBlockX();
      int x2 = point2.getBlockX();
      int y1 = point1.getBlockY();
      int y2 = point2.getBlockY();
      int z1 = point1.getBlockZ();
      int z2 = point2.getBlockZ();
      int xdist;
      if (x1 > x2) {
         xdist = Math.round((float)(x1 - x2));
      } else {
         xdist = Math.round((float)(x2 - x1));
      }

      int ydist;
      if (y1 > y2) {
         ydist = Math.round((float)(y1 - y2));
      } else {
         ydist = Math.round((float)(y2 - y1));
      }

      int zdist;
      if (z1 > z2) {
         zdist = Math.round((float)(z1 - z2));
      } else {
         zdist = Math.round((float)(z2 - z1));
      }

      int blockCount = xdist * ydist * zdist;
      return blockCount;
   }

   public ArrayList getBlocksInRegion(Location point1, Location point2) {
      ArrayList al = new ArrayList();
      double x1 = point1.getX();
      double x2 = point2.getX();
      double y1 = point1.getY();
      double y2 = point2.getY();
      double z1 = point1.getZ();
      double z2 = point2.getZ();
      double tmp = 0.0D;
      World w = point1.getWorld();
      if (x2 > x1) {
         tmp = x2;
         x2 = x1;
         x1 = tmp;
      }

      if (y2 > y1) {
         tmp = y2;
         y2 = y1;
         y1 = tmp;
      }

      if (z2 > z1) {
         tmp = z2;
         z2 = z1;
         z1 = tmp;
      }

      for(double x = x1 - x2; x <= x2; ++x) {
         for(double y = y1 - y2; y <= y2; ++y) {
            for(double z = z1 - z2; z <= z2; ++z) {
               Location construct = new Location(w, x2 + x, y2 + y, z2 + z);
               al.add(w.getBlockAt(construct));
            }
         }
      }

      return al;
   }
}
