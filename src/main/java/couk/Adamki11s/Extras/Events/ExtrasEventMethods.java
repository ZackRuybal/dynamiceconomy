package couk.Adamki11s.Extras.Events;

import org.bukkit.event.player.PlayerMoveEvent;

public abstract class ExtrasEventMethods {
   public abstract boolean didRotate(PlayerMoveEvent var1);

   public abstract boolean didMove(PlayerMoveEvent var1);

   public abstract boolean didChangeBlock(PlayerMoveEvent var1);
}
