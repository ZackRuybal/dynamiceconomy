package couk.Adamki11s.Extras.Events;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerMoveEvent;

public class ExtrasEvents extends ExtrasEventMethods {
   private double x = 0.0D;
   private double z = 0.0D;
   private double offsetx = 0.0D;
   private double offsetz = 0.0D;
   private double oldx = 0.0D;
   private double oldz = 0.0D;

   public boolean didRotate(PlayerMoveEvent evt) {
      Location to = evt.getTo();
      Location from = evt.getFrom();
      float toYaw = to.getYaw();
      float fromYaw = from.getYaw();
      float toPitch = to.getPitch();
      float fromPitch = from.getPitch();
      return toYaw != fromYaw || toPitch != fromPitch;
   }

   public boolean didMove(PlayerMoveEvent evt) {
      return evt.getTo().getX() != evt.getFrom().getX() || evt.getTo().getY() != evt.getFrom().getY() || evt.getTo().getZ() != evt.getFrom().getZ();
   }

   public boolean didChangeBlock(PlayerMoveEvent evt) {
      if (this.didMove(evt)) {
         Player player = evt.getPlayer();
         Location location = player.getLocation();
         if (this.x != this.oldx) {
            this.oldx = this.x;
         }

         if (this.z != this.oldz) {
            this.oldz = this.z;
         }

         this.x = location.getX();
         this.z = location.getZ();
         if (this.x - this.oldx > 0.0D) {
            this.offsetx += this.x - this.oldx;
            if (this.offsetx >= 1.0D) {
               this.offsetx = 0.0D;
               return true;
            }
         }

         if (this.x - this.oldx < 0.0D) {
            this.offsetx += this.oldx - this.x;
            if (this.offsetx >= 1.0D) {
               this.offsetx = 0.0D;
               return true;
            }
         }

         if (this.z - this.oldz > 0.0D) {
            this.offsetz += this.z - this.oldz;
            if (this.offsetz >= 1.0D) {
               this.offsetz = 0.0D;
               return true;
            }
         }

         if (this.z - this.oldz < 0.0D) {
            this.offsetz += this.oldz - this.z;
            if (this.offsetz >= 1.0D) {
               this.offsetz = 0.0D;
               return true;
            }
         }

         return false;
      } else {
         return false;
      }
   }
}
