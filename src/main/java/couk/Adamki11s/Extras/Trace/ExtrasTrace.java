package couk.Adamki11s.Extras.Trace;

import couk.Adamki11s.Extras.Extras.Extras;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import org.bukkit.Server;
import org.bukkit.entity.Player;

public class ExtrasTrace extends TraceMethods {
   private long startTrace = 0L;
   private long endTrace = 0L;

   public void startTrace() {
      this.startTrace = System.nanoTime();
   }

   public void stopTrace() {
      this.endTrace = System.nanoTime() - this.startTrace;
   }

   public long getTrace() {
      return this.endTrace / 1000L;
   }

   public void logTraceTime() {
      System.out.println("[Extras] Trace time : " + this.getTrace() + "ms");
   }

   public void logTraceTime(File file, String description) {
      try {
         FileWriter fstream = new FileWriter(file, true);
         BufferedWriter bw = new BufferedWriter(fstream);
         bw.write("[Extras] Trace : " + description + " : " + this.getTrace() + "ms.");
         bw.newLine();
         bw.close();
      } catch (IOException var5) {
         System.out.println("[Extras] Could not print trace to file! Caused by plugin : " + Extras.pluginName);
         var5.printStackTrace();
      }

   }

   public void broadcastTraceTime(Server s) {
      s.broadcastMessage("Trace time : " + this.getTrace());
   }

   public void sendTraceTime(Player p) {
      p.sendMessage("Trace time : " + this.getTrace());
   }

   public void broadcastTraceTimeCustom(Server s, String prefix) {
      s.broadcastMessage(prefix + this.getTrace());
   }

   public void sendTraceTimeCustom(Player p, String prefix) {
      p.sendMessage(prefix + this.getTrace());
   }
}
