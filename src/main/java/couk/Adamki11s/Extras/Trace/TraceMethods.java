package couk.Adamki11s.Extras.Trace;

import java.io.File;
import org.bukkit.Server;
import org.bukkit.entity.Player;

public abstract class TraceMethods {
   public abstract void startTrace();

   public abstract void stopTrace();

   public abstract long getTrace();

   public abstract void logTraceTime();

   public abstract void logTraceTime(File var1, String var2);

   public abstract void broadcastTraceTime(Server var1);

   public abstract void broadcastTraceTimeCustom(Server var1, String var2);

   public abstract void sendTraceTime(Player var1);

   public abstract void sendTraceTimeCustom(Player var1, String var2);
}
