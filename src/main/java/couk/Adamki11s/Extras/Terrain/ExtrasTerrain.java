package couk.Adamki11s.Extras.Terrain;

import java.util.ArrayList;
import java.util.Iterator;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;

public class ExtrasTerrain extends TerrainMethods {
   public void generateCube(int length, Location startPoint, Material m, Byte data) {
      double xx = startPoint.getX();
      double yy = startPoint.getY();
      double zz = startPoint.getZ();
      World w = startPoint.getWorld();

      for(double x = xx - (double)length; x <= xx; ++x) {
         for(double y = yy - (double)length; y <= yy; ++y) {
            for(double z = zz - (double)length; z <= zz; ++z) {
               Location construct = new Location(w, x, y, z);
               w.getBlockAt(construct).setType(m);
               if (data != null) {
                  w.getBlockAt(construct).setData(data);
               }
            }
         }
      }

   }

   public void generateCube(int length, Location startPoint, int id, Byte data) {
      double xx = startPoint.getX();
      double yy = startPoint.getY();
      double zz = startPoint.getZ();
      World w = startPoint.getWorld();

      for(double x = xx - (double)length; x <= xx; ++x) {
         for(double y = yy - (double)length; y <= yy; ++y) {
            for(double z = zz - (double)length; z <= zz; ++z) {
               Location construct = new Location(w, x, y, z);
               w.getBlockAt(construct).setTypeId(id);
               if (data != null) {
                  w.getBlockAt(construct).setData(data);
               }
            }
         }
      }

   }

   public void deleteSpecificCube(int length, Location startPoint, Material m, Byte data) {
      double xx = startPoint.getX();
      double yy = startPoint.getY();
      double zz = startPoint.getZ();
      World w = startPoint.getWorld();

      for(double x = xx - (double)length; x <= xx; ++x) {
         for(double y = yy - (double)length; y <= yy; ++y) {
            for(double z = zz - (double)length; z <= zz; ++z) {
               Location construct = new Location(w, x, y, z);
               if (w.getBlockAt(construct).getType() == m) {
                  if (data != null) {
                     if (w.getBlockAt(construct).getData() == data) {
                        w.getBlockAt(construct).setType(Material.AIR);
                     }
                  } else {
                     w.getBlockAt(construct).setType(Material.AIR);
                  }
               }
            }
         }
      }

   }

   public void deleteSpecificCube(int length, Location startPoint, int id, Byte data) {
      double xx = startPoint.getX();
      double yy = startPoint.getY();
      double zz = startPoint.getZ();
      World w = startPoint.getWorld();

      for(double x = xx - (double)length; x <= xx; ++x) {
         for(double y = yy - (double)length; y <= yy; ++y) {
            for(double z = zz - (double)length; z <= zz; ++z) {
               Location construct = new Location(w, x, y, z);
               if (w.getBlockAt(construct).getTypeId() == id) {
                  if (data != null) {
                     if (w.getBlockAt(construct).getData() == data) {
                        w.getBlockAt(construct).setType(Material.AIR);
                     }
                  } else {
                     w.getBlockAt(construct).setType(Material.AIR);
                  }
               }
            }
         }
      }

   }

   public void deleteAllCube(int length, Location startPoint) {
      double xx = startPoint.getX();
      double yy = startPoint.getY();
      double zz = startPoint.getZ();
      World w = startPoint.getWorld();

      for(double x = xx - (double)length; x <= xx; ++x) {
         for(double y = yy - (double)length; y <= yy; ++y) {
            for(double z = zz - (double)length; z <= zz; ++z) {
               Location construct = new Location(w, x, y, z);
               w.getBlockAt(construct).setType(Material.AIR);
            }
         }
      }

   }

   public void generateFromPoints(Location point1, Location point2, Material m, Byte data) {
      double x1 = point1.getX();
      double x2 = point2.getX();
      double y1 = point1.getY();
      double y2 = point2.getY();
      double z1 = point1.getZ();
      double z2 = point2.getZ();
      double tmp = 0.0D;
      World w = point1.getWorld();
      if (x2 > x1) {
         tmp = x2;
         x2 = x1;
         x1 = tmp;
      }

      if (y2 > y1) {
         tmp = y2;
         y2 = y1;
         y1 = tmp;
      }

      if (z2 > z1) {
         tmp = z2;
         z2 = z1;
         z1 = tmp;
      }

      for(double xx = x1 - (x1 - x2); xx <= x1; ++xx) {
         for(double yy = y1 - (y1 - y2); yy <= y1; ++yy) {
            for(double zz = z1 - (z1 - z2); zz <= z1; ++zz) {
               Location construct = new Location(w, xx, yy, zz);
               if (data != null) {
                  w.getBlockAt(construct).setType(m);
                  w.getBlockAt(construct).setData(data);
               } else {
                  w.getBlockAt(construct).setType(m);
               }
            }
         }
      }

   }

   public void generateFromPoints(Location point1, Location point2, int id, Byte data) {
      double x1 = point1.getX();
      double x2 = point2.getX();
      double y1 = point1.getY();
      double y2 = point2.getY();
      double z1 = point1.getZ();
      double z2 = point2.getZ();
      double tmp = 0.0D;
      World w = point1.getWorld();
      if (x2 > x1) {
         tmp = x2;
         x2 = x1;
         x1 = tmp;
      }

      if (y2 > y1) {
         tmp = y2;
         y2 = y1;
         y1 = tmp;
      }

      if (z2 > z1) {
         tmp = z2;
         z2 = z1;
         z1 = tmp;
      }

      for(double xx = x1 - (x1 - x2); xx <= x1; ++xx) {
         for(double yy = y1 - (y1 - y2); yy <= y1; ++yy) {
            for(double zz = z1 - (z1 - z2); zz <= z1; ++zz) {
               Location construct = new Location(w, xx, yy, zz);
               if (data != null) {
                  w.getBlockAt(construct).setTypeId(id);
                  w.getBlockAt(construct).setData(data);
               } else {
                  w.getBlockAt(construct).setTypeId(id);
               }
            }
         }
      }

   }

   public void generateFromArrayList(ArrayList ArrayList, Material m, Byte data) {
      Iterator var5 = ArrayList.iterator();

      while(var5.hasNext()) {
         Location l = (Location)var5.next();
         if (data != null) {
            l.getWorld().getBlockAt(l).setType(m);
            l.getWorld().getBlockAt(l).setData(data);
         } else {
            l.getWorld().getBlockAt(l).setType(m);
         }
      }

   }

   public void generateFromArrayList(ArrayList ArrayList, int id, Byte data) {
      Iterator var5 = ArrayList.iterator();

      while(var5.hasNext()) {
         Location l = (Location)var5.next();
         if (data != null) {
            l.getWorld().getBlockAt(l).setTypeId(id);
            l.getWorld().getBlockAt(l).setData(data);
         } else {
            l.getWorld().getBlockAt(l).setTypeId(id);
         }
      }

   }

   public void deleteSpecificFromPoints(Location point1, Location point2, Material m, Byte data) {
      double x1 = point1.getX();
      double x2 = point2.getX();
      double y1 = point1.getY();
      double y2 = point2.getY();
      double z1 = point1.getZ();
      double z2 = point2.getZ();
      double tmp = 0.0D;
      World w = point1.getWorld();
      if (x2 > x1) {
         tmp = x2;
         x2 = x1;
         x1 = tmp;
      }

      if (y2 > y1) {
         tmp = y2;
         y2 = y1;
         y1 = tmp;
      }

      if (z2 > z1) {
         tmp = z2;
         z2 = z1;
         z1 = tmp;
      }

      for(double xx = x1 - (x1 - x2); xx <= x1; ++xx) {
         for(double yy = y1 - (y1 - y2); yy <= y1; ++yy) {
            for(double zz = z1 - (z1 - z2); zz <= z1; ++zz) {
               Location construct = new Location(w, xx, yy, zz);
               if (data != null) {
                  if (w.getBlockAt(construct).getType() == m && w.getBlockAt(construct).getData() == data) {
                     w.getBlockAt(construct).setTypeId(0);
                  }
               } else if (w.getBlockAt(construct).getType() == m) {
                  w.getBlockAt(construct).setTypeId(0);
               }
            }
         }
      }

   }

   public void deleteSpecificFromPoints(Location point1, Location point2, int id, Byte data) {
      double x1 = point1.getX();
      double x2 = point2.getX();
      double y1 = point1.getY();
      double y2 = point2.getY();
      double z1 = point1.getZ();
      double z2 = point2.getZ();
      double tmp = 0.0D;
      World w = point1.getWorld();
      if (x2 > x1) {
         tmp = x2;
         x2 = x1;
         x1 = tmp;
      }

      if (y2 > y1) {
         tmp = y2;
         y2 = y1;
         y1 = tmp;
      }

      if (z2 > z1) {
         tmp = z2;
         z2 = z1;
         z1 = tmp;
      }

      for(double xx = x1 - (x1 - x2); xx <= x1; ++xx) {
         for(double yy = y1 - (y1 - y2); yy <= y1; ++yy) {
            for(double zz = z1 - (z1 - z2); zz <= z1; ++zz) {
               Location construct = new Location(w, xx, yy, zz);
               if (data != null) {
                  if (w.getBlockAt(construct).getTypeId() == id && w.getBlockAt(construct).getData() == data) {
                     w.getBlockAt(construct).setTypeId(0);
                  }
               } else if (w.getBlockAt(construct).getTypeId() == id) {
                  w.getBlockAt(construct).setTypeId(0);
               }
            }
         }
      }

   }

   public void deleteAllFromPoints(Location point1, Location point2) {
      double x1 = point1.getX();
      double x2 = point2.getX();
      double y1 = point1.getY();
      double y2 = point2.getY();
      double z1 = point1.getZ();
      double z2 = point2.getZ();
      double tmp = 0.0D;
      World w = point1.getWorld();
      if (x2 > x1) {
         tmp = x2;
         x2 = x1;
         x1 = tmp;
      }

      if (y2 > y1) {
         tmp = y2;
         y2 = y1;
         y1 = tmp;
      }

      if (z2 > z1) {
         tmp = z2;
         z2 = z1;
         z1 = tmp;
      }

      for(double xx = x1 - (x1 - x2); xx <= x1; ++xx) {
         for(double yy = y1 - (y1 - y2); yy <= y1; ++yy) {
            for(double zz = z1 - (z1 - z2); zz <= z1; ++zz) {
               Location construct = new Location(w, xx, yy, zz);
               w.getBlockAt(construct).setTypeId(0);
            }
         }
      }

   }

   public void replaceBlocks(Material toReplace, Byte toReplaceData, Material replaceWith, Byte replaceWithData, Location point1, Location point2) {
      double x1 = point1.getX();
      double x2 = point2.getX();
      double y1 = point1.getY();
      double y2 = point2.getY();
      double z1 = point1.getZ();
      double z2 = point2.getZ();
      double tmp = 0.0D;
      World w = point1.getWorld();
      if (x2 > x1) {
         tmp = x2;
         x2 = x1;
         x1 = tmp;
      }

      if (y2 > y1) {
         tmp = y2;
         y2 = y1;
         y1 = tmp;
      }

      if (z2 > z1) {
         tmp = z2;
         z2 = z1;
         z1 = tmp;
      }

      for(double xx = x1 - (x1 - x2); xx <= x1; ++xx) {
         for(double yy = y1 - (y1 - y2); yy <= y1; ++yy) {
            for(double zz = z1 - (z1 - z2); zz <= z1; ++zz) {
               Location construct = new Location(w, xx, yy, zz);
               if (toReplaceData != null) {
                  if (w.getBlockAt(construct).getType() == toReplace && w.getBlockAt(construct).getData() == toReplaceData) {
                     if (replaceWithData != null) {
                        w.getBlockAt(construct).setType(replaceWith);
                        w.getBlockAt(construct).setData(replaceWithData);
                     } else {
                        w.getBlockAt(construct).setType(replaceWith);
                     }
                  }
               } else if (w.getBlockAt(construct).getType() == toReplace) {
                  if (replaceWithData != null) {
                     w.getBlockAt(construct).setType(replaceWith);
                     w.getBlockAt(construct).setData(replaceWithData);
                  } else {
                     w.getBlockAt(construct).setType(replaceWith);
                  }
               }
            }
         }
      }

   }

   public void replaceBlocks(int toReplace, Byte toReplaceData, int replaceWith, Byte replaceWithData, Location point1, Location point2) {
      double x1 = point1.getX();
      double x2 = point2.getX();
      double y1 = point1.getY();
      double y2 = point2.getY();
      double z1 = point1.getZ();
      double z2 = point2.getZ();
      double tmp = 0.0D;
      World w = point1.getWorld();
      if (x2 > x1) {
         tmp = x2;
         x2 = x1;
         x1 = tmp;
      }

      if (y2 > y1) {
         tmp = y2;
         y2 = y1;
         y1 = tmp;
      }

      if (z2 > z1) {
         tmp = z2;
         z2 = z1;
         z1 = tmp;
      }

      for(double xx = x1 - (x1 - x2); xx <= x1; ++xx) {
         for(double yy = y1 - (y1 - y2); yy <= y1; ++yy) {
            for(double zz = z1 - (z1 - z2); zz <= z1; ++zz) {
               Location construct = new Location(w, xx, yy, zz);
               if (toReplaceData != null) {
                  if (w.getBlockAt(construct).getTypeId() == toReplace && w.getBlockAt(construct).getData() == toReplaceData) {
                     if (replaceWithData != null) {
                        w.getBlockAt(construct).setTypeId(replaceWith);
                        w.getBlockAt(construct).setData(replaceWithData);
                     } else {
                        w.getBlockAt(construct).setTypeId(replaceWith);
                     }
                  }
               } else if (w.getBlockAt(construct).getTypeId() == toReplace) {
                  if (replaceWithData != null) {
                     w.getBlockAt(construct).setTypeId(replaceWith);
                     w.getBlockAt(construct).setData(replaceWithData);
                  } else {
                     w.getBlockAt(construct).setTypeId(replaceWith);
                  }
               }
            }
         }
      }

   }

   public void simulateSnow(Location point1, Location point2) {
      double x1 = point1.getX();
      double x2 = point2.getX();
      double y1 = point1.getY();
      double y2 = point2.getY();
      double z1 = point1.getZ();
      double z2 = point2.getZ();
      double tmp = 0.0D;
      World w = point1.getWorld();
      if (x2 > x1) {
         tmp = x2;
         x2 = x1;
         x1 = tmp;
      }

      if (y2 > y1) {
         tmp = y2;
         y2 = y1;
         y1 = tmp;
      }

      if (z2 > z1) {
         tmp = z2;
         z2 = z1;
         z1 = tmp;
      }

      for(double xx = x1 - (x1 - x2); xx <= x1; ++xx) {
         for(double yy = y1 - (y1 - y2); yy <= y1; ++yy) {
            for(double zz = z1 - (z1 - z2); zz <= z1; ++zz) {
               Location construct = new Location(w, xx, yy, zz);
               Block b = w.getBlockAt(construct);
               if (b.getTypeId() != 0) {
                  Block offset = b.getRelative(0, 1, 0);
                  if (offset.getType() != Material.SNOW) {
                     offset.setType(Material.SNOW);
                  }
               }
            }
         }
      }

   }

   public void simulateSnowFromArrayList(ArrayList blockSet) {
      Iterator var3 = blockSet.iterator();

      while(var3.hasNext()) {
         Block b = (Block)var3.next();
         Block bRel = b.getRelative(0, 1, 0);
         bRel.setType(Material.SNOW);
      }

   }
}
