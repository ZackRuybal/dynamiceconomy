package couk.Adamki11s.Extras.Terrain;

import java.util.ArrayList;
import org.bukkit.Location;
import org.bukkit.Material;

public abstract class TerrainMethods {
   public abstract void generateCube(int var1, Location var2, Material var3, Byte var4);

   public abstract void generateCube(int var1, Location var2, int var3, Byte var4);

   public abstract void deleteSpecificCube(int var1, Location var2, Material var3, Byte var4);

   public abstract void deleteSpecificCube(int var1, Location var2, int var3, Byte var4);

   public abstract void deleteAllCube(int var1, Location var2);

   public abstract void generateFromPoints(Location var1, Location var2, Material var3, Byte var4);

   public abstract void generateFromPoints(Location var1, Location var2, int var3, Byte var4);

   public abstract void generateFromArrayList(ArrayList var1, Material var2, Byte var3);

   public abstract void generateFromArrayList(ArrayList var1, int var2, Byte var3);

   public abstract void deleteSpecificFromPoints(Location var1, Location var2, Material var3, Byte var4);

   public abstract void deleteSpecificFromPoints(Location var1, Location var2, int var3, Byte var4);

   public abstract void deleteAllFromPoints(Location var1, Location var2);

   public abstract void replaceBlocks(Material var1, Byte var2, Material var3, Byte var4, Location var5, Location var6);

   public abstract void replaceBlocks(int var1, Byte var2, int var3, Byte var4, Location var5, Location var6);

   public abstract void simulateSnow(Location var1, Location var2);

   public abstract void simulateSnowFromArrayList(ArrayList var1);
}
