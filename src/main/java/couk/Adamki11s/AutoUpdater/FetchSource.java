package couk.Adamki11s.AutoUpdater;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.logging.Logger;

public class FetchSource {
   protected static String fetchSource(URL website, Logger l, String prefix) {
      InputStream is = null;
      DataInputStream dis = null;
      String source = "";

      try {
         is = website.openStream();
      } catch (IOException var10) {
         var10.printStackTrace();
         l.info("[AU]" + prefix + " Error opening URL input stream!");
      }

      dis = new DataInputStream(new BufferedInputStream(is));
      BufferedReader br = new BufferedReader(new InputStreamReader(dis));

      String s;
      try {
         while((s = br.readLine()) != null) {
            source = source + s;
         }
      } catch (IOException var11) {
         var11.printStackTrace();
         l.info("[AU]" + prefix + " Error reading input stream!");
      }

      try {
         is.close();
      } catch (IOException var9) {
         var9.printStackTrace();
         l.info("[AU]" + prefix + " Error closing URL input stream!");
      }

      return source;
   }
}
