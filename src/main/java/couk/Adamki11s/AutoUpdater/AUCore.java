package couk.Adamki11s.AutoUpdater;

import couk.Adamki11s.Extras.Colour.ExtrasColour;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Logger;
import org.bukkit.entity.Player;

public class AUCore {
   private double versionNO;
   private double subVersionNO;
   private String reason;
   private String source;
   private String prefix;
   private String urgency;
   private Logger log;
   private ExtrasColour color = new ExtrasColour();
   private URL uri;

   public AUCore(String website, Logger l, String pref) {
      this.log = l;
      this.prefix = pref;

      try {
         this.uri = new URL(website);
      } catch (MalformedURLException var5) {
         var5.printStackTrace();
         this.log.info("[AU]" + this.prefix + " Malformed URL Exception. Make sure the URL is in the form 'http://www.website.domain'");
      }

   }

   public boolean checkVersion(double currentVersion, double currentSubVersion, String pluginname) {
      this.source = FetchSource.fetchSource(this.uri, this.log, this.prefix);
      this.formatSource(this.source);
      String subVers;
      if (currentSubVersion == 0.0D) {
         subVers = "";
      } else {
         subVers = Double.toString(currentSubVersion);
      }

      if (this.versionNO != currentVersion) {
         this.log.info("[AU]" + this.prefix + " You are not running the latest version of " + pluginname + "!");
         this.log.info("[AU]" + this.prefix + " Running version : " + currentVersion + "_" + subVers + ". Latest version : " + this.versionNO + "_" + this.subVersionNO + ".");
         this.log.info("[AU]" + this.prefix + " Update urgency for version " + this.versionNO + "_" + this.subVersionNO + " : " + this.urgency + ".");
         this.log.info("[AU]" + this.prefix + " Update reason : " + this.reason);
         return false;
      } else if (this.subVersionNO != currentSubVersion) {
         this.log.info("[AU]" + this.prefix + " You are not running the latest sub version of " + pluginname + "!");
         this.log.info("[AU]" + this.prefix + " Running version : " + currentVersion + "_" + subVers + ". Latest version : " + this.versionNO + "_" + this.subVersionNO + ".");
         this.log.info("[AU]" + this.prefix + " Update urgency for version " + this.versionNO + "_" + this.subVersionNO + " : " + this.urgency + ".");
         this.log.info("[AU]" + this.prefix + " Update reason : " + this.reason);
         return false;
      } else {
         return this.versionNO == currentVersion && this.subVersionNO == currentSubVersion;
      }
   }

   public void forceDownload(String downloadLink, String pluginName, Player player) {
      if ((new File("plugins/" + pluginName + ".jar")).exists()) {
         (new File("plugins/" + pluginName + ".jar")).delete();
      }

      this.log.info("[AU]" + this.prefix + " Downloading newest version of " + pluginName + "...");

      try {
         BufferedInputStream in = new BufferedInputStream((new URL(downloadLink)).openStream());
         FileOutputStream fos = new FileOutputStream("plugins/" + pluginName + ".jar");
         BufferedOutputStream bout = new BufferedOutputStream(fos, 1024);
         byte[] data = new byte[1024];
         boolean var8 = false;

         int x;
         while((x = in.read(data, 0, 1024)) >= 0) {
            bout.write(data, 0, x);
         }

         bout.close();
         in.close();
      } catch (Exception var9) {
         var9.printStackTrace();
         this.log.info("[AU]" + this.prefix + " Error whilst downloading update!");
         this.color.sendColouredMessage(player, "&2Error updating!");
      }

      this.log.info("[AU]" + this.prefix + " Download completed successfully!");
      this.color.sendColouredMessage(player, "&2DynamicEconomy succesfully updated!");
      this.log.info("[AU]" + this.prefix + " Newest version of " + pluginName + " has been downloaded to : '/plugins/" + pluginName + ".jar");
   }

   private void formatSource(String source) {
      String[] parts = source.split("\\@");

      try {
         this.versionNO = Double.parseDouble(parts[1]);
         this.subVersionNO = Double.parseDouble(parts[2]);
      } catch (NumberFormatException var4) {
         var4.printStackTrace();
         this.log.info("[AU]" + this.prefix + " Error while parsing version number!");
      }

      this.urgency = parts[3];
      this.reason = parts[4];
   }

   public String getReason() {
      return this.reason;
   }
}
